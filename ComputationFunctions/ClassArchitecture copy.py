import os
import multiprocessing.synchronize
import numpy as np
import multiprocessing
from scipy.signal import butter, sosfilt, freqz
import multiprocessing
from multiprocessing import shared_memory
import multiprocessing.sharedctypes
import time
import matplotlib
matplotlib.use('GTK3Agg')
import matplotlib.pyplot as plt
import ctypes
from enum import Enum
from fractions import Fraction
from overrides import overrides
import json
import sounddevice as sd

class MicrophoneArray:
    def __init__(self, radius: float, m: int, c: int, sf: int, buffer_size: int, channels_in: int, active_channels_ids: np.ndarray = None):
        self.radius = radius        # Radius of the microphone array
        self.m = m                  # Number of microphones in the array
        self.c = c                  # Speed of sound (m/s)
        self.sf = sf                # Sampling frequency (Hz)
        self.buffer_size = buffer_size  # Buffer size for processing
        self.channels_in = channels_in  # Number of input channels
        self.active_channels_ids = active_channels_ids  # IDs of active channels

class DSB:
    def __init__(self, microphoneArray: MicrophoneArray, phi: int = 0, theta: int = 90):
        # Initialize DSB beamformer by microhoneArrayCore
        self.microphone_array = microphoneArray

        self.phi = phi
        self.theta = theta
        # Calculation of delays to be on realtime beamforming
        self.fixed_delays = np.empty((8), dtype=int) 
        self.fixed_delays = np.array(self._quantize_tmi_to_samples(self._cma_tmi(self.phi, self.theta)), dtype=int)
            
    def find_device_by_name(self, target_name: str) -> int:
        """Searches a device with name defined in the parameter.

        :param target_name: Name of the searched device.
        :type target_name: str
        :return: Index of searched device.
        :rtype: int
        """
        devices = sd.query_devices()
        for index, device in enumerate(devices):
            if target_name in device['name']:
                return index
        return None

    def _quantize_tmi_to_samples(self, latencies: np.ndarray) -> np.ndarray:
        """
        Takes an array of latencies as an input and returns them back quantized to a given sampling frequency.
        Most common sampling frequencies are 44.1 kHz, 48 kHz and 96 kHz.

        :param latencies: input Latencies ment to be quantized.
        :type latencies: np.ndarray
        :return: Array of latencies quantized to certain sampling frequency.
        :rtype: np.ndarray        
        """
        minimum_latency = (1 / self.microphone_array.sf)
        result = np.round(latencies/minimum_latency)
        return result

    def _degrees_to_radians(self, degrees: int) -> float:
        """
        Converts degrees to radians.
        
        :param degrees: Angle in degrees.
        :type degrees: int
        :return: Angle in radians.
        :rtype: float
        """
        return degrees * (np.pi / 180)

    def _cma_tmi(self, phi_angle: float, theta_angle: float) -> np.ndarray:
        """
        Calculates delay times on all microphones in the circular microphone array. Results are stored and returned in a list,
        which indices are matching indices of each microphone. Delays are returned in seconds.

        :param radius: radius of circular microphone array
        :param M: number of microphones in circular microphone array
        :param phi_angle: horizontal angle of a sound source in degrees
        :param theta_angle: vertical angle of a sound source in degrees
        :param c: speed of sound
        :return: list of delays where every index of the list corresponds to the microphone index delay
        :rtype: np.ndarray
        """
        latencies = np.empty(self.microphone_array.m)
        k_hat_vector = np.array([
            np.sin(self._degrees_to_radians(theta_angle)) * np.cos(self._degrees_to_radians(phi_angle)),
            np.sin(self._degrees_to_radians(theta_angle)) * np.sin(self._degrees_to_radians(phi_angle)),
            np.cos(self._degrees_to_radians(theta_angle))
        ])
        # Dot product for k_hat and r_m_i for all mics
        for mic_id in range(self.microphone_array.m):
            rm_vector = self.microphone_array.radius * np.array([
                np.cos(2 * np.pi * (mic_id / self.microphone_array.m)),
                np.sin(2 * np.pi * (mic_id / self.microphone_array.m)),
                0
            ])
            t_m_i = np.dot(k_hat_vector, rm_vector)/self.microphone_array.c
            latencies[mic_id] = t_m_i
        # Find smallest t_m_i and adds it to the latencies according to fomrula 1.2
        t_m_c = np.max(latencies)
        latencies = t_m_c - latencies
        return latencies

    def _calculate_rms_from_buffer(self, audio_buffer: np.ndarray) -> int:
        """Calculates RMS value from given buffer in dimensionles values.

        :param audio_buffer: Audio buffer from which rms should be calculated.
        :type audio_buffer: np.ndarray
        :return: RMS value of current buffer
        :rtype: int
        """
        rms_value = np.sqrt(np.mean(audio_buffer**2))
        return rms_value

    def _delay_signal_by_frames(self, sample_delay: int, input_buffer: np.ndarray, previous_buffer: np.ndarray) -> np.ndarray:
        """This function takes in previous buffer and uses it to delay data in the current "input_buffer" buffer.
        Signal cannot be delayed by more samples than the size of the buffer.

        :param sample_delay: Number of samples by which a signal should be delayed.
        :type sample_deay: int
        :param input_buffer: Buffer on which the delays should be applied
        :type input_buffer: np.ndarray
        :param previous_buffer: Previous input buffer used to get samples for delay
        :type delay_buffer: np.ndarray
        :return: Delayed signal
        :rtype: np.ndarray
        """
        if sample_delay == 0:
            return input_buffer
        history = previous_buffer[-sample_delay:]
        delayed_signal = np.concatenate((history, input_buffer))
        delayed_signal = delayed_signal[:-sample_delay]
        return delayed_signal

    def _delay_signal_interpolation(self, audio_buffer: np.ndarray, delay_time_microseconds: float,
                                additional_sample_delay: int = None) -> np.ndarray:
        """This function creates delay smaller than a minimum quantization step caused by sampling frequency. 
        This is done by doing linear interpolation of signal for selected amounth of microseconds. User can also
        specify additional delay by whole samples in last argument of the function (optional).
        
        Make sure to notice, that for a linear interpolation a to be done properly, a previous samples from the current buffer
        are needed to be known. If previous buffer is not specified, a zero at the start is NOT DEFINED sample. If delay 
        exeeds quantization step, all zeros before the "not defined" samples are considered to be delay. This effect is 
        removed if previous buffer is defined.
        
        :param audio_buffer: Audio buffer on which the delay should be applied
        :type audio_buffer: np.ndarray
        :param delay_time_microseconds: Time by which the signal should be delayed in microseconds
        :type delay_time_microseconds: float
        :param additional_sample_delay: Additional delay in whole samples
        :type additional_sample_delay: int
        :return: Delayed signal by interpolation
        :rtype: np.ndarray
        """
        # Checking if delay is smaller than quantization step. If so, additional delay in whole samples is applied.
        eta_raw = delay_time_microseconds * 1e-6 * self.microphone_array.sf
        sample_delay = int(eta_raw)

        # Adding more whole samples delay if user specified any.
        if additional_sample_delay != None:
            sample_delay += additional_sample_delay

        # Calculating a fraction of a quantization step by which the samples should be delayed
        eta = eta_raw - sample_delay

        # Buffer for storing resulting signal
        delayed_buffer = np.zeros_like(audio_buffer)
        
        # Definition of supporting indices arrays to enable vectorization in numpy. This is to achieve a better performance.
        original_indices = np.arange(sample_delay + 1, len(audio_buffer))
        delayed_indices = original_indices - sample_delay

        # Linear interpolation
        delayed_buffer[original_indices] = (1 - eta) * audio_buffer[delayed_indices - 1] + eta * audio_buffer[delayed_indices]

        return delayed_buffer

    def apply_dsb_delays_realtime(self, input_buffer: np.ndarray, previous_buffer: np.ndarray) -> np.ndarray:
        """This function takes 16 channel audio with 8 active channels as an input and applies delays that are stored in all_delays variable.

        :param input_buffer: Purrent multichannel buffer on which delays should be applied
        :type input_buffer: np.ndarray
        :param previous_buffer: Previous multichannel buffer which is used for applying delays 
        :type previous_buffer: np.ndarray
        :param theta_index: Index identifying theta angle in all_delays array
        :type theta_index: int
        :param phi_index: Index identifying phi angle in all_delays array
        :type phi_index: int
        :param all_delays: Array in which the latencies are stored. This is to save computation time of calculating delays in every function call.
        :type all_delays: np.ndarray
        :return: 16 channel audio buffer 
        :rtype: np.ndarray
        """
        delayed_buffer = np.empty((input_buffer.shape[0], 8))
        delayed_buffer[:, 0] = self._delay_signal_by_frames(self.fixed_delays[4], input_buffer[:, 8], previous_buffer[:, 8])
        delayed_buffer[:, 1] = self._delay_signal_by_frames(self.fixed_delays[5], input_buffer[:, 0], previous_buffer[:, 0])
        delayed_buffer[:, 2] = self._delay_signal_by_frames(self.fixed_delays[6], input_buffer[:, 9], previous_buffer[:, 9])
        delayed_buffer[:, 3] = self._delay_signal_by_frames(self.fixed_delays[7], input_buffer[:, 1], previous_buffer[:, 1])
        delayed_buffer[:, 4] = self._delay_signal_by_frames(self.fixed_delays[0], input_buffer[:, 10], previous_buffer[:, 10])
        delayed_buffer[:, 5] = self._delay_signal_by_frames(self.fixed_delays[1], input_buffer[:, 2], previous_buffer[:, 2])
        delayed_buffer[:, 6] = self._delay_signal_by_frames(self.fixed_delays[2], input_buffer[:, 11], previous_buffer[:, 11])
        delayed_buffer[:, 7] = self._delay_signal_by_frames(self.fixed_delays[3], input_buffer[:, 3], previous_buffer[:, 3])
        # Add delayed signals together
        added_signals = np.sum(delayed_buffer, axis=1) / 8
        return added_signals 

    def _signal_to_decibels(self, signal, reference=1):
        """ Converts signal given in unitless format to decibels """
        decibels = 10 * np.log10(signal / reference)
        return decibels

class SourceFinderDSB(DSB):
    def __init__(self, dsb_instance: MicrophoneArray, theta_steps: int, phi_steps: int, duration: int, 
                 show_time = False, show_angles = False, 
                 calculate_average_time = False, show_gui = False, refreshing_time = 0):
        super().__init__(
            dsb_instance.microphone_array,
        )
        self.theta_steps = theta_steps
        self.phi_steps = phi_steps
        
        self.duration = duration
        self.show_time = show_time
        self.show_angles = show_angles
        self.calculate_average_time = calculate_average_time
        self.show_gui = show_gui
        self.refreshing_time = refreshing_time # In miliseconds

        self.all_dsb_delays_quantized = self.__calculate_dsb_delays()

        # Setting angle spaces for analysis purposes
        if self.show_angles:
            self.phi_angles_deg = np.linspace(0, 360, self.phi_steps, endpoint=False)
            self.theta_angles_deg = np.linspace(90, 0, self.theta_steps) 
        
    def __calculate_dsb_delays(self, quantize: bool = True, floor: bool = False) -> np.ndarray:
        """This function is used to precalculate all delays needed for delay and sum beamforming for circular microphone delay. 
        This is to save time while performing real time calculations of finding the loudest source.
        It takes number of tried theta and number of tried phi as an argument and according to that it divides the space to equally distanced angles.
        For every theta angle it calculates corresponding number of phi angles. Result is stored and returned in 3 dimensional array,
        where fist dimension is theta angle, second dimension phi anlge and third dimension are individual delays for every microphone according to those parameters.
        When parameter quantized is set to False, it returns delays on each microphone in miliseconds. When quantized is set to false, it returns delays in whole samples.

        TODO: Test this function for multiple theta and M.

        :param radius: Radius of circular microphone array
        :type radius: float
        :param M: Number of microphones in circular microphone array
        :type M: int
        :param sampling_frequency: Sampling frequency
        :type sampling_frequency: int
        :param number_of_tried_theta: Number of theta angles for which the space of (pi, 0) angles should be divided.
        :type number_of_tried_theta: int
        :param number_of_tried_phi: Number of phi angles for which the space of (0, 2*pi) angles should be divided.
        :type number_of_tried_phi: int
        :param c: Speed of sound, usually 343 m/s
        :type c: int
        :return: 3 dimensional array corresponding to theta angle, phi angle and individual delays for each microphone.
        :rtype: np.ndarray
        """
        if quantize or floor:
            all_latencies = np.empty((self.theta_steps, self.phi_steps, 8), dtype=int)
        else:
            all_latencies = np.empty((self.theta_steps, self.phi_steps, 8), dtype=np.float64)
        phi_angles = np.linspace(0, 360, self.phi_steps, endpoint=False)
        theta_angles = np.linspace(90, 20, self.theta_steps) 
        for theta_index in range(0, self.theta_steps):
            for phi_index in range(0, self.phi_steps):
                # Calculate delays for current direction
                delays = self._cma_tmi(phi_angles[phi_index], theta_angles[theta_index])
                if floor:
                    quantization_step = 1/self.sf
                    all_latencies[theta_index][phi_index] = np.array(np.floor(delays/quantization_step), dtype=int)
                elif quantize:
                    all_latencies[theta_index][phi_index] = np.array(self._quantize_tmi_to_samples(delays), dtype=int)
                else:
                    all_latencies[theta_index][phi_index] = delays * 1e6
        return all_latencies

    def realtime_visualization(self, rms_values: multiprocessing.sharedctypes.Array, rms_values_updated_event: multiprocessing.synchronize.Event, 
            duration: int, refresh_time: float):
        start_time = time.time()
        # Initializing phi_radians with 2*pi at the end to close the polar diagram
        phi_radians = np.linspace(0, 2*np.pi, self.phi_steps, endpoint=False)
        phi_radians = np.append(phi_radians, np.array([2*np.pi]))

        # Set interactive mode
        plt.ion() 
        # Set plot
        fig = plt.figure()
        ax = fig.add_subplot(111, polar=True)
        ax.set_ylim([-30, 0])
        ax.set_theta_direction(-1)
        arrow = ax.annotate('', xy=(phi_radians[0], -10), 
                            xytext=(phi_radians[0], -30), 
                            arrowprops=dict(color='red', linewidth=2, arrowstyle='->'))
        
        line, = ax.plot(phi_radians, np.full(self.phi_steps + 1, -10), linestyle='-', color='b')
        plt.pause(0.1)

        while time.time() - start_time < duration:
            if not rms_values_updated_event.is_set():
                rms_values_updated_event.wait()
            else:
                rms_values_updated_event.clear()
                with rms_values.get_lock():
                    rms_values_ndarray = np.frombuffer(rms_values.get_obj(), dtype=np.float64)
                rms_decibels = self._signal_to_decibels(rms_values_ndarray)
                rms_decibels_polar = np.append(rms_decibels, rms_decibels[0]) # Adding first element at the end to close the polar plot
                upper_limit = np.max(rms_decibels)
                upper_limit_index = np.argmax(rms_decibels)
                bottom_limit = np.min(rms_decibels)
                line.set_ydata(rms_decibels_polar)
                ax.set_ylim([bottom_limit, upper_limit])
                arrow.remove()
                arrow = ax.annotate('', xy=(phi_radians[upper_limit_index], upper_limit), 
                        xytext=(phi_radians[upper_limit_index], bottom_limit), 
                        arrowprops=dict(color='red', linewidth=2, arrowstyle='->'))
                plt.draw()
                plt.pause(refresh_time)

    def _cut_dsb_delays(self, input_buffer: np.ndarray, theta_index: int, phi_index: int) -> tuple[np.ndarray, int]:
        """This function takes 16 channel audio with 8 active channels as an input and applies delays that are stored in all_delays variable.
        In contrast to function apply_delays_on_buffer is this one not using a previous buffer to calculate resulting signal. This is to optimize
        algorithm for loudest source finder which does not need an audio output to be continuous. It just needs to calculate RMS from "some" signal
        sample.
        
        :param input_buffer: Purrent multichannel buffer on which delays should be applied
        :type input_buffer: np.ndarray
        :param previous_buffer: Previous multichannel buffer which is used for applying delays 
        :type previous_buffer: np.ndarray
        :param theta_index: Index identifying theta angle in all_delays array
        :type theta_index: int
        :param phi_index: Index identifying phi angle in all_delays array
        :type phi_index: int
        :param all_delays: Array in which the latencies are stored. This is to save computation time of calculating delays in every function call.
        :type all_delays: np.ndarray
        :return: 16 channel audio buffer 
        :rtype: np.ndarray
        """
        # V této modifikaci chci vpodstatě jenom useknout zpoždění od těch nijak nezpožděných
        max_delay = np.max(self.all_dsb_delays_quantized[theta_index][phi_index])
        delayed_buffer = np.empty((input_buffer.shape[0] - max_delay, 8))
        delayed_buffer[:, 0] = input_buffer[:, 8][max_delay - self.all_dsb_delays_quantized[theta_index][phi_index][4] : input_buffer.shape[0] - self.all_dsb_delays_quantized[theta_index][phi_index][4]]
        delayed_buffer[:, 1] = input_buffer[:, 0][max_delay - self.all_dsb_delays_quantized[theta_index][phi_index][5] : input_buffer.shape[0] - self.all_dsb_delays_quantized[theta_index][phi_index][5]]
        delayed_buffer[:, 2] = input_buffer[:, 9][max_delay - self.all_dsb_delays_quantized[theta_index][phi_index][6] : input_buffer.shape[0] - self.all_dsb_delays_quantized[theta_index][phi_index][6]]
        delayed_buffer[:, 3] = input_buffer[:, 1][max_delay - self.all_dsb_delays_quantized[theta_index][phi_index][7] : input_buffer.shape[0] - self.all_dsb_delays_quantized[theta_index][phi_index][7]]
        delayed_buffer[:, 4] = input_buffer[:, 10][max_delay - self.all_dsb_delays_quantized[theta_index][phi_index][0] : input_buffer.shape[0] - self.all_dsb_delays_quantized[theta_index][phi_index][0]]
        delayed_buffer[:, 5] = input_buffer[:, 2][max_delay - self.all_dsb_delays_quantized[theta_index][phi_index][1] : input_buffer.shape[0] - self.all_dsb_delays_quantized[theta_index][phi_index][1]]
        delayed_buffer[:, 6] = input_buffer[:, 11][max_delay - self.all_dsb_delays_quantized[theta_index][phi_index][2] : input_buffer.shape[0] - self.all_dsb_delays_quantized[theta_index][phi_index][2]]
        delayed_buffer[:, 7] = input_buffer[:, 3][max_delay - self.all_dsb_delays_quantized[theta_index][phi_index][3] : input_buffer.shape[0] - self.all_dsb_delays_quantized[theta_index][phi_index][3]]
        added_signals = np.sum(delayed_buffer, axis=1) / 8
        return (added_signals, max_delay)

    def loop_impl(self, shm_name: str, shm_lock: multiprocessing.synchronize.Lock, buffer_ready_event: multiprocessing.synchronize.Event, 
                             max_rms_indx: multiprocessing.sharedctypes.SynchronizedArray, rms_values: multiprocessing.sharedctypes.SynchronizedArray, 
                             rms_values_updated_event: multiprocessing.synchronize.Event) -> None:
        computation_process_start_time = time.time()
        # Initialize "pointer" to shared memory, which stores data (buffer) to be computed
        buffer_shm = shared_memory.SharedMemory(name=shm_name)
        indata = np.ndarray((self.microphone_array.buffer_size, self.microphone_array.channels_in), dtype=np.float64, buffer=buffer_shm.buf)

        # Prepare variables to store results of the computation
        rms_values_temp = np.empty((self.theta_steps, self.phi_steps), dtype=np.float64)

        # Analysis tools:
        if self.calculate_average_time:
            average_times = []

        # Begin cycle till the duration is over
        while time.time() - computation_process_start_time < self.duration:
            # Waiting for the audio buffer form callback to be availible
            if not buffer_ready_event.is_set():
                buffer_ready_event.wait()
            else:
                # Start timer measuring the time of the algorithm
                cycle_start_time = time.time()
                # Lock the indata variable so only I can read it to prevent data corruption
                with shm_lock:
                    for theta_index in range(0, self.theta_steps):
                        for phi_index in range(0, self.phi_steps):
                            added_signals, max_delay = self._cut_dsb_delays(indata, theta_index, phi_index)
                            rms_values_temp[theta_index][phi_index] = self._calculate_rms_from_buffer(added_signals)
                max_rms_index = np.unravel_index(np.argmax(rms_values_temp), rms_values_temp.shape)
                # Start timer measuring the time of the algorithm
                cycle_end_time = time.time()
                # Save results to the shared resources
                # ----------- Save results to the shared resources -----------
                with max_rms_indx.get_lock():
                    max_rms_indx[0] = max_rms_index[0]
                    max_rms_indx[1] = max_rms_index[1]
                if self.show_gui:
                    with rms_values.get_lock():
                        np.frombuffer(rms_values.get_obj(), dtype=np.float64)[:] = rms_values_temp
                    rms_values_updated_event.set()
                # found_direction_event.set()
                # ------------------------------------------------------------
                # ----------------------- ANALYSIS PART ----------------------
                total_cycle_time = (cycle_end_time - cycle_start_time) * 1000
                if self.calculate_average_time:
                    average_times.append(total_cycle_time)
                if self.show_time:
                    print(f"Cycle length: {total_cycle_time} ms")
                if self.show_angles:
                    print(f"Theta: {self.theta_angles_deg[max_rms_index[0]]}, Phi: {self.phi_angles_deg[max_rms_index[1]]}")
                # ------------------------------------------------------------
                if self.refreshing_time != 0 and time.time() - cycle_start_time < self.refreshing_time:
                    # When refreshing time is set it is waiting the remaining time it waits the remaining time 
                    # to artificialy slow the algorithm down
                    time.sleep(self.refreshing_time - (time.time() - cycle_start_time) )
                # Send a signal to the callback function that it can prepare next buffer to be processed
                buffer_ready_event.clear()
        
        # ANALYSIS - counting average computing time among the whole run of the algorithm
        if self.calculate_average_time:
            print(f"Average computation time is {sum(average_times) / len(average_times)} ms per cycle")

    def _init_process(self) -> None:
        """sets lower priority on processor level to the pool of processes
        """
        os.nice(30) 

    def _calculate_direction_dsb(self, args) -> tuple:
        """
        Calculates rms for direction specified in theta_index and phi_index. Those indices correspond to all_latencies
        variable, where they correspond to a certain angles. This is to speed up the system, so new latencies are not
        calculated every time they are being used.

        Purpose of this function is to speed up the algorithm, each of those functions are ment to be calculated in
        different thread.
        """
        indata, theta_index, phi_index = args
        return self._calculate_rms_from_buffer(self._cut_dsb_delays(indata, theta_index, phi_index)[0]), theta_index, phi_index

    def pool_impl(self, shm_name: str, shm_lock: multiprocessing.synchronize.Lock, buffer_ready_event: multiprocessing.synchronize.Event, 
                             max_rms_indx: multiprocessing.sharedctypes.SynchronizedArray, rms_values: multiprocessing.sharedctypes.SynchronizedArray, 
                             rms_values_updated_event: multiprocessing.synchronize.Event) -> None:
        buffer_shm = shared_memory.SharedMemory(name=shm_name)
        computation_process_start_time = time.time()
        # Initialize "pointer" to shared memory, which stores data (buffer) to be computed
        indata = np.ndarray((self.microphone_array.buffer_size, self.microphone_array.channels_in), dtype=np.float64, buffer=buffer_shm.buf)

        # Prepare variables to store results of the computation
        rms_values_temp = np.empty((self.theta_steps, self.phi_steps), dtype=np.float64)

        index_combinations = [(theta_index, phi_index)
                          for theta_index in range(self.theta_steps)
                          for phi_index in range(self.phi_steps)]

        # Analysis tools:
        if self.calculate_average_time:
            average_times = []

        with multiprocessing.Pool(initializer=self._init_process, processes=multiprocessing.cpu_count()) as pool:
            # Begin cycle till the duration is over
            while time.time() - computation_process_start_time < self.duration:
                # Waiting for the audio buffer form callback to be availible
                if not buffer_ready_event.is_set():
                    buffer_ready_event.wait()
                else:
                    # Start timer measuring the time of the algorithm
                    cycle_start_time = time.time()
                    # Lock the indata variable so only I can read it to prevent data corruption
                    with shm_lock:
                        args = [(indata, *indices) for indices in index_combinations]
                    # Parallelize the computation of the loudest source finder in the processes pool
                    results = pool.map(self._calculate_direction_dsb, args)
                    # Extract results from the calculations
                    for result in results:
                        rms_value, theta_index, phi_index = result
                        rms_values_temp[theta_index][phi_index] = rms_value
                    max_rms_index = np.unravel_index(np.argmax(rms_values_temp), rms_values_temp.shape)
                    # Start timer measuring the time of the algorithm
                    cycle_end_time = time.time()
                    # ----------- Save results to the shared resources -----------
                    with max_rms_indx.get_lock():
                        max_rms_indx[0] = max_rms_index[0]
                        max_rms_indx[1] = max_rms_index[1]
                    if self.show_gui:
                        with rms_values.get_lock():
                            np.frombuffer(rms_values.get_obj(), dtype=np.float64)[:] = rms_values_temp
                        rms_values_updated_event.set()
                    # ------------------------------------------------------------
                    # ----------------------- ANALYSIS PART ----------------------
                    total_cycle_time = (cycle_end_time - cycle_start_time) * 1000
                    if self.calculate_average_time:
                        average_times.append(total_cycle_time)
                    if self.show_time:
                        print(f"Cycle length: {total_cycle_time} ms")
                    if self.show_angles:
                        print(f"Theta: {self.theta_angles_deg[max_rms_index[0]]}, Phi: {self.phi_angles_deg[max_rms_index[1]]}")
                    # ------------------------------------------------------------
                    if self.refreshing_time != 0 and time.time() - cycle_start_time < self.refreshing_time:
                        # When refreshing time is set it is waiting the remaining time it waits the remaining time 
                        # to artificialy slow the algorithm down
                        time.sleep(self.refreshing_time - (time.time() - cycle_start_time) )
                    # Send a signal to the callback function that it can prepare next buffer to be processed
                    buffer_ready_event.clear()
        
        # ANALYSIS - counting average computing time among the whole run of the algorithm
        if self.calculate_average_time:
            print(f"Average computation time is {sum(average_times) / len(average_times)} ms per cycle")

    @overrides
    def apply_dsb_delays_realtime(self, input_buffer: np.ndarray, previous_buffer: np.ndarray, theta_index: int = 0, phi_index: int = 0) -> np.ndarray:
        """This function takes 16 channel audio with 8 active channels as an input and applies delays that are stored in all_delays variable.

        :param input_buffer: Purrent multichannel buffer on which delays should be applied
        :type input_buffer: np.ndarray
        :param previous_buffer: Previous multichannel buffer which is used for applying delays 
        :type previous_buffer: np.ndarray
        :param theta_index: Index identifying theta angle in all_delays array
        :type theta_index: int
        :param phi_index: Index identifying phi angle in all_delays array
        :type phi_index: int
        :param all_delays: Array in which the latencies are stored. This is to save computation time of calculating delays in every function call.
        :type all_delays: np.ndarray
        :return: 16 channel audio buffer 
        :rtype: np.ndarray
        """
        delayed_buffer = np.empty((input_buffer.shape[0], 8))
        delayed_buffer[:, 0] = self._delay_signal_by_frames(self.all_dsb_delays_quantized[theta_index][phi_index][4], input_buffer[:, 8], previous_buffer[:, 8])
        delayed_buffer[:, 1] = self._delay_signal_by_frames(self.all_dsb_delays_quantized[theta_index][phi_index][5], input_buffer[:, 0], previous_buffer[:, 0])
        delayed_buffer[:, 2] = self._delay_signal_by_frames(self.all_dsb_delays_quantized[theta_index][phi_index][6], input_buffer[:, 9], previous_buffer[:, 9])
        delayed_buffer[:, 3] = self._delay_signal_by_frames(self.all_dsb_delays_quantized[theta_index][phi_index][7], input_buffer[:, 1], previous_buffer[:, 1])
        delayed_buffer[:, 4] = self._delay_signal_by_frames(self.all_dsb_delays_quantized[theta_index][phi_index][0], input_buffer[:, 10], previous_buffer[:, 10])
        delayed_buffer[:, 5] = self._delay_signal_by_frames(self.all_dsb_delays_quantized[theta_index][phi_index][1], input_buffer[:, 2], previous_buffer[:, 2])
        delayed_buffer[:, 6] = self._delay_signal_by_frames(self.all_dsb_delays_quantized[theta_index][phi_index][2], input_buffer[:, 11], previous_buffer[:, 11])
        delayed_buffer[:, 7] = self._delay_signal_by_frames(self.all_dsb_delays_quantized[theta_index][phi_index][3], input_buffer[:, 3], previous_buffer[:, 3])
        # Add delayed signals together
        added_signals = np.sum(delayed_buffer, axis=1) / 8
        return added_signals 

class DMACompRegime(Enum):
    BUTTER = 1,
    INTERPOL = 2

class RMSfilter(Enum):
    """Definition of filter regimes for search of the loudest source finder by cardioid and dsb algorithm.
    """
    LOWPASS_ONLY = 1
    HIGHPASS_ONLY = 2,
    BOTH = 3

class SourceFinderDSBCardioid(SourceFinderDSB):
    def __init__(self, dsb_instance, theta_steps: int, phi_steps: int, duration: int, mic_pairs: np.ndarray, 
                 boundary_cutoff_freq: int, show_time = False, show_angles = False, 
                 calculate_average_time = False, show_gui = False, refreshing_time = 0, 
                 rms_filter: RMSfilter = RMSfilter.BOTH, regime = DMACompRegime.INTERPOL):
        super().__init__(
            dsb_instance=dsb_instance,
            duration=duration,
            show_time=show_time,
            show_angles=show_angles,
            calculate_average_time=calculate_average_time,
            show_gui=show_gui,
            refreshing_time=refreshing_time,
            theta_steps=theta_steps,
            phi_steps=phi_steps
        )

        self.mic_pairs = mic_pairs
        self.rms_filter = rms_filter
        self.regime = regime
        self.boundary_cutoff_freq = boundary_cutoff_freq

        self.directions_between_mics = int(self.phi_steps/self.microphone_array.m)
        
        if regime == DMACompRegime.INTERPOL:
            self.interpol_delays = self.__prepare_interpol_delays()
        elif regime == DMACompRegime.BUTTER:
            self.butter_coeffs = self.__prepare_butter_coeffs()

        # Setting up butterworth filter coeLOWPfficients
        self.sos_low = butter(1, boundary_cutoff_freq, fs=self.microphone_array.sf, btype='low', output='sos')
        self.sos_high = butter(1, boundary_cutoff_freq, fs=self.microphone_array.sf, btype='high', output='sos')

        # Setting angle spaces for analysis purposes
        if self.show_angles:
            self.phi_angles_deg = np.linspace(0, 360, self.phi_steps, endpoint=False)
            self.theta_angles_deg = np.linspace(90, 20, self.theta_steps) 

    def __approximation_radius(self, numenator: int, denominator: int) -> np.float64:
        """This function calculates distance between 2 mics that are approximated. This is needed
        because they do not lay on the circle of the microphone array, therefore radius cannot be
        used.

        :param circle_radius: _description_
        :type circle_radius: float
        :param approximation_ratio: _description_
        :type approximation_ratio: str
        """
        # Distance between 2 neighbouring microphones. Calculated using law of cosines formula (c^2 = a^2 + b^2 -2*a*b**cos(gamma))
        dist_between_mics = np.sqrt(self.microphone_array.radius**2 + self.microphone_array.radius**2 -2*(self.microphone_array.radius**2)*np.cos((2*np.pi)/8))
        my_distance = numenator/denominator * dist_between_mics
        return np.sqrt((my_distance)**2 + self.microphone_array.radius**2 - 2*self.microphone_array.radius*my_distance*np.cos(np.radians(67.5)))
    
    def __tau_compensation(self, delta_norm: float, delta_approx: float, quantize_tau: bool = False) -> np.float64:
        """This function calculates the compensation of shorter distance between physical (delta) mics and approximated
        mics (delta_2). It is done by calculating heigher delay tau that should be added to the approximated delaying
        mic. It returns compensated tau for the delaying approximated mic.

        :param delta_norm: _description_
        :type delta_norm: float
        :param delta_approx: _description_
        :type delta_approx: float
        :param quantize_tau: determines if it should calculate compensation towards quantized tau on physical mics, defaults to True
        :type quantize_tau: bool, optional
        :return: _description_
        :rtype: float
        """
        tau_norm = delta_norm/self.microphone_array.c # Tau for physical microphones
        if quantize_tau:
            q_step = 1/self.microphone_array.sf # Quantization step
            tau_norm = round(tau_norm / q_step) * q_step # Count with quantized t
        tau_approx = (delta_approx * tau_norm) / delta_norm # Tau for the approximated microphones
        tau_compensated = 2*tau_norm - tau_approx
        return tau_compensated

    def __prepare_interpol_delays(self):
        quantization_step = 1/self.microphone_array.sf
        interpolation_delays = {} # Initialize dictionary, where the delays will be put
        my_tau = self.microphone_array.radius*2/self.microphone_array.c
        my_sample_delay = int(np.floor(my_tau/quantization_step))
        delay_in_ms = (my_tau % quantization_step) * 1000000
        interpolation_delays[Fraction(0, self.directions_between_mics)] = (my_sample_delay, delay_in_ms) # physical microphone
        for i in range(1, int(np.floor(self.directions_between_mics/2)) + 1):
            distance = self.__approximation_radius(i, self.directions_between_mics) * 2
            tau_compensated = self.__tau_compensation(self.microphone_array.radius*2, distance)
            delay_in_samples = int(np.floor(tau_compensated/quantization_step))
            remaining_delay_in_ms = (tau_compensated % quantization_step) * 1000000
            interpolation_delays[Fraction(i, self.directions_between_mics)] = (delay_in_samples, remaining_delay_in_ms)
        return interpolation_delays

    def _calculate_direction_card_dsb_interpol(self, args) -> tuple:
        buffer_LP, buffer_HP, pair_index, approx_index = args
        ## Cardioid + DSB calculation
        # Selection of mics for cardioid. It takes also the next one for approximation purposes.
        leading_buffer = buffer_LP[:, self.mic_pairs[pair_index][1]]
        leading_next_buffer = buffer_LP[:, self.mic_pairs[(pair_index+1)%self.microphone_array.m][1]]

        delaying_buffer = buffer_LP[:, self.mic_pairs[pair_index][0]]
        delaying_next_buffer = buffer_LP[:, self.mic_pairs[(pair_index+1)%self.microphone_array.m][0]]

        # Approximating microphones based on approx_index.
        lead_buffer = ((self.directions_between_mics - approx_index)/self.directions_between_mics)*leading_buffer + (approx_index/self.directions_between_mics)*leading_next_buffer
        delaying_buffer = ((self.directions_between_mics - approx_index)/self.directions_between_mics)*delaying_buffer + (approx_index/self.directions_between_mics)*delaying_next_buffer
        
        # Approximation calculation
        if approx_index > np.floor(self.directions_between_mics/2):
            cardioid_sample_delay, ms_delay = self.interpol_delays[Fraction(self.directions_between_mics - approx_index, self.directions_between_mics)]
        else:
            cardioid_sample_delay, ms_delay = self.interpol_delays[Fraction(approx_index, self.directions_between_mics)]
        delayed_buffer = self._delay_signal_interpolation(delaying_buffer, ms_delay, self.microphone_array.sf) 
        cardioid = (lead_buffer[:self.microphone_array.buffer_size - cardioid_sample_delay] - delayed_buffer[cardioid_sample_delay:])
            
        ## DSB high Calculation
        output_high, max_delay = self._cut_dsb_delays(buffer_HP, 0, (self.directions_between_mics*pair_index)+approx_index)

        ## RMS calculation
        return pair_index, approx_index, self._calculate_rms_from_buffer(cardioid), self._calculate_rms_from_buffer(output_high)

    def __cardioid_tf_quantized(self, phi: int, mic_dist: float, max_frequency=1000):
        """
        Calculates transfer function for cardioid.
        """
        q_step = 1/self.microphone_array.sf
        frequencies = np.arange(20, max_frequency + 1)
        ro_frequencies = 2 * np.pi * frequencies
        tau = mic_dist / self.microphone_array.c
        tau_q = round((tau / q_step)) * q_step

        ts = 1 - np.exp(-1j * ro_frequencies * ((mic_dist/self.microphone_array.c) * np.cos(phi) + tau_q))

        return ts

    def __find_most_similar_frequency(self, ratios, a, b):
        """
        Najde frekvenci, na které je frekvenční odezva Butterworthova filtru nejvíce podobná danému poli ratios.
        TODO

        Parametry:
        ratios (numpy.ndarray): Pole hodnot pro porovnání s frekvenční odezvou.
        fs (int): Vzorkovací frekvence.
        a (int): Dolní hranice intervalu frekvencí pro testování.
        b (int): Horní hranice intervalu frekvencí pro testování.
        n_points (int): Počet frekvenčních bodů v intervalu od a do b.
        
        Vrací:
        float: Frekvence, na které byla nalezena největší podobnost.
        """
        n_points = b - a + 1
        frequencies = np.linspace(a, b, n_points)
        best_frequency = a
        lowest_mae = np.inf
        order = 1 # Butterworth filter order
        
        for cutoff in frequencies:
            # Filter design
            b, a = butter(order, cutoff / (0.5 * self.microphone_array.sf), btype='low', analog=False)
            # Frequency response
            w, h = freqz(b, a, worN=np.linspace(20, self.boundary_cutoff_freq, self.boundary_cutoff_freq - 19) * 2*np.pi / self.microphone_array.sf)
            magnitude = np.abs(h) * ratios[0]

            mean = np.mean(np.abs((ratios - magnitude[:len(ratios)])))
            if mean < lowest_mae:
                lowest_mae = mean
                best_frequency = cutoff

        return best_frequency

    def __load_cutoff_data(self):
        try:
            with open('cutoff_data.json', 'r') as file:
                return json.load(file)
        except FileNotFoundError:
            return {}

    def __save_cutoff_data(self, data):
        with open('cutoff_data.json', 'w') as file:
            json.dump(data, file)

    def __prepare_butter_coeffs(self, butter_output = 'sos'):
        cutoff_data = self.__load_cutoff_data()
        tf_main_amplitudes = np.abs(self.__cardioid_tf_quantized(0, self.microphone_array.radius*2))
        filter_coeffs_and_amplifier = {}
        filter_coeffs_and_amplifier[Fraction(0, self.directions_between_mics)] = (None, int(np.round(((self.microphone_array.radius*2/self.microphone_array.c)*self.microphone_array.sf))), 1.08)
        # Creating butter coefficients with amplifier
        for numerator in range(1, int(np.ceil(self.directions_between_mics / 2))+1):
            distance = self.__approximation_radius(numerator, self.directions_between_mics) * 2
            # Transfer function of approximated microphones
            tf_approx_amplitudes = np.abs(self.__cardioid_tf_quantized(0, distance))
            # Caclulation of ratio between physical and approximation microphones curve
            ratios_curve = np.abs(tf_main_amplitudes/tf_approx_amplitudes)
            # Determining the amplification numerator 
            amplification_numerator = ratios_curve[0]
            # Calculation of delay in samples
            delay_in_samples = round((distance/self.microphone_array.c)*self.microphone_array.sf)
            # Calculation of cutoff frequency for this distance
            fraction_key = str(Fraction(numerator, self.directions_between_mics))
            if fraction_key in cutoff_data:
                cutoff = cutoff_data[fraction_key]['cutoff']
            else:
                cutoff = self.__find_most_similar_frequency(ratios_curve, 2200, 10000)
                cutoff_data[fraction_key] = {'cutoff': cutoff}
                self.__save_cutoff_data(cutoff_data)
                print(f"Data for distance {numerator} / {self.directions_between_mics} has been calculated, cutoff is {cutoff}")
            filter_coeffs_and_amplifier[Fraction(numerator, self.directions_between_mics)] = (butter(1, cutoff, fs=self.microphone_array.sf, btype='low', output=butter_output), delay_in_samples, amplification_numerator)
        return filter_coeffs_and_amplifier

    def _calculate_direction_card_dsb_butter(self, args) -> tuple:
        buffer_LP, buffer_HP, pair_index, approx_index = args
        ## Cardioid + DSB calculation
        # Selection of mics for cardioid. It takes also the next one for approximation purposes.
        leading_buffer = buffer_LP[:, self.mic_pairs[pair_index][1]]
        leading_next_buffer = buffer_LP[:, self.mic_pairs[(pair_index+1)%self.microphone_array.m][1]]

        delaying_buffer = buffer_LP[:, self.mic_pairs[pair_index][0]]
        delaying_next_buffer = buffer_LP[:, self.mic_pairs[(pair_index+1)%self.microphone_array.m][0]]

        # Approximating microphones based on approx_index.
        lead_buffer = ((self.directions_between_mics - approx_index)/self.directions_between_mics)*leading_buffer + (approx_index/self.directions_between_mics)*leading_next_buffer
        delaying_buffer = ((self.directions_between_mics - approx_index)/self.directions_between_mics)*delaying_buffer + (approx_index/self.directions_between_mics)*delaying_next_buffer
        
        # Approximation calculation
        if approx_index > np.floor(self.directions_between_mics/2):
            butter_coeffs, cardioid_sample_delay, amplification_numerator = self.butter_coeffs[Fraction(self.directions_between_mics - approx_index, self.directions_between_mics)]
        else:
            butter_coeffs, cardioid_sample_delay, amplification_numerator = self.butter_coeffs[Fraction(approx_index, self.directions_between_mics)]
        cardioid = (lead_buffer[:self.microphone_array.buffer_size - cardioid_sample_delay] - delaying_buffer[cardioid_sample_delay:])
        if approx_index != 0:
            cardioid = sosfilt(butter_coeffs, cardioid, axis=0) * amplification_numerator
            
        ## DSB high Calculation
        output_high, max_delay = self._cut_dsb_delays(buffer_HP, 0, (self.directions_between_mics*pair_index)+approx_index)

        ## RMS calculation
        return pair_index, approx_index, self._calculate_rms_from_buffer(cardioid), self._calculate_rms_from_buffer(output_high)

    @overrides
    def loop_impl(self, shm_name: str, shm_lock: multiprocessing.synchronize.Lock, buffer_ready_event: multiprocessing.synchronize.Event, 
                             max_rms_indx: multiprocessing.sharedctypes.SynchronizedArray, rms_values: multiprocessing.sharedctypes.SynchronizedArray, 
                             rms_values_updated_event: multiprocessing.synchronize.Event) -> None:
        if self.phi_steps % self.microphone_array.m != 0:
            raise Exception("phi_steps must be dividable by 8 for SourceFinderDSBCardioid.")
        if self.theta_steps != 1:
            raise Exception("SourceFinderDSBCardioid isn't implemented for multiple theta angles. Inicialize it again with theta_steps = 1.")
        computation_process_start_time = time.time()
        # Initialize "pointer" to shared memory, which stores data (buffer) to be computed
        buffer_shm = shared_memory.SharedMemory(name=shm_name)
        indata = np.ndarray((self.microphone_array.buffer_size, self.microphone_array.channels_in), dtype=np.float64, buffer=buffer_shm.buf)

        # Prepare variables to store results of the computation
        rms_values_temp = np.empty((self.theta_steps, self.phi_steps), dtype=np.float64)

        # Analysis tools:
        if self.calculate_average_time:
            average_times = []

        # Begin cycle till the duration is over
        while time.time() - computation_process_start_time < self.duration:
            # Waiting for the audio buffer form callback to be availible
            if not buffer_ready_event.is_set():
                buffer_ready_event.wait()
            else:
                # Start timer measuring the time of the algorithm
                cycle_start_time = time.time()
                # Lock the indata variable so only I can read it to prevent data corruption
                with shm_lock:
                    # Remove DC offset
                    dc_offsets_in = np.mean(indata, axis=0)
                    indata_noDC = indata - dc_offsets_in
                ## Perform highpass and lowpass filter
                # Lowpass butterworth filtration
                buffer_LP = np.empty(indata_noDC.shape)
                for channel in self.microphone_array.active_channels_ids:
                    buffer_LP[:, channel] = sosfilt(self.sos_low, indata_noDC[:, channel], axis=0)

                # Highpass butterworth filtration
                buffer_HP = np.empty(indata_noDC.shape)
                for channel in self.microphone_array.active_channels_ids:
                    buffer_HP[:, channel] = sosfilt(self.sos_high, indata_noDC[:, channel], axis=0)

                for pair_index in range(0, len(self.mic_pairs)):
                    for approx_index in range(0, self.directions_between_mics):
                        if self.regime == DMACompRegime.INTERPOL:
                            output = self._calculate_direction_card_dsb_interpol((
                                buffer_LP,
                                buffer_HP,
                                pair_index,
                                approx_index
                            ))
                        elif self.regime == DMACompRegime.BUTTER:
                            output = self._calculate_direction_card_dsb_butter((
                                buffer_LP,
                                buffer_HP,
                                pair_index,
                                approx_index
                            ))
                        if self.rms_filter == RMSfilter.BOTH:
                            rms_values_temp[0][self.directions_between_mics*pair_index+approx_index] = np.sqrt(np.square(output[2]) + np.square(output[3]))
                        elif self.rms_filter == RMSfilter.LOWPASS_ONLY:
                            rms_values_temp[0][self.directions_between_mics*pair_index+approx_index] = output[2]
                        elif self.rms_filter == RMSfilter.HIGHPASS_ONLY:
                            rms_values_temp[0][self.directions_between_mics*pair_index+approx_index] = output[3]
                max_rms_index = np.unravel_index(np.argmax(rms_values_temp), rms_values_temp.shape)
                # Start timer measuring the time of the algorithm
                cycle_end_time = time.time()
                # Save results to the shared resources
                # ----------- Save results to the shared resources -----------
                with max_rms_indx.get_lock():
                    max_rms_indx[0] = max_rms_index[0]
                    max_rms_indx[1] = max_rms_index[1]
                if self.show_gui:
                    with rms_values.get_lock():
                        np.frombuffer(rms_values.get_obj(), dtype=np.float64)[:] = rms_values_temp
                    rms_values_updated_event.set()
                # ------------------------------------------------------------
                # ----------------------- ANALYSIS PART ----------------------
                total_cycle_time = (cycle_end_time - cycle_start_time) * 1000
                if self.calculate_average_time:
                    average_times.append(total_cycle_time)
                if self.show_time:
                    print(f"Cycle length: {total_cycle_time} ms")
                if self.show_angles:
                    print(f"Theta: {self.theta_angles_deg[max_rms_index[0]]}, Phi: {self.phi_angles_deg[max_rms_index[1]]}")
                # ------------------------------------------------------------
                if self.refreshing_time != 0 and time.time() - cycle_start_time < self.refreshing_time:
                    # When refreshing time is set it is waiting the remaining time it waits the remaining time 
                    # to artificialy slow the algorithm down
                    time.sleep(self.refreshing_time - (time.time() - cycle_start_time) )
                # Send a signal to the callback function that it can prepare next buffer to be processed
                buffer_ready_event.clear()
        
        # ANALYSIS - counting average computing time among the whole run of the algorithm
        if self.calculate_average_time:
            print(f"Average computation time is {sum(average_times) / len(average_times)} ms per cycle")

    @overrides
    def pool_impl(self, shm_name: str, shm_lock: multiprocessing.synchronize.Lock, buffer_ready_event: multiprocessing.synchronize.Event, 
                             max_rms_indx: multiprocessing.sharedctypes.SynchronizedArray, rms_values: multiprocessing.sharedctypes.SynchronizedArray, 
                             rms_values_updated_event: multiprocessing.synchronize.Event) -> None:
        if self.phi_steps % self.microphone_array.m != 0:
            raise Exception("phi_steps must be dividable by 8 for SourceFinderDSBCardioid.")
        if self.theta_steps != 1:
            raise Exception("SourceFinderDSBCardioid isn't implemented for multiple theta angles. Inicialize it again with theta_steps = 1.")
        buffer_shm = shared_memory.SharedMemory(name=shm_name)
        computation_process_start_time = time.time()
        # Initialize "pointer" to shared memory, which stores data (buffer) to be computed
        indata = np.ndarray((self.microphone_array.buffer_size, self.microphone_array.channels_in), dtype=np.float64, buffer=buffer_shm.buf)

        # Prepare variables to store results of the computation
        rms_values_temp = np.empty((self.theta_steps, self.phi_steps), dtype=np.float64)

        index_combinations = [(pair_index, approx_index) 
                                for pair_index in range(0, len(self.mic_pairs))
                                for approx_index in range(0, self.directions_between_mics)]

        # Analysis tools:
        if self.calculate_average_time:
            average_times = []

        with multiprocessing.Pool(initializer=self._init_process, processes=multiprocessing.cpu_count()) as pool:
            # Begin cycle till the duration is over
            while time.time() - computation_process_start_time < self.duration:
                # Waiting for the audio buffer form callback to be availible
                if not buffer_ready_event.is_set():
                    buffer_ready_event.wait()
                else:
                    # Start timer measuring the time of the algorithm
                    cycle_start_time = time.time()
                    # Lock the indata variable so only I can read it to prevent data corruption
                    with shm_lock:
                        # Remove DC offset
                        dc_offsets_in = np.mean(indata, axis=0)
                        indata_noDC = indata - dc_offsets_in
                    ## Perform highpass and lowpass filter
                    # Lowpass butterworth filtration
                    buffer_LP = np.empty(indata_noDC.shape)
                    for channel in self.microphone_array.active_channels_ids:
                        buffer_LP[:, channel] = sosfilt(self.sos_low, indata_noDC[:, channel], axis=0)

                    # Highpass butterworth filtration
                    buffer_HP = np.empty(indata_noDC.shape)
                    for channel in self.microphone_array.active_channels_ids:
                        buffer_HP[:, channel] = sosfilt(self.sos_high, indata_noDC[:, channel], axis=0)

                    args = [(buffer_LP, buffer_HP, *indices) for indices in index_combinations]
                    # Parallelize the computation of the loudest source finder in the processes pool
                    if self.regime == DMACompRegime.INTERPOL:
                        results = pool.map(self._calculate_direction_card_dsb_interpol, args)
                    if self.regime == DMACompRegime.BUTTER:
                        results = pool.map(self._calculate_direction_card_dsb_butter, args)
                    # Extract results from the calculations
                    for result in results:
                        pair_index, approx_index, rms_LP, rms_HP = result
                        # Showing only rms results of highpass or lowpass filter for analysis purposes
                        if self.rms_filter == RMSfilter.BOTH:
                            rms_values_temp[0][self.directions_between_mics*pair_index+approx_index] = np.sqrt(np.square(rms_LP) + np.square(rms_HP))
                        elif self.rms_filter == RMSfilter.LOWPASS_ONLY:
                            rms_values_temp[0][self.directions_between_mics*pair_index+approx_index] = rms_LP
                        elif self.rms_filter == RMSfilter.HIGHPASS_ONLY:
                            rms_values_temp[0][self.directions_between_mics*pair_index+approx_index] = rms_HP
                    
                    max_rms_index = np.unravel_index(np.argmax(rms_values_temp), rms_values_temp.shape)
                    # Start timer measuring the time of the algorithm
                    cycle_end_time = time.time()
                    # ----------- Save results to the shared resources -----------
                    with max_rms_indx.get_lock():
                        max_rms_indx[0] = max_rms_index[0]
                        max_rms_indx[1] = max_rms_index[1]
                    if self.show_gui:
                        with rms_values.get_lock():
                            np.frombuffer(rms_values.get_obj(), dtype=np.float64)[:] = rms_values_temp
                        rms_values_updated_event.set()
                    # ------------------------------------------------------------
                    # ----------------------- ANALYSIS PART ----------------------
                    total_cycle_time = (cycle_end_time - cycle_start_time) * 1000
                    if self.calculate_average_time:
                        average_times.append(total_cycle_time)
                    if self.show_time:
                        print(f"Cycle length: {total_cycle_time} ms")
                    if self.show_angles:
                        print(f"Theta: {self.theta_angles_deg[max_rms_index[0]]}, Phi: {self.phi_angles_deg[max_rms_index[1]]}")
                    # ------------------------------------------------------------
                    if self.refreshing_time != 0 and time.time() - cycle_start_time < self.refreshing_time:
                        # When refreshing time is set it is waiting the remaining time it waits the remaining time 
                        # to artificialy slow the algorithm down
                        time.sleep(self.refreshing_time - (time.time() - cycle_start_time) )
                    # Send a signal to the callback function that it can prepare next buffer to be processed
                    buffer_ready_event.clear()
        
        # ANALYSIS - counting average computing time among the whole run of the algorithm
        if self.calculate_average_time:
            print(f"Average computation time is {sum(average_times) / len(average_times)} ms per cycle")

class AppRegime(Enum):
    DSB_LOOP = 1,
    DSB_POOL = 2,
    DSB_CARD_LOOP = 3,
    DSB_CARD_POOL = 4,
    