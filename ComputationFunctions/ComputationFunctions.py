"""
Source code for all essential functions used along the thesis are defined in this python file. This is to avoid
copying code used in other scripts.
"""
import numpy as np
import time
from enum import Enum
import sounddevice as sd
import matplotlib.pyplot as plt
from fractions import Fraction
from scipy.signal import butter, freqz

# ----------------------------------------------------------------
# ------------------------- SOUNDDEVICE --------------------------
# ----------------------------------------------------------------

def find_device_by_name(target_name: str) -> int:
    """Searches a device with name defined in the parameter.

    :param target_name: Name of the searched device.
    :type target_name: str
    :return: Index of searched device.
    :rtype: int
    """
    devices = sd.query_devices()
    for index, device in enumerate(devices):
        if target_name in device['name']:
            return index
    return None

# ----------------------------------------------------------------
# ------------------------- ENUMERATIONS -------------------------
# ----------------------------------------------------------------

class RMSfilter(Enum):
    """Definition of filter regimes for search of the loudest source finder by cardioid and dsb algorithm.
    """
    LOWPASS_ONLY = 1
    HIGHPASS_ONLY = 2,
    BOTH = 3

class AppRegime(Enum):
    """Definition of algorithm that is being used in current run of the script. 
    """
    DSB_BASIC = 1,
    DSB_ADVANCED = 2,
    CARDIOID_DSB_ADVANCED =  3

class FLSRegime(Enum):
    DSB = 1
    DSB_CARD_INTERPOL = 2
    DSB_CARD_BUTTER = 3

# ----------------------------------------------------------------
# ---------------------- ANALYSIS FUNCTIONS ----------------------
# ----------------------------------------------------------------

def degrees_to_radians(degrees):
    """ Converts degrees to radians """
    return degrees * (np.pi / 180)

def signal_to_decibels_old(signal, reference=1):
    """ Converts signal given in unitless format to decibels """
    decibels = []
    for i in signal:
        decibels.append(10 * np.log10(i / reference))
    return decibels

def signal_to_decibels(signal, reference=1):
    """ Converts signal given in unitless format to decibels """
    decibels = 10 * np.log10(signal / reference)
    return decibels

def quantize_tmi(latencies, sampling_frequency):
    minimum_latency = (1 / sampling_frequency)
    result = []
    for i in latencies:
        result.append(round(i / minimum_latency)*minimum_latency)
    return result

def quantize_tmi_to_samples(latencies: np.ndarray, sampling_frequency: int) -> np.ndarray:
    """TODO: MAKE SURE THAT LATENCIES ARE NOT INPUTED AS LIST
    Takes an array of latencies as an input and returns them back quantized to a given sampling frequency.
    Most common sampling frequencies are 44.1 kHz, 48 kHz and 96 kHz.
    :param latencies: input Latencies ment to be quantized.
    :param sampling_frequency: Sampling frequency for quantization.
    :return: Array of latencies quantized to certain sampling frequency.
    """
    minimum_latency = (1 / sampling_frequency)
    if type(latencies) != np.ndarray:
        print("Warning: quantize_tmi_to_samples - latencies are inputed as list")
        result = []
        for i in latencies:
            result.append(round(i / minimum_latency))
        results = np.array(results)
    else:
        result = np.round(latencies/minimum_latency)
    return result

def cma_tmi(radius: float, M: int, phi_angle: float, theta_angle: float, c: int) -> list:
    """
    Calculates delay times on all microphones in circular microphone array. Results are stored and returned in a list,
    which indices are matching indices of each microphone.
    :param radius: radius of circular microphone array
    :param M: number of microphones in circular microphone array
    :param phi_angle: horizontal angle of a sound source
    :param theta_angle: vertical angle of a sound source
    :param c: speed of sound
    :return: list of delays where every index of the list corresponds to the microphone index delay
    """
    latencies = np.empty(M)
    k_hat_vector = np.array([
        np.sin(degrees_to_radians(theta_angle)) * np.cos(degrees_to_radians(phi_angle)),
        np.sin(degrees_to_radians(theta_angle)) * np.sin(degrees_to_radians(phi_angle)),
        np.cos(degrees_to_radians(theta_angle))
    ])
    # Dot product for k_hat and r_m_i for all mics
    for mic_id in range(M):
        rm_vector = radius * np.array([
            np.cos(2 * np.pi * (mic_id / M)),
            np.sin(2 * np.pi * (mic_id / M)),
            0
        ])
        t_m_i = np.dot(k_hat_vector, rm_vector)/c
        latencies[mic_id] = t_m_i
    # Find smallest t_m_i and adds it to the latencies according to fomrula 1.2
    t_m_c = np.max(latencies)
    latencies = t_m_c - latencies
    return latencies

def calculate_dsb_delays(radius: float, M: int, sampling_frequency: int, number_of_tried_theta: int, number_of_tried_phi: int, c: int, quantize: bool = True, floor: bool = False) -> np.ndarray:
    """This function is used to precalculate all delays needed for delay and sum beamforming for circular microphone delay. 
    This is to save time while performing real time calculations of finding the loudest source.
    It takes number of tried theta and number of tried phi as an argument and according to that it divides the space to equally distanced angles.
    For every theta angle it calculates corresponding number of phi angles. Result is stored and returned in 3 dimensional array,
    where fist dimension is theta angle, second dimension phi anlge and third dimension are individual delays for every microphone according to those parameters.
    When parameter quantized is set to False, it returns delays on each microphone in miliseconds. When quantized is set to false, it returns delays in whole samples.

    TODO: Test this function for multiple theta and M.

    :param radius: Radius of circular microphone array
    :type radius: float
    :param M: Number of microphones in circular microphone array
    :type M: int
    :param sampling_frequency: Sampling frequency
    :type sampling_frequency: int
    :param number_of_tried_theta: Number of theta angles for which the space of (pi, 0) angles should be divided.
    :type number_of_tried_theta: int
    :param number_of_tried_phi: Number of phi angles for which the space of (0, 2*pi) angles should be divided.
    :type number_of_tried_phi: int
    :param c: Speed of sound, usually 343 m/s
    :type c: int
    :return: 3 dimensional array corresponding to theta angle, phi angle and individual delays for each microphone.
    :rtype: np.ndarray
    """
    if quantize or floor:
        all_latencies = np.empty((number_of_tried_theta, number_of_tried_phi, 8), dtype=int)
    else:
        all_latencies = np.empty((number_of_tried_theta, number_of_tried_phi, 8), dtype=np.float64)
    phi_angles = np.linspace(0, 360, number_of_tried_phi, endpoint=False)
    theta_angles = np.linspace(90, 20, number_of_tried_theta) 
    for theta_index in range(0, number_of_tried_theta):
        for phi_index in range(0, number_of_tried_phi):
            # Calculate delays for current direction
            delays = cma_tmi(radius, M, phi_angles[phi_index], theta_angles[theta_index], c)
            if floor:
                quantization_step = 1/sampling_frequency
                all_latencies[theta_index][phi_index] = np.array(np.floor(delays/quantization_step), dtype=int)
            elif quantize:
                all_latencies[theta_index][phi_index] = np.array(quantize_tmi_to_samples(delays, sampling_frequency), dtype=int)
            else:
                all_latencies[theta_index][phi_index] = delays * 1e6
    return all_latencies

def cma_beampattern(theta: float, phi: float, frequency: int, M: int, radius: float, c: int, resolution: int) -> list:
    """
    This function calculates beam pattern of circular microphone array. Beam pattern is calculated in frequency domain,
    therefore frequency as a parameter is required. Returned data are best displayed on polar plot.
    :param theta: vertical angle of a sound source
    :param phi: horizontal angle of a sound source
    :param frequency: frequency of a beam pattern
    :param M: number of microphones in circular microphone array
    :param radius: radius of circular microphone array
    :param c: speed of sound
    :param resolution: number of points that should be generated in range of 0..360 degrees
    :return: list of all magnitudes of the signals dispersed in directions of 0.360 degrees
    """
    omega = 2 * np.pi * frequency
    beampattern = []
    tmcmi = cma_tmi(radius, M, phi, theta, c)
    degrees_resolution = np.linspace(0, 360, resolution)
    # Calculate H_DSB according to formula 1.4
    for phi_s in degrees_resolution:
        kj = cma_tmi(radius, M, phi_s, theta, c)
        sum = 0
        for index in range(0, M):
            sum += np.exp(1j * omega * (tmcmi[index] - kj[index]))
        beampattern.append(sum / M)
    return beampattern

def cma_beampattern_quantization(theta: float, phi: float, frequency: int, M: int, radius: float, c: int, resolution: int, sf: int) -> list:
    """
    This function calculates beam pattern of circular microphone array. Beam pattern is calculated in frequency domain,
    therefore frequency as a parameter is required. Returned data are best displayed on polar plot.
    :param theta: vertical angle of a sound source
    :param phi: horizontal angle of a sound source
    :param frequency: frequency of a beam pattern
    :param M: number of microphones in circular microphone array
    :param radius: radius of circular microphone array
    :param c: speed of sound
    :param resolution: number of points that should be generated in range of 0..360 degrees
    :return: list of all magnitudes of the signals dispersed in directions of 0.360 degrees with applied quantization.
    """
    omega = 2 * np.pi * frequency
    result = []
    tmcmi = quantize_tmi(cma_tmi(radius, M, phi, theta, c), sf)
    degrees_resolution = np.linspace(0, 360, resolution)
    # Calculate H_DSB according to formula 1.4
    for phi_s in degrees_resolution:
        kj = cma_tmi(radius, M, phi_s, theta, c)
        sum = 0
        for index in range(0, M):
            sum += np.exp(1j * omega * (tmcmi[index] - kj[index]))
        result.append(sum / M)
    return result

def cardioid_beampattern(freq: int, mic_dist: float, resolution: int, tau: float = None, c=343):
    """
    fi je uhel dopadu, tj. pro tyto všechny úhly testuji
    omega = 2*pi*f, v tom je schovaná ta moje frekvence
    """
    degrees_resolution = np.linspace(0, 2*np.pi, resolution)
    if tau == None:
        tau = mic_dist / c # mic_dist must be given in meters!
    omega = 2 * np.pi * freq

    beampattern = ((1 - np.exp(-1j * omega * ((mic_dist/c) * np.cos(degrees_resolution) + tau)))
                   /
                   (1 - np.exp(-1j * omega * ((mic_dist/c) + tau))))
    return beampattern

def cardioid_beampattern_quantization(freq: int, mic_dist: float, resolution: int, sf: int = 48000, c:int =343):
    """
    fi je uhel dopadu, tj. pro tyto všechny úhly testuji
    omega = 2*pi*f, v tom je schovaná ta moje frekvence
    """
    q_step = 1/sf
    degrees_resolution = np.linspace(0, 2*np.pi, resolution)
    tau = mic_dist / c
    tau_q = round((tau / q_step)) * q_step # mic_dist must be given in meters!
    omega = 2 * np.pi * freq

    beampattern = ((1 - np.exp(-1j * omega * ((mic_dist/c) * np.cos(degrees_resolution) + tau_q)))
                   /
                   (1 - np.exp(-1j * omega * ((mic_dist/c) + tau_q))))
    return beampattern

def cardioid_tf(phi: int, mic_dist: float, max_frequency=1000, c=343):
    """
    Calculates transfer function for cardioid.
    """
    frequencies = np.arange(20, max_frequency + 1)
    ro_frequencies = 2 * np.pi * frequencies
    tau = mic_dist / c

    ts = 1 - np.exp(-1j * ro_frequencies * ((mic_dist/c) * np.cos(phi) + tau))

    return ts

def cardioid_tf_quantized(phi: int, mic_dist: float, max_frequency=1000, sf: int = 48000, c=343):
    """
    Calculates transfer function for cardioid.
    """
    q_step = 1/sf
    frequencies = np.arange(20, max_frequency + 1)
    ro_frequencies = 2 * np.pi * frequencies
    tau = mic_dist / c
    tau_q = round((tau / q_step)) * q_step

    ts = 1 - np.exp(-1j * ro_frequencies * ((mic_dist/c) * np.cos(phi) + tau_q))

    return ts

def find_most_similar_frequency(ratios, fs, a, b, max_frequency=1000):
    """
    Najde frekvenci, na které je frekvenční odezva Butterworthova filtru nejvíce podobná danému poli ratios.
    TODO

    Parametry:
    ratios (numpy.ndarray): Pole hodnot pro porovnání s frekvenční odezvou.
    fs (int): Vzorkovací frekvence.
    a (int): Dolní hranice intervalu frekvencí pro testování.
    b (int): Horní hranice intervalu frekvencí pro testování.
    n_points (int): Počet frekvenčních bodů v intervalu od a do b.
    
    Vrací:
    float: Frekvence, na které byla nalezena největší podobnost.
    """
    n_points = b - a  + 1
    frequencies = np.linspace(a, b, n_points)
    best_frequency = a
    lowest_mae = np.inf
    
    for cutoff in frequencies:
        # Butterworth filter order
        order = 1
        # Filter design
        b, a = butter(order, cutoff / (0.5 * fs), btype='low', analog=False)
        # Frequency response
        w, h = freqz(b, a, worN=np.linspace(20, max_frequency, max_frequency - 19) * 2*np.pi / fs)
        magnitude = np.abs(h) * ratios[0]

        mean = np.mean(np.abs((ratios - magnitude[:len(ratios)])))
        if mean < lowest_mae:
            lowest_mae = mean
            best_frequency = cutoff

    return best_frequency

def cardioid_tf_quantized_filtered(phi: int, mic_dist: float, filter_coeffs: np.ndarray, max_frequency=1000, sf: int = 48000, c=343):
    """
    Calculates transfer function for cardioid.
    """
    q_step = 1/sf
    frequencies = np.arange(20, max_frequency + 1)
    ro_frequencies = 2 * np.pi * frequencies
    tau = mic_dist / c
    tau_q = round((tau / q_step)) * q_step

    ts = filter_coeffs * (1 - np.exp(-1j * ro_frequencies * ((mic_dist/c) * np.cos(phi) + tau_q)))
    return ts

def cardioid_tf_weighting(phi: int, mic_dist: float, tau_add: float, max_frequency=1000, sf: int = 48000, c=343):
    print(tau_add)
    q_step = 1/sf
    frequencies = np.arange(20, max_frequency + 1)
    ro_frequencies = 2 * np.pi * frequencies
    tau = mic_dist / c
    tau_q = (round((tau / q_step)) * q_step) + tau_add
    print(f"tau_q_with addition: {tau_q}")

    ts = 1 - np.exp(-1j * ro_frequencies * ((mic_dist/c) * np.cos(phi) + tau_q))

    return ts

def cardioid_tf_tau(phi: int, mic_dist: float, tau: float, max_frequency=1000, sf: int = 48000, c=343):
    frequencies = np.arange(20, max_frequency + 1)
    ro_frequencies = 2 * np.pi * frequencies

    ts = 1 - np.exp(-1j * ro_frequencies * ((mic_dist/c) * np.cos(phi) + tau))

    return ts

def approximation_radius(circle_radius: float, numenator: int, denominator: int):
    """This function calculates distance between 2 mics that are approximated. 

    :param circle_radius: _description_
    :type circle_radius: float
    :param approximation_ratio: _description_
    :type approximation_ratio: str
    """
    # Distance between 2 neighbouring microphones. Calculated using law of cosines formula (c^2 = a^2 + b^2 -2*a*b**cos(gamma))
    dist_between_mics = np.sqrt(circle_radius**2 + circle_radius**2 -2*(circle_radius**2)*np.cos(np.pi/4))
    my_distance = numenator/denominator * dist_between_mics
    return np.sqrt((my_distance)**2 + circle_radius**2 - 2*circle_radius*my_distance*np.cos(3*np.pi/8)) # np.radians(67.5))

def tau_compensation(delta_norm: float, delta_approx: float, sf: int = 48000, quantize_tau: bool = True, c: int = 343) -> float:
    """This function calculates the compensation of shorter distance between physical (delta) mics and approximated
    mics (delta_2). It is done by calculating heigher delay tau that should be added to the approximated delaying
    mic. It returns compensated tau for the delaying approximated mic.

    :param delta_norm: _description_
    :type delta_norm: float
    :param delta_approx: _description_
    :type delta_approx: float
    :param sf: _description_, defaults to 48000
    :type sf: int, optional
    :param quantize_tau: _description_, defaults to True
    :type quantize_tau: bool, optional
    :param c: _description_, defaults to 343
    :type c: int, optional
    :return: _description_
    :rtype: float
    """
    tau_norm = delta_norm/c
    if quantize_tau:
        q_step = 1/sf
        tau_norm = round(tau_norm / q_step) * q_step
    tau_approx = (delta_approx * tau_norm) / delta_norm
    difference = tau_norm - tau_approx
    tau_approx_compensated = difference + tau_norm
    return tau_approx_compensated

# not used
def are_arrays_within_bounds(arr1, arr2, m):
    """TODO
    Kontroluje, zda jsou všechny prvky v arr2 v mezích arr1 +/- m.
    
    Parametry:
        arr1 (numpy.array): První pole hodnot.
        arr2 (numpy.array): Druhé pole hodnot, které se porovnává.
        m (float): Tolerance pro porovnání hodnot.
    
    Návrat:
        bool: True, pokud jsou všechny prvky arr2 v mezích arr1 +/- m, jinak False.
    """
    lower_bound = arr1 - m
    upper_bound = arr1 + m
    within_bounds = np.logical_and(arr2 >= lower_bound, arr2 <= upper_bound)
    return np.all(within_bounds)

# ----------------------------------------------------------------
# ---------------------- BUFFER OPERATIONS -----------------------
# ----------------------------------------------------------------

def calculate_rms_from_buffer(audio_buffer: np.ndarray) -> int:
    """Calculates RMS value from given buffer in dimensionles values.

    :param audio_buffer: Audio buffer from which rms should be calculated.
    :type audio_buffer: np.ndarray
    :return: RMS value of current buffer
    :rtype: int
    """
    rms_value = np.sqrt(np.mean(audio_buffer**2))
    return rms_value

def delay_signal_by_frames(sample_delay: int, input_buffer: np.ndarray, previous_buffer: np.ndarray) -> np.ndarray:
    """This function takes in previous buffer and uses it to delay data in the current "input_buffer" buffer.
    Signal cannot be delayed by more samples than the size of the buffer. If previous buffer is not defined,
    it uses 0 to fill the delay samples.

    :param sample_delay: Number of samples by which a signal should be delayed.
    :type sample_deay: int
    :param input_buffer: Buffer on which the delays should be applied
    :type input_buffer: np.ndarray
    :param previous_buffer: Previous input buffer used to get samples for delay
    :type delay_buffer: np.ndarray
    :return: Delayed signal
    :rtype: np.ndarray
    """
    if sample_delay == 0:
        return input_buffer
    history = previous_buffer[-sample_delay:]
    delayed_signal = np.concatenate((history, input_buffer))
    delayed_signal = delayed_signal[:-sample_delay]
    return delayed_signal

def delay_signal_interpolation(audio_buffer: np.ndarray, delay_time_microseconds: float, sample_rate: int,
                                previous_buffer: np.ndarray = None, additional_sample_delay: int = None) -> np.ndarray:
    """This function creates delay smaller than a minimum quantization step caused by sampling frequency. 
    This is done by doing linear interpolation of signal for selected amounth of microseconds. User can also
    specify additional delay by whole samples in last argument of the function (optional).
    
    Make sure to notice, that for a linear interpolation a to be done properly, a previous samples from the current buffer
    are needed to be known. If previous buffer is not specified, a zero at the start is NOT DEFINED sample. If delay 
    exeeds quantization step, all zeros before the "not defined" samples are considered to be delay. This effect is 
    removed if previous buffer is defined.
    
    :param audio_buffer: _description_
    :type audio_buffer: _type_
    :param delay_time_microseconds: _description_
    :type delay_time_microseconds: _type_
    :param sample_rate: _description_
    :type sample_rate: _type_
    :param additional_sample_delay: _description_, defaults to None
    :type additional_sample_delay: _type_, optional
    :return: _description_
    :rtype: _type_
    """
    # Checking if delay is smaller than quantization step. If so, additional delay in whole samples is applied.
    eta_raw = delay_time_microseconds * 1e-6 * sample_rate
    sample_delay = int(eta_raw)
    # print(f"frac_delay_raw: {frac_delay_raw}")
    # print(f"sample_delay: {sample_delay}")

    # Adding more whole samples delay if user specified any.
    if additional_sample_delay != None:
        sample_delay += additional_sample_delay

    # Calculating a fraction of a quantization step by which the samples should be delayed
    eta = eta_raw - sample_delay
    # print(f"frac_delay: {eta}")

    # Buffer for storing resulting signal
    delayed_buffer = np.zeros_like(audio_buffer)
    
    # Definition of supporting indices arrays to enable vectorization in numpy. This is to achieve a better performance.
    original_indices = np.arange(sample_delay + 1, len(audio_buffer))
    delayed_indices = original_indices - sample_delay
    # print(f"original indices: {original_indices}, delayed_indices: {delayed_indices}")

    # Linear interpolation
    delayed_buffer[original_indices] = (1 - eta) * audio_buffer[delayed_indices - 1] + eta * audio_buffer[delayed_indices]

    return delayed_buffer

def apply_dsb_delays_on_buffer(input_buffer: np.ndarray, previous_buffer: np.ndarray, theta_index: int, phi_index: int, all_delays: np.ndarray) -> np.ndarray:
    """This function takes 16 channel audio with 8 active channels as an input and applies delays that are stored in all_delays variable.

    :param input_buffer: Purrent multichannel buffer on which delays should be applied
    :type input_buffer: np.ndarray
    :param previous_buffer: Previous multichannel buffer which is used for applying delays 
    :type previous_buffer: np.ndarray
    :param theta_index: Index identifying theta angle in all_delays array
    :type theta_index: int
    :param phi_index: Index identifying phi angle in all_delays array
    :type phi_index: int
    :param all_delays: Array in which the latencies are stored. This is to save computation time of calculating delays in every function call.
    :type all_delays: np.ndarray
    :return: 16 channel audio buffer 
    :rtype: np.ndarray
    """
    delayed_buffer = np.empty((input_buffer.shape[0], 8))
    delayed_buffer[:, 0] = delay_signal_by_frames(all_delays[theta_index][phi_index][4], input_buffer[:, 8], previous_buffer[:, 8])
    delayed_buffer[:, 1] = delay_signal_by_frames(all_delays[theta_index][phi_index][5], input_buffer[:, 0], previous_buffer[:, 0])
    delayed_buffer[:, 2] = delay_signal_by_frames(all_delays[theta_index][phi_index][6], input_buffer[:, 9], previous_buffer[:, 9])
    delayed_buffer[:, 3] = delay_signal_by_frames(all_delays[theta_index][phi_index][7], input_buffer[:, 1], previous_buffer[:, 1])
    delayed_buffer[:, 4] = delay_signal_by_frames(all_delays[theta_index][phi_index][0], input_buffer[:, 10], previous_buffer[:, 10])
    delayed_buffer[:, 5] = delay_signal_by_frames(all_delays[theta_index][phi_index][1], input_buffer[:, 2], previous_buffer[:, 2])
    delayed_buffer[:, 6] = delay_signal_by_frames(all_delays[theta_index][phi_index][2], input_buffer[:, 11], previous_buffer[:, 11])
    delayed_buffer[:, 7] = delay_signal_by_frames(all_delays[theta_index][phi_index][3], input_buffer[:, 3], previous_buffer[:, 3])
    # Add delayed signals together
    added_signals = np.sum(delayed_buffer, axis=1) / 8
    return added_signals 

def cut_dsb_delays_on_buffer(input_buffer: np.ndarray, theta_index: int, phi_index: int, all_delays_q: np.ndarray, 
                             additional_interpol_delays: np.ndarray = None, sample_rate: int = 48000) -> tuple[np.ndarray, int]:
    """This function takes 16 channel audio with 8 active channels as an input and applies delays that are stored in all_delays variable.
    In contrast to function apply_delays_on_buffer is this one not using a previous buffer to calculate resulting signal. This is to optimize
    algorithm for loudest source finder which does not need an audio output to be continuous. It just needs to calculate RMS from "some" signal
    sample.
    TODO: interpolate_delays
    
    :param input_buffer: Purrent multichannel buffer on which delays should be applied
    :type input_buffer: np.ndarray
    :param previous_buffer: Previous multichannel buffer which is used for applying delays 
    :type previous_buffer: np.ndarray
    :param theta_index: Index identifying theta angle in all_delays array
    :type theta_index: int
    :param phi_index: Index identifying phi angle in all_delays array
    :type phi_index: int
    :param all_delays: Array in which the latencies are stored. This is to save computation time of calculating delays in every function call.
    :type all_delays: np.ndarray
    :return: 16 channel audio buffer 
    :rtype: np.ndarray
    """
    # V této modifikaci chci vpodstatě jenom useknout zpoždění od těch nijak nezpožděných
    max_delay = np.max(all_delays_q[theta_index][phi_index])
    delayed_buffer = np.empty((input_buffer.shape[0] - max_delay, 8))
    # print(f"max_delay: {max_delay}")
    delayed_buffer[:, 0] = input_buffer[:, 8][max_delay - all_delays_q[theta_index][phi_index][4] : input_buffer.shape[0] - all_delays_q[theta_index][phi_index][4]]
    delayed_buffer[:, 1] = input_buffer[:, 0][max_delay - all_delays_q[theta_index][phi_index][5] : input_buffer.shape[0] - all_delays_q[theta_index][phi_index][5]]
    delayed_buffer[:, 2] = input_buffer[:, 9][max_delay - all_delays_q[theta_index][phi_index][6] : input_buffer.shape[0] - all_delays_q[theta_index][phi_index][6]]
    delayed_buffer[:, 3] = input_buffer[:, 1][max_delay - all_delays_q[theta_index][phi_index][7] : input_buffer.shape[0] - all_delays_q[theta_index][phi_index][7]]
    delayed_buffer[:, 4] = input_buffer[:, 10][max_delay - all_delays_q[theta_index][phi_index][0] : input_buffer.shape[0] - all_delays_q[theta_index][phi_index][0]]
    delayed_buffer[:, 5] = input_buffer[:, 2][max_delay - all_delays_q[theta_index][phi_index][1] : input_buffer.shape[0] - all_delays_q[theta_index][phi_index][1]]
    delayed_buffer[:, 6] = input_buffer[:, 11][max_delay - all_delays_q[theta_index][phi_index][2] : input_buffer.shape[0] - all_delays_q[theta_index][phi_index][2]]
    delayed_buffer[:, 7] = input_buffer[:, 3][max_delay - all_delays_q[theta_index][phi_index][3] : input_buffer.shape[0] - all_delays_q[theta_index][phi_index][3]]
    quantization_step = 1/sample_rate
    if additional_interpol_delays is not None:
        """
        Interpolace bude probíhat tak, že nejdříve zpozdím signál o odpovídající mikrosekundy (v rámci kvantizačního kroku), 
        který získám tak, že udělám modulo quantization_step. Jakmile signál budu mít takto zpožděný, tak můžu začít osekávat (cutovat) 
        ty buffery, aby odpovídali zpoždění. Zpoždění ve vzorcích pak musí být zaokrouhlené dolů (funkcí floor).

        Z toho plyne, že tady není důvod k tomu, proč interpolační zpoždění neudávat již nastavené na quantization step.
        """
        delayed_buffer_1 = np.empty((input_buffer.shape[0] - (max_delay + 1), 8))
        # Additional interpolation is done in the in addition to the sample delay. 
        # Sample delays have to be quantized by using floor function, not round!
        delayed_buffer_1[:, 0] = delay_signal_interpolation(delayed_buffer[:, 0], additional_interpol_delays[theta_index][phi_index][4] % quantization_step, sample_rate)[1:]
        delayed_buffer_1[:, 1] = delay_signal_interpolation(delayed_buffer[:, 1], additional_interpol_delays[theta_index][phi_index][5] % quantization_step, sample_rate)[1:]
        delayed_buffer_1[:, 2] = delay_signal_interpolation(delayed_buffer[:, 2], additional_interpol_delays[theta_index][phi_index][6] % quantization_step, sample_rate)[1:]
        delayed_buffer_1[:, 3] = delay_signal_interpolation(delayed_buffer[:, 3], additional_interpol_delays[theta_index][phi_index][7] % quantization_step, sample_rate)[1:]
        delayed_buffer_1[:, 4] = delay_signal_interpolation(delayed_buffer[:, 4], additional_interpol_delays[theta_index][phi_index][0] % quantization_step, sample_rate)[1:]
        delayed_buffer_1[:, 5] = delay_signal_interpolation(delayed_buffer[:, 5], additional_interpol_delays[theta_index][phi_index][1] % quantization_step, sample_rate)[1:]
        delayed_buffer_1[:, 6] = delay_signal_interpolation(delayed_buffer[:, 6], additional_interpol_delays[theta_index][phi_index][2] % quantization_step, sample_rate)[1:]
        delayed_buffer_1[:, 7] = delay_signal_interpolation(delayed_buffer[:, 7], additional_interpol_delays[theta_index][phi_index][3] % quantization_step, sample_rate)[1:]
        
        added_signals = np.sum(delayed_buffer_1, axis=1) / 8
        return (added_signals, max_delay + 1)
    else:
        added_signals = np.sum(delayed_buffer, axis=1) / 8
        return (added_signals, max_delay)

# ----------------------------------------------------------------
# ------------------ SOURCE FINDER ALGORITHMS --------------------
# ----------------------------------------------------------------

def prepare_interpol_delays(radius: float, number_of_tried_phi: int, m: int, c: int = 343, sf: int = 48000):
    directions_between_mics = int(number_of_tried_phi/m)
    quantization_step = 1/sf
    interpolation_delays = {}
    my_tau = radius*2/c
    my_sample_delay = int(np.floor(my_tau/quantization_step))
    delay_in_ms = (my_tau % quantization_step) * 1000000
    interpolation_delays[Fraction(0, directions_between_mics)] = (my_sample_delay, delay_in_ms) # physical microphone
    for i in range(1, int(np.floor(directions_between_mics/2)) + 1):
        distance = approximation_radius(radius, i, directions_between_mics) * 2
        tau_compensated = tau_compensation(radius*2, distance)
        delay_in_samples = int(np.floor(tau_compensated/quantization_step))
        remaining_delay_in_ms = (tau_compensated % quantization_step) * 1000000
        interpolation_delays[Fraction(i, directions_between_mics)] = (delay_in_samples, remaining_delay_in_ms)
    
    return interpolation_delays

def prepare_butter_coeffs(radius: float, cutoff_freq: int, number_of_tried_phi: int, m: int, c:int = 343, sf:int = 48000, butter_output = 'sos'):
    """
    Goal of following lines of code is to create a dictionary FILTER_COEFFS_AND_AMPLIFIER, that contains 
    butterworth filter a and b coefficients together with delay in samples and amplification numerator of the 
    whole signal. As keys to this dicrionary are set the fractions representing distance of approximated 
    microphone between 2 physical microphones.

    Frequencies to the butterworth filter are set manualy for each of the fractions. In further development,
    more accurate calculations can be added.

    Amplification numerators are determined by averaging the difference between transfer function of physical
    microphones and approximated microphones among the whole spectrum of the cardioid (for lowpass filtered singal
    at 1 kHz in our context). Amplification numerator tells us, by how much do we need to amplify the signal of 
    approximated microphones in order to reach the levels of the physical microphones (e.g. 1.079 times.).
    """
    directions_between_mics = int(number_of_tried_phi/m)
    # Manualy set frequencies based of the look of the curve
    cutoff_frequencies = {
        Fraction(1, 2) : 2280,
        Fraction(1, 3) : 2400,
        Fraction(1, 4) : 2450,
        Fraction(1, 5) : 2750,
        Fraction(2, 5) : 2650,
        # Fraction(1, 6) : 0,
        # Fraction(1, 7) : 0,
        # Fraction(2, 7) : 0,
        # Fraction(3, 7) : 0,
        Fraction(1, 8) : 4850,
        Fraction(3, 8) : 2300
    }
    # Transfer function of the physical microphones
    tf_main_amplitudes = np.abs(cardioid_tf_quantized(0, radius*2, cutoff_freq))
    filter_coeffs_and_amplifier = {}
    filter_coeffs_and_amplifier[Fraction(0, directions_between_mics)] = (None, int(np.floor(((radius*2/c)*sf))), 1.08)
    for fraction, cutoff_freq_f in cutoff_frequencies.items():
        # Distance between approximated microhphones
        distance = approximation_radius(radius, fraction.numerator, fraction.denominator) * 2
        # Transfer function of approximated microphones
        tf_approx_amplitudes = np.abs(cardioid_tf_quantized(0, distance, cutoff_freq))
        # Caclulation of ratio between physical and approximation microphones curve
        ratios_curve = np.abs(tf_main_amplitudes/tf_approx_amplitudes)
        # Determining the amplification numerator 
        amplification_numerator = ratios_curve[50] # + 0.02
        # Calculation of delay in samples
        delay_in_samples = round((distance/c)*sf)
        # Adding results to the dictionary
        filter_coeffs_and_amplifier[fraction] = (butter(1, cutoff_freq_f, fs=sf, btype='low', output=butter_output), delay_in_samples, amplification_numerator)
    print(filter_coeffs_and_amplifier)
    return filter_coeffs_and_amplifier

# ----------------------------------------------------------------
# ----------------------------- GUI ------------------------------
# ----------------------------------------------------------------

def gui_thread(rms_values_queue: np.ndarray, duration: int, refresh_time: float, number_of_tried_phi: int):
    start_time = time.time()
    # Initializing phi_radians with 2*pi at the end to close the polar diagram
    phi_radians = np.linspace(0, 2*np.pi, number_of_tried_phi, endpoint=False)
    phi_radians = np.append(phi_radians, np.array([2*np.pi]))

    # Set interactive mode
    plt.ion() 
    # Set plot
    fig = plt.figure()
    ax = fig.add_subplot(111, polar=True)
    ax.set_ylim([-30, 0])
    ax.set_theta_direction(-1)
    arrow = ax.annotate('', xy=(phi_radians[0], -10), 
                        xytext=(phi_radians[0], -30), 
                        arrowprops=dict(color='red', linewidth=2, arrowstyle='->'))
    
    line, = ax.plot(phi_radians, np.full(number_of_tried_phi + 1, -10), linestyle='-', color='b')
    plt.pause(0.1)

    while time.time() - start_time < duration:
        if not rms_values_queue.empty():
            rms_values = rms_values_queue.get_nowait()[0]
            rms_decibels = signal_to_decibels(rms_values)
            rms_decibels_polar = np.append(rms_decibels, rms_decibels[0]) # Adding first element at the end to close the polar plot
            upper_limit = np.max(rms_decibels)
            upper_limit_index = np.argmax(rms_decibels)
            bottom_limit = np.min(rms_decibels)
            line.set_ydata(rms_decibels_polar)
            ax.set_ylim([bottom_limit, upper_limit])
            arrow.remove()
            arrow = ax.annotate('', xy=(phi_radians[upper_limit_index], upper_limit), 
                    xytext=(phi_radians[upper_limit_index], bottom_limit), 
                    arrowprops=dict(color='red', linewidth=2, arrowstyle='->'))
            plt.draw()
            plt.pause(refresh_time)
        else:
            time.sleep(refresh_time)