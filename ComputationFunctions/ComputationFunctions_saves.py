import numpy as np

def delay_signal_interpolation_old(audio_buffer, delay_time_microseconds, sample_rate, additional_samples):
    delay_samples = delay_time_microseconds * 1e-6 * sample_rate
    
    # Celkové zpoždění zahrnuje i další vzorky
    total_delay_samples = delay_samples + additional_samples
    
    # Vypočítáme celé a zlomkové části celkového zpoždění
    int_delay = int(total_delay_samples)
    frac_delay = total_delay_samples - int_delay

    # Vytvoříme nový buffer, který bude obsahovat zpožděný signál
    delayed_buffer = np.zeros_like(audio_buffer)
    
    # Provedeme lineární interpolaci
    for i in range(int_delay + 1, len(audio_buffer)):
        delayed_buffer[i] = (1 - frac_delay) * audio_buffer[i - int_delay - 1] + frac_delay * audio_buffer[i - int_delay]

    return delayed_buffer

def delay_signal_interpolation_not_vectorized(audio_buffer, delay_time_microseconds, sample_rate, additional_sample_delay = None):
    """This function creates delay smaller than a minimum quantization step caused by sampling frequency. 
    This is done by doing linear interpolation of signal for selected amounth of microseconds. User can also
    specify additional delay by whole samples in last argument of the function (optional).

    :param audio_buffer: _description_
    :type audio_buffer: _type_
    :param delay_time_microseconds: _description_
    :type delay_time_microseconds: _type_
    :param sample_rate: _description_
    :type sample_rate: _type_
    :param additional_sample_delay: _description_, defaults to None
    :type additional_sample_delay: _type_, optional
    :return: _description_
    :rtype: _type_
    """
    # Checking if delay is smaller than quantization step. If so, additional delay in whole samples is applied.
    sample_delay_microseconds = delay_time_microseconds * 1e-6 * sample_rate
    sample_delay = int(sample_delay_microseconds)

    # Adding more whole samples delay if user specified any.
    if additional_sample_delay != None:
        sample_delay += additional_sample_delay

    # Calculating a fraction of a quantization step by which the samples should be delayed
    frac_delay = sample_delay_microseconds - sample_delay

    # Buffer for storing resulting signal
    delayed_buffer = np.zeros_like(audio_buffer)
    
    # Linear interpolation
    for i in range(sample_delay + 1, len(audio_buffer)):
        delayed_buffer[i] = (1 - frac_delay) * audio_buffer[i - sample_delay - 1] + frac_delay * audio_buffer[i - sample_delay]

    return delayed_buffer

def cut_delays_on_buffer_old(input_buffer: np.ndarray, theta_index: int, phi_index: int, all_delays: np.ndarray, M: int) -> np.ndarray:
    """This function takes 16 channel audio with 8 active channels as an input and applies delays that are stored in all_delays variable.
    In contrast to function apply_delays_on_buffer is this one not using a previous buffer to calculate resulting signal. This is to optimize
    algorithm for loudest source finder which does not need an audio output to be continuous. It just needs to calculate RMS from "some" signal
    sample.

    :param input_buffer: Purrent multichannel buffer on which delays should be applied
    :type input_buffer: np.ndarray
    :param previous_buffer: Previous multichannel buffer which is used for applying delays 
    :type previous_buffer: np.ndarray
    :param theta_index: Index identifying theta angle in all_delays array
    :type theta_index: int
    :param phi_index: Index identifying phi angle in all_delays array
    :type phi_index: int
    :param all_delays: Array in which the latencies are stored. This is to save computation time of calculating delays in every function call.
    :type all_delays: np.ndarray
    :param M: Number of active channels (i. e. channels where microphones are connected)
    :type M: int
    :param gain: Gain of the output signal, i.e. how much it should be made louder or quiter., defaults to 1
    :type gain: int, optional
    :return: 16 channel audio buffer 
    :rtype: np.ndarray
    """
    # V této modifikaci chci vpodstatě jenom useknout zpoždění od těch nijak nezpožděných
    max_delay = np.max(all_delays[theta_index][phi_index])
    delayed_ch1 = input_buffer[:, 8][max_delay - all_delays[theta_index][phi_index][4] : input_buffer.shape[0] - all_delays[theta_index][phi_index][4]]
    delayed_ch2 = input_buffer[:, 0][max_delay - all_delays[theta_index][phi_index][5] : input_buffer.shape[0] - all_delays[theta_index][phi_index][5]]
    delayed_ch3 = input_buffer[:, 9][max_delay - all_delays[theta_index][phi_index][6] : input_buffer.shape[0] - all_delays[theta_index][phi_index][6]]
    delayed_ch4 = input_buffer[:, 1][max_delay - all_delays[theta_index][phi_index][7] : input_buffer.shape[0] - all_delays[theta_index][phi_index][7]]
    delayed_ch5 = input_buffer[:, 10][max_delay - all_delays[theta_index][phi_index][0] : input_buffer.shape[0] - all_delays[theta_index][phi_index][0]]
    delayed_ch6 = input_buffer[:, 2][max_delay - all_delays[theta_index][phi_index][1] : input_buffer.shape[0] - all_delays[theta_index][phi_index][1]]
    delayed_ch7 = input_buffer[:, 11][max_delay - all_delays[theta_index][phi_index][2] : input_buffer.shape[0] - all_delays[theta_index][phi_index][2]]
    delayed_ch8 = input_buffer[:, 3][max_delay - all_delays[theta_index][phi_index][3] : input_buffer.shape[0] - all_delays[theta_index][phi_index][3]]
    added_signals = np.empty((4800 - max_delay, 1))
    # Add delayed signals together
    for i in range(0, len(delayed_ch1)):
        added_signals[i] = (delayed_ch1[i] + 
                            delayed_ch2[i] + 
                            delayed_ch3[i] + 
                            delayed_ch4[i] + 
                            delayed_ch5[i] + 
                            delayed_ch6[i] + 
                            delayed_ch7[i] + 
                            delayed_ch8[i]) / M
    return added_signals 

def apply_delays_on_buffer_old(input_buffer: np.ndarray, previous_buffer: np.ndarray, theta_index: int, phi_index: int, all_delays: np.ndarray, M: int, gain: int = 1) -> np.ndarray:
    """This function takes 16 channel audio with 8 active channels as an input and applies delays that are stored in all_delays variable.

    :param input_buffer: Purrent multichannel buffer on which delays should be applied
    :type input_buffer: np.ndarray
    :param previous_buffer: Previous multichannel buffer which is used for applying delays 
    :type previous_buffer: np.ndarray
    :param theta_index: Index identifying theta angle in all_delays array
    :type theta_index: int
    :param phi_index: Index identifying phi angle in all_delays array
    :type phi_index: int
    :param all_delays: Array in which the latencies are stored. This is to save computation time of calculating delays in every function call.
    :type all_delays: np.ndarray
    :param M: Number of active channels (i. e. channels where microphones are connected)
    :type M: int
    :param gain: Gain of the output signal, i.e. how much it should be made louder or quiter., defaults to 1
    :type gain: int, optional
    :return: 16 channel audio buffer 
    :rtype: np.ndarray
    """
    delayed_ch1 = delay_signal_by_frames(all_delays[theta_index][phi_index][4], input_buffer[:, 8], previous_buffer[:, 8])
    delayed_ch2 = delay_signal_by_frames(all_delays[theta_index][phi_index][5], input_buffer[:, 0], previous_buffer[:, 0])
    delayed_ch3 = delay_signal_by_frames(all_delays[theta_index][phi_index][6], input_buffer[:, 9], previous_buffer[:, 9])
    delayed_ch4 = delay_signal_by_frames(all_delays[theta_index][phi_index][7], input_buffer[:, 1], previous_buffer[:, 1])
    delayed_ch5 = delay_signal_by_frames(all_delays[theta_index][phi_index][0], input_buffer[:, 10], previous_buffer[:, 10])
    delayed_ch6 = delay_signal_by_frames(all_delays[theta_index][phi_index][1], input_buffer[:, 2], previous_buffer[:, 2])
    delayed_ch7 = delay_signal_by_frames(all_delays[theta_index][phi_index][2], input_buffer[:, 11], previous_buffer[:, 11])
    delayed_ch8 = delay_signal_by_frames(all_delays[theta_index][phi_index][3], input_buffer[:, 3], previous_buffer[:, 3])
    added_signals = np.empty((4800, 1))
    # Add delayed signals together
    for i in range(0, len(delayed_ch1)):
        added_signals[i] = (delayed_ch1[i] + 
                            delayed_ch2[i] + 
                            delayed_ch3[i] + 
                            delayed_ch4[i] + 
                            delayed_ch5[i] + 
                            delayed_ch6[i] + 
                            delayed_ch7[i] + 
                            delayed_ch8[i]) / M
        added_signals[i] *= gain
    return added_signals 

# interpolation description:
"""
Na indexu 0 v delayed_buffer budu mít vždycky 0, která mi tam přebude z toho np.zero_like.
Z toho plyne, že řeším pouze lineární interpolaci od indexu 1. Proto mám v tom original_indices 
to + 1.  

První prvek toho mého arraye bude tedy vždycky NEDEFINOVANÝ. Není tam žádné zpoždění nebo tak něco...

Pokud je zpoždění větší než můj je vzorkovací zaokrouhlení, tak mám zpoždění o n sampů + ta jedna nedefinovaná 0.
"""