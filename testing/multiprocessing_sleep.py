import multiprocessing
import time

def worker():
    print("Worker starts")
    time.sleep(2)
    print("Worker finished")

if __name__ == "__main__":
    p = multiprocessing.Process(target=worker)
    p.start()
    p.join()  # Čeká, až proces 'p' dokončí svou práci
    print("All processes have finished.")
