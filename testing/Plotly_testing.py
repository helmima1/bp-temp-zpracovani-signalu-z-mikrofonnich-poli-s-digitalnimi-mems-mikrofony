import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import numpy as np
import time

app = dash.Dash(__name__)

# Definování layoutu aplikace
app.layout = html.Div([
    dcc.Graph(id='live-polar-plot'),
    dcc.Interval(
            id='interval-component',
            interval=0.5*1000,  # v milisekundách (5 sekund)
            n_intervals=0
    )
])

# Callback pro aktualizaci grafu
@app.callback(
    Output('live-polar-plot', 'figure'),
    Input('interval-component', 'n_intervals')
)
def update_polar_plot(n):
    theta = np.linspace(0, 2*np.pi, 100)
    r = np.abs(np.sin(theta * 2 * n))  # dynamicky měnící se data

    trace = go.Scatterpolar(
        r = r,
        theta = np.degrees(theta),
        mode = 'lines'
    )

    layout = go.Layout(
        polar = dict(
            radialaxis = dict(
                visible = True,
                range = [0, 1]
            )
        ),
        showlegend = False
    )

    return {'data': [trace], 'layout': layout}

if __name__ == '__main__':
    app.run_server(debug=True)
