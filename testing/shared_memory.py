import numpy as np
import time
from multiprocessing import shared_memory, Process, Event, Lock

def producer(shm_name, shape, event, lock, num_cycles):
    existing_shm = shared_memory.SharedMemory(name=shm_name)
    np_array = np.ndarray(shape, dtype=np.float64, buffer=existing_shm.buf)
    
    for i in range(num_cycles):
        with lock:  # Zajištění exkluzivního přístupu k sdílené paměti
            np_array[:] = np.random.rand(*shape)
            print(f"Producer: Data generated and written to shared memory. {np_array[0][0:5]}")
        
        event.set()  # Signalizace, že data jsou připravena
        print(f"Producer: Data ready at cycle {i + 1}.")
        time.sleep(1)
    
    with lock:
        np_array[:] = np.nan  # Signal konce
    event.set()
    print("Producer: Finished producing data.")
    
    existing_shm.close()

def consumer(shm_name, shape, event, lock):
    existing_shm = shared_memory.SharedMemory(name=shm_name)
    np_array = np.ndarray(shape, dtype=np.float64, buffer=existing_shm.buf)
    
    while True:
        event.wait()
        event.clear()
        
        with lock:  # Zajištění exkluzivního přístupu k sdílené paměti
            if np.isnan(np_array[0, 0]):
                print("Consumer: No more data to process, exiting.")
                break
            np_array = np.ndarray(shape, dtype=np.float64, buffer=existing_shm.buf)
            print(f"Consumer: Data received and processed. {np_array[0][0:5]}")
    
    existing_shm.close()

if __name__ == '__main__':
    shape = (4800, 16)
    num_cycles = 10

    shm = shared_memory.SharedMemory(create=True, size=np.prod(shape) * np.float64().itemsize)
    event = Event()
    lock = Lock()
    
    producer_process = Process(target=producer, args=(shm.name, shape, event, lock, num_cycles))
    consumer_process = Process(target=consumer, args=(shm.name, shape, event, lock))
    
    producer_process.start()
    consumer_process.start()
    
    producer_process.join()
    consumer_process.join()
    
    shm.close()
    shm.unlink()
