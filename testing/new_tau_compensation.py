import os
import sys
import sounddevice as sd
from functools import partial
import numpy as np
import multiprocessing
from multiprocessing import shared_memory

current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
from ClassArchitecture import *

# ------------------------------------------------------------
# ------------------- MODIFYING VARIABLES --------------------
# ------------------------------------------------------------
# App regime
APP_REGIME = AppRegime.DSB_LOOP

# Audio variables
DURATION = 200
OUTPUT_DEVICE_ID = 0

# Phi resolution
PHI_STEPS = 30 # This number must be dividble by 8 for DSB_CARD regimes
THETA_STEPS = 10

# Computation process refresh time
COMPUTATION_THREAD_REFRESH_TIME = 0 # in seconds

# Analysis variables
CALCULATE_AVERAGE_TIME = True
PRINT_STATUS = True
SHOW_TIME = False
SHOW_ANGLES = True

# VISUALIZATION
SHOW_VISUALIZATION = False
GUI_REFRESH_TIME = 0.1 # in seconds

# First-order DMA regime
DMA_COMP_REGIME = DMACompRegime.BUTTER
RMS_FILTER = RMSfilter.BOTH

# ------------------------------------------------------------
# ----------- CMA PROTOTYPE VARIABLES DEFINITIONS ------------
# ------------------------------------------------------------
MICROPHONE_PAIRS = [[8, 10], [0, 2], [9, 11], [1, 3], [10, 8], [2, 0], [11, 9], [3, 1]] # IDs of microphones facing in front of each other
ACTIVE_CHANNELS_IDS = np.array([0, 1, 2, 3, 8, 9, 10, 11]) # Ids of active channels in this current FPGA configuration
BOUNDARY_CUTOFF_FREQUENCY = 1000

SF = 48000 # sampling frequency
BUFFER_SIZE = 4800
CHANNELS_IN = 16 # number of input channels
# ------------------------------------------------------------
# ------------------- CLASSES DEFINITIONS --------------------
# ------------------------------------------------------------
# Microphone array properties definition
microphoneArray = MicrophoneArray(radius=0.05,
                              m=8,
                              c=343,
                              sf=SF,
                              buffer_size=BUFFER_SIZE,
                              channels_in=CHANNELS_IN,
                              active_channels_ids=ACTIVE_CHANNELS_IDS
                            )

# Delay and sum beamforming properties definition
dsb = DSB(microphoneArray=microphoneArray, 
             theta_steps=THETA_STEPS,
             phi_steps=PHI_STEPS,
            )

sourceFinderDsb = SourceFinderDSB(dsb_instance=dsb, 
                                  duration=DURATION,
                                  show_angles=SHOW_ANGLES,
                                  show_time=SHOW_TIME,
                                  calculate_average_time=CALCULATE_AVERAGE_TIME,
                                  show_gui=SHOW_VISUALIZATION,
                                  refreshing_time=COMPUTATION_THREAD_REFRESH_TIME)

sourceFinderDSBCardioid = SourceFinderDSBCardioid(dsb_instance=dsb,
                                                  mic_pairs=MICROPHONE_PAIRS,
                                                  boundary_cutoff_freq=BOUNDARY_CUTOFF_FREQUENCY,
                                                  duration=DURATION,
                                                  show_angles=SHOW_ANGLES,
                                                  show_time=SHOW_TIME,
                                                  calculate_average_time=CALCULATE_AVERAGE_TIME,
                                                  show_gui=SHOW_VISUALIZATION,
                                                  refreshing_time=COMPUTATION_THREAD_REFRESH_TIME,
                                                  rms_filter=RMS_FILTER,
                                                  regime=DMA_COMP_REGIME)

approx_delta = sourceFinderDSBCardioid.__approximation_radius(1, 8)
stary = sourceFinderDSBCardioid.tau_compensation_old(0.1, approx_delta)
novy = sourceFinderDSBCardioid.__tau_compensation(0.1, approx_delta)

print(f"novy : {novy}")
print(f"stary: {stary}")
print(f"rovanji se? {novy == stary}")