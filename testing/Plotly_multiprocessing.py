import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import numpy as np
import multiprocessing
import time

refreshing_time = 0.1

def generate_data(queue):
    n = 1
    while True:
        theta = np.linspace(0, 2*np.pi, 100)
        r = np.abs(np.sin(theta * 2 * n))  # Simulace dat
        data = (theta, r)
        queue.put(data)
        if n > 3:
            n -= 1
        else:
            n += 1
        time.sleep(refreshing_time)  # Generuje data každých 5 sekund

# Vytvoření queue a procesu
data_queue = multiprocessing.Queue()
data_process = multiprocessing.Process(target=generate_data, args=(data_queue,))
data_process.start()

app = dash.Dash(__name__)

app.layout = html.Div([
    dcc.Graph(id='live-polar-plot'),
    dcc.Interval(
        id='interval-component',
        interval=refreshing_time*1000,  # v milisekundách (5 sekund)
        n_intervals=0
    )
])

@app.callback(
    Output('live-polar-plot', 'figure'),
    Input('interval-component', 'n_intervals')
)
def update_polar_plot(n):
    while True:
        if not data_queue.empty():
            print(f"mám data: {data_queue}")
            theta, r = data_queue.get_nowait()  # Vyjímá data z queue
            trace = go.Scatterpolar(
                r=r,
                theta=np.degrees(theta),
                mode='lines'
            )
            layout = go.Layout(
                polar=dict(
                    radialaxis=dict(
                        visible=True,
                        range=[0, 1]
                    )
                ),
                showlegend=False
            )
            return {'data': [trace], 'layout': layout}
        else:
            time.sleep(0.1)  # Krátké čekání před dalším pokusem

if __name__ == '__main__':
    app.run_server(debug=True)
