import multiprocessing
import time

def producer(pipe_conn):
    for i in range(5):
        time.sleep(1)  # Představujeme zpoždění při vytváření dat
        pipe_conn.send(i)
        print(f"Produced {i} and sent it through the pipe.")
    pipe_conn.send("DONE")  # Posíláme zprávu o dokončení
    print("All data produced and sent.")

def consumer(pipe_conn):
    while True:
        print("waiting for data")
        data = pipe_conn.recv()
        if data == "DONE":
            print("All data received, stopping consumer.")
            break
        print(f"Consumed data: {data}")

if __name__ == '__main__':
    parent_conn, child_conn = multiprocessing.Pipe()

    producer_process = multiprocessing.Process(target=producer, args=(parent_conn,))
    consumer_process = multiprocessing.Process(target=consumer, args=(child_conn,))

    producer_process.start()
    consumer_process.start()

    producer_process.join()
    consumer_process.join()
