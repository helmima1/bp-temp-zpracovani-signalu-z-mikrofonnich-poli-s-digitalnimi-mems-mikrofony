class Prarodice:
    def __init__(self, promenna1):
        self.promenna1 = promenna1
        print(f"Prarodič inicializován s promenna1 = {self.promenna1}")

class Rodice:
    def __init__(self, prarodice_instance, promenna2):
        # Přístup k atributům instance prarodiče
        super().__init__(prarodice_instance.promenna1)
        self.promenna2 = promenna2
        print(f"Rodič inicializován s promenna1 = {self.promenna1}, promenna2 = {self.promenna2}")

class Potomek:
    def __init__(self, rodice_instance, promenna3):
        # Přístup k atributům instance rodiče
        super().__init__(rodice_instance.promenna1, rodice_instance.promenna2)
        self.promenna3 = promenna3
        print(f"Potomek inicializován s promenna1 = {self.promenna1}, promenna2 = {self.promenna2}, promenna3 = {self.promenna3}")

# Vytvoření instance Prarodiče
prarodice_instance = Prarodice("hodnota1")

# Vytvoření instance Rodiče pomocí instance Prarodiče
rodice_instance = Rodice(prarodice_instance, "hodnota2")

# Vytvoření instance Potomka pomocí instance Rodiče
potomek_instance = Potomek(rodice_instance, "hodnota3")
