import sounddevice as sd
import numpy as np
import multiprocessing
import time

def zpracovani_dat(event, data):
    """Funkce pro zpracování dat v separátním procesu."""
    print(f"Zpracování dat začíná: {data}")
    time.sleep(2)  # Představme dlouhé zpracování
    print("Zpracování dat dokončeno.")
    event.set()  # Signalizace dokončení zpracování

def callback(indata, frames, time, status):
    global zpracovani_event
    if status:
        print(status)
    if not zpracovani_event.is_set():
        print("Zpracování ještě nedoběhlo, čekám...")
    else:
        print("Zpracování doběhlo, spouštím další.")
        zpracovani_event.clear()  # Resetování události pro další zpracování
        data = np.random.rand(100)  # Představme, že toto jsou naše data k zpracování
        process = multiprocessing.Process(target=zpracovani_dat, args=(zpracovani_event, data))
        process.start()

if __name__ == '__main__':
    # Inicializace události pro signalizaci dokončení zpracování
    zpracovani_event = multiprocessing.Event()
    zpracovani_event.set()  # Nastavení události na začátku, aby první zpracování mohlo začít

    # Nastavení streamu
    stream = sd.InputStream(callback=callback)
    with stream:
        print("Stream spuštěn, čekání na data...")
        time.sleep(10)  # Spuštění streamu na omezenou dobu, upravte dle potřeby

    # Poznámka: V tomto jednoduchém příkladu nejsou procesy explicitně ukončeny
    # V reálné aplikaci byste měli zajistit, aby všechny procesy byly na konci správně ukončeny
