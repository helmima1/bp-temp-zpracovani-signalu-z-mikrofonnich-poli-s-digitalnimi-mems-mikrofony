import os
import sys

current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf
from ClassArchitecture import *

# Basic variables
OUTPUT_DEVICE_ID = 0
SF = 48000
BUFFER_SIZE = 4800
CHANNELS_IN = 16
DURATION = 100

# Analysis variables
CALCULATE_AVERAGE_TIME = True
PRINT_STATUS = True

microphoneArray = MicrophoneArray(radius=0.05,
                              m=8,
                              c=343,
                              sf=SF,
                              buffer_size=BUFFER_SIZE,
                              channels_in=CHANNELS_IN,
                            )

# Delay and sum beamforming properties definition
dsb = DSB(microphoneArray=microphoneArray, 
             theta_steps=1,
             phi_steps=32
            )

sourceFinderDsb = SourceFinderDSB(dsb_instance=dsb, duration=DURATION)

# print(dsb.cma_tmi(0, 90))