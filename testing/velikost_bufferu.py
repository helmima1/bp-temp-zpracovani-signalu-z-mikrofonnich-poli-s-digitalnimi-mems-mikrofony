import numpy as np
import sys

debil = np.empty((1, 8), dtype=int)
print(debil)

# Vytvoření pole
data = np.random.rand(4800, 16)
print(type(data[0][0]))

# Získání velikosti dat v bajtech
data_nbytes = data.nbytes 
total_size_bits = sys.getsizeof(data)

print(f"Data nbytes: {data_nbytes} bytes")
print(f"Total size of ndarray including overhead: {total_size_bits} bits")