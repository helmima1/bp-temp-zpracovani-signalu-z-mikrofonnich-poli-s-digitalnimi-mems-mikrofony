import threading
import sounddevice as sd
import numpy as np
import time

print(sd.query_devices())
duration = 3  # determines how long a program should run in seconds
sd.default.device = 4, 3  # Selection of input and output devices (usually different for every computer)

# ------------------------------------------------------------
# ------------------- VARIABLES DEFINITION -------------------
# ------------------------------------------------------------
radius = 0.05  # radius of a circular microphone array in meters
M = 8  # number of microphones in circular microphone array
theta = 90
phi = 0
c = 343  # speed of sound in meters/seconds
sampling_frequency = 48000  # sampling frequency of microphones in Hz

buffer_size = 4800
channels_in = 16

# Event pro sledování, zda je zpracování dat dokončeno
zpracovani_hotovo = threading.Event()
# Na začátku nastavíme event na "hotovo", aby první vlákno mohlo být spuštěno
zpracovani_hotovo.set()

def zpracovani_dat():
    """Simulace dlouhotrvající úlohy zpracování dat."""
    print("Zpracování dat začíná.")
    # Simulace zpracování dat trvající 5 sekund
    time.sleep(2)
    print("Zpracování dat dokončeno.")
    # Po dokončení zpracování signalizujeme, že je možné spustit další vlákno
    zpracovani_hotovo.set()

def callback(indata, outdata, frames, time, status):
    """Tento callback se volá pro každý buffer."""
    global zpracovani_hotovo
    if status:
        print(status)
    # Kontrola, zda je předchozí zpracování hotovo
    if zpracovani_hotovo.is_set():
        # Pokud ano, resetujeme event a spouštíme nové zpracování
        zpracovani_hotovo.clear()
        threading.Thread(target=zpracovani_dat).start()
    else:
        print("Předchozí zpracování ještě není dokončeno.")
    outdata[:, 0] = indata[:, 0]
    outdata[:, 1] = indata[:, 1]

# Nastavení streamu

with sd.Stream(samplerate=sampling_frequency, channels=[16, 2], callback=callback, blocksize=buffer_size):
    sd.sleep(int(duration * 1000))  # Upravte podle potřeby
