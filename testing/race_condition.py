from multiprocessing import Process, Lock, shared_memory
import numpy as np

def writer(shm_name, lock):
    shm = shared_memory.SharedMemory(name=shm_name)
    array = np.ndarray((10,), dtype=np.int32, buffer=shm.buf)
    with lock:
        for i in range(10):
            array[i] = i + 1  # Bezpečné zápisování do sdílené paměti
    shm.close()

def reader(shm_name, lock):
    shm = shared_memory.SharedMemory(name=shm_name)
    array = np.ndarray((10,), dtype=np.int32, buffer=shm.buf)
    with lock:
        data = array.copy()  # Bezpečné čtení ze sdílené paměti
    shm.close()
    print("Data read by reader:", data)

if __name__ == '__main__':
    shm = shared_memory.SharedMemory(create=True, size=40)
    lock = Lock()

    p1 = Process(target=writer, args=(shm.name, lock))
    p2 = Process(target=reader, args=(shm.name, lock))

    p1.start()
    p2.start()

    p1.join()
    p2.join()

    shm.close()
    shm.unlink()
