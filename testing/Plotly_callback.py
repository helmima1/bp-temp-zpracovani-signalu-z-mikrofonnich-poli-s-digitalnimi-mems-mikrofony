import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import numpy as np
import multiprocessing
import sounddevice as sd
import time

# Sample rate and duration for audio processing
samplerate = 44100  # Hz
duration = 0.1  # seconds, this will match your refreshing time for Dash

def audio_callback(indata, frames, time, status):
    """This callback function processes audio input and puts results in a queue."""
    if status:
        print(status)
    # Example processing: Compute magnitude of FFT
    magnitude = np.abs(np.fft.rfft(indata[:, 0]))
    frequencies = np.fft.rfftfreq(len(indata[:, 0]), 1 / samplerate)
    queue.put((frequencies, magnitude))

# Queue for sharing data between audio callback and Dash app
queue = multiprocessing.Queue()

# Sounddevice stream setup
stream = sd.InputStream(callback=audio_callback, channels=1, samplerate=samplerate, blocksize=int(samplerate * duration))

app = dash.Dash(__name__)

app.layout = html.Div([
    dcc.Graph(id='live-polar-plot'),
    dcc.Interval(
        id='interval-component',
        interval=int(duration * 1000),  # Interval in milliseconds
        n_intervals=0
    )
])

@app.callback(
    Output('live-polar-plot', 'figure'),
    Input('interval-component', 'n_intervals')
)
def update_polar_plot(n):
    if not queue.empty():
        frequencies, magnitude = queue.get_nowait()
        trace = go.Scatterpolar(
            r=magnitude,
            theta=np.degrees(frequencies),  # Adjust if necessary
            mode='lines'
        )
        layout = go.Layout(
            polar=dict(
                radialaxis=dict(
                    visible=True,
                    range=[0, np.max(magnitude)]
                )
            ),
            showlegend=False
        )
        return {'data': [trace], 'layout': layout}
    # Return an empty figure if no data available yet
    return go.Figure()

if __name__ == '__main__':
    with stream:
        app.run_server(debug=True)
