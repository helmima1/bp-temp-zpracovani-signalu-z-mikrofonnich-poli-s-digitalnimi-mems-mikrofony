import numpy as np
from vispy import app, scene

# Vytvoření okna
canvas = scene.SceneCanvas(keys='interactive')
canvas.size = 800, 600
canvas.show()

# Přidání ViewBoxu pro kontrolu zoomu a panování
view = canvas.central_widget.add_view()

# Definování polárních dat
theta = np.linspace(0, 2 * np.pi, 100)
r = 1 + 0.3 * np.sin(3 * theta)

# Převod na kartézské souřadnice
x = r * np.cos(theta)
y = r * np.sin(theta)

# Vytvoření line plotu
line = scene.LinePlot(np.column_stack([x, y]), color='white', width=2)
line.transform = scene.transforms.STTransform(scale=(1, 1, 1), translate=(400, 300, 0))
view.add(line)

# Nastavení kamerového pohledu
view.camera = scene.PanZoomCamera(aspect=1)
view.camera.set_range()

# Spuštění aplikace
if __name__ == '__main__':
    app.run()
