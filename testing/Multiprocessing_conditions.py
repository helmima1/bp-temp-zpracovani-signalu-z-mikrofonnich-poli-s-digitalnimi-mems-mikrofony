import multiprocessing
import time

def producer(data_for_consumer_1, data_for_consumer_2, cond):
    """Producent, který vytváří data a informuje consumer_1 pomocí Condition."""
    for i in range(1, 6):  # Předpokládejme 5 cyklů pro ukázku
        # Vytvoření dat pro oba spotřebitele
        data_for_consumer_1.value = i * 2
        data_for_consumer_2.value = i * 3
        print(f"Producer updated data to Consumer 1: {data_for_consumer_1.value}, Consumer 2: {data_for_consumer_2.value}")
        
        # Notifikace consumer_1
        with cond:
            cond.notify_all()
        
        time.sleep(1)  # Časové zpoždění mezi aktualizacemi

def consumer_1(shared_value, cond):
    """První spotřebitel, který čeká na data od producenta pomocí Condition."""
    last_seen = 0
    while True:
        with cond:
            cond.wait()  # Čeká na notifikaci od producenta
            if shared_value.value != last_seen:
                print(f"Consumer 1 processed new data: {shared_value.value}")
                last_seen = shared_value.value
                if last_seen >= 10:
                    break

def consumer_2(shared_value):
    """Druhý spotřebitel pravidelně kontroluje aktualizaci svých dat."""
    last_seen = 0
    while True:
        if shared_value.value != last_seen:
            print(f"Consumer 2 processed new data: {shared_value.value}")
            last_seen = shared_value.value
            if last_seen >= 15:
                break
        time.sleep(1)  # Kontroluje každou sekundu

if __name__ == "__main__":
    # Vytvoření sdílených proměnných a Condition objektu
    data_for_consumer_1 = multiprocessing.Value('i', 0)
    data_for_consumer_2 = multiprocessing.Value('i', 0)
    lock = multiprocessing.Lock()
    condition = multiprocessing.Condition(lock)

    # Vytvoření a spuštění procesů
    producer_process = multiprocessing.Process(target=producer, args=(data_for_consumer_1, data_for_consumer_2, condition))
    consumer1_process = multiprocessing.Process(target=consumer_1, args=(data_for_consumer_1, condition))
    consumer2_process = multiprocessing.Process(target=consumer_2, args=(data_for_consumer_2,))

    producer_process.start()
    consumer1_process.start()
    consumer2_process.start()

    producer_process.join()
    consumer1_process.join()
    consumer2_process.join()
