import os
import sys
import numpy as np

current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# -------------------------------------------------------------------------------------------
RADIUS = 0.05
M = 8
SF = 48000
NUMBER_OF_TRIED_THETA = 1
NUMBER_OF_TRIED_PHI = 8
C = 343


my_tmis = cf.cma_tmi(RADIUS, M, 0, 90, C)
# print(my_tmis)

ALL_LATENCIES_Q = cf.calculate_dsb_delays(RADIUS, M, SF, NUMBER_OF_TRIED_THETA, NUMBER_OF_TRIED_PHI, C, quantize=True)
print(ALL_LATENCIES_Q)
ALL_LATENCIES_F = cf.calculate_dsb_delays(RADIUS, M, SF, NUMBER_OF_TRIED_THETA, NUMBER_OF_TRIED_PHI, C, floor=True)
print(ALL_LATENCIES_F)

# buffer = np.random.rand(10)
# print(buffer)
# delayed_buffer = cf.delay_signal_interpolation(buffer, 10.42, 48000, 1)
# print(delayed_buffer)