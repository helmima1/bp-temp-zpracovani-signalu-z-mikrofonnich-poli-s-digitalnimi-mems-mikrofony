import multiprocessing
import sounddevice as sd
from functools import partial
import numpy as np
import time

def producer(data_for_consumer_1, data_for_consumer_2, cond):
    for i in range(1, 6):  # Předpokládejme 5 cyklů pro ukázku
        data_for_consumer_1.value = i * 2
        data_for_consumer_2.value = i * 3
        print(f"Producer updated data to Consumer 1: {data_for_consumer_1.value}, Consumer 2: {data_for_consumer_2.value}")
        with cond:
            cond.notify_all()
        time.sleep(1)

def consumer_1(shared_value, cond):
    last_seen = 0
    while True:
        with cond:
            cond.wait()
            if shared_value.value != last_seen:
                print(f"Consumer 1 processed new data: {shared_value.value}")
                last_seen = shared_value.value
                if last_seen >= 10:
                    break

def audio_callback(indata, outdata, frames, time_info, status, shared_value):
    if status:
        print(status)
    new_value = shared_value.value
    # print(f"Audio callback processing new data: {new_value}")
    # Simulovat nějaké zpracování zvuku na základě sdílené hodnoty
    outdata[:, 0] = indata[:, 0] * new_value  # Příklad, jak můžete použít hodnotu pro modifikaci vstupu
    outdata[:, 1] = indata[:, 1] * new_value

if __name__ == "__main__":
    data_for_consumer_1 = multiprocessing.Value('i', 0)
    data_for_consumer_2 = multiprocessing.Value('i', 0)
    lock = multiprocessing.Lock()
    condition = multiprocessing.Condition(lock)

    producer_process = multiprocessing.Process(target=producer, args=(data_for_consumer_1, data_for_consumer_2, condition))
    consumer1_process = multiprocessing.Process(target=consumer_1, args=(data_for_consumer_1, condition))

    producer_process.start()
    consumer1_process.start()
    
    print(sd.query_devices())
    sd.default.device = 4, 0
    wrapped_callback = partial(audio_callback, queue)

    # Nastavit stream pro audio callback s přístupem ke sdílené hodnotě
    # with sd.Stream(samplerate=48000, channels=[16, 2], callback=wrapped_callback, blocksize=BUFFER_SIZE):
    with sd.Stream(callback=lambda indata, outdata, frames, time_info, status: audio_callback(indata, outdata, frames, time_info, status, data_for_consumer_2)):
        input("Press Enter to stop the audio stream...")

    producer_process.join()
    consumer1_process.join()
