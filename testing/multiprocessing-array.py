from multiprocessing import Process, Array

def update_shared_array(shared_array):
    # Tento proces může aktualizovat pole
    with shared_array.get_lock():  # Zajištění přístupu k datům
        shared_array[0] = 10  # Aktualizace prvního integeru
        shared_array[1] = 20  # Aktualizace druhého integeru

def use_shared_array(shared_array):
    with shared_array.get_lock():
        print(tuple(shared_array))  # Čtení a konverze na tuple

if __name__ == '__main__':
    # Vytvoření sdíleného pole dvou integerů
    shared_array = Array('i', 2)  # 'i' znamená integer

    p1 = Process(target=update_shared_array, args=(shared_array,))
    p2 = Process(target=use_shared_array, args=(shared_array,))

    p1.start()
    p1.join()

    p2.start()
    p2.join()