"""
V tomto skriptu jsem přišel na to, že lowpass filtr aplikovaný na buffer zpracovávaný kardioidou nemá 
žádný vliv na ratio mezi přenosovou funkcí fyzických mikrofonů a imaginární. To dává docela smysl,
jelikož tu přenosovou funkci zpracovávám z jednoho bufferu, který je odfiltrovaný tím prvním lowpass 
filtrem na 1 kHz. 

Takže tento skript byl vlastně úplně zbytečný..
""" 
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, freqz

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
max_frequency = 1000
c = 343
phi = 0
mic_distance = 0.1 # For angle 0
mic_distance_1 = 0.09238795325112868 # For angle 22.5
mic_distance_2 = mic_distance_3 = cf.approximation_radius(mic_distance_1, 1, 4) # 0.09326442173106045 # For angle 15 or 30

fs = 48000  # Vzorkovací frekvence v Hz
cutoff_primary = 1000
order_primary = 6
cutoff_secondary = 2250  # Mezní frekvence low-pass filtru v Hz
order_secondary = 1  # Řád Butterworthova filtru

# --------------- TRANSFER FUNCTION CALCULATION ---------------

# initial filter
b_prim, a_prim = butter(order_primary, cutoff_primary / (0.5 * fs), btype='low', analog=False)
freqs, response_prim = freqz(b_prim, a_prim, worN=np.linspace(20, max_frequency, max_frequency - 19) * 2*np.pi / fs)
magnitude_prim = np.abs(response_prim)

# Transfer function for 0 degrease
tf = cf.cardioid_tf(0, mic_distance, max_frequency)
tf_amplitudes = np.array([np.abs(x) for x in tf])
tf_quantized = cf.cardioid_tf_quantized(0, mic_distance, max_frequency)
tf_quantized_amplitudes = np.array([np.abs(x) for x in tf_quantized])
tf_filtered = cf.cardioid_tf_quantized_filtered(0, mic_distance, magnitude_prim, max_frequency)
tf_filtered_amplitudes = np.array([np.abs(x) for x in tf_filtered])

# Transfer function for angle 22.5 degrease
tf_1 = cf.cardioid_tf(0, mic_distance_1, max_frequency) 
tf_1_amplitudes = np.array([np.abs(x) for x in tf_1])
tf_1_quantized = cf.cardioid_tf_quantized(0, mic_distance_1, max_frequency)
tf_1_quantized_amplitudes = np.array([np.abs(x) for x in tf_1_quantized])
tf_1_filtered = cf.cardioid_tf_quantized_filtered(0, mic_distance_1, magnitude_prim, max_frequency)
tf_1_filtered_amplitudes = np.array([np.abs(x) for x in tf_1_filtered])

# Transfer function for angles 15 or 30 degrease
tf_2 = cf.cardioid_tf(0, mic_distance_2, max_frequency)
tf_2_amplitudes = np.array([np.abs(x) for x in tf_2])
tf_2_quantized = cf.cardioid_tf_quantized(0, mic_distance_2, max_frequency)
tf_2_quantized_amplitudes = np.array([np.abs(x) for x in tf_2_quantized])
tf_2_filtered = cf.cardioid_tf_quantized_filtered(0, mic_distance_2, magnitude_prim, max_frequency)
tf_2_filtered_amplitudes = np.array([np.abs(x) for x in tf_2_filtered])

# --------------- AVERAGE CALCULATION ---------------
# This part calculates difference in loudness between transfer function for 0 degrease and other degreases 
ratios_1 = np.abs(tf_amplitudes/tf_1_amplitudes)
ratios_2 = np.abs(tf_amplitudes/tf_2_amplitudes)
average_1 = np.sum(np.abs(tf_amplitudes/tf_1_amplitudes))/tf_amplitudes.shape[0]
average_2 = np.sum(np.abs(tf_amplitudes/tf_2_amplitudes))/tf_amplitudes.shape[0]
print(f"Average norm 1: {average_1}, Average norm 2: {average_2}")

ratios_quantized_1 = np.abs(tf_quantized_amplitudes/tf_1_quantized_amplitudes)
ratios_quantized_2 = np.abs(tf_quantized_amplitudes/tf_2_quantized_amplitudes)
average_quantized_1 = np.sum(np.abs(tf_quantized_amplitudes/tf_1_quantized_amplitudes))/tf_quantized_amplitudes.shape[0]
average_quantized_2 = np.sum(np.abs(tf_quantized_amplitudes/tf_2_quantized_amplitudes))/tf_quantized_amplitudes.shape[0]
print(f"Average quan 1: {average_quantized_1}, Average quan 2: {average_quantized_2}")

ratios_filtered_1 = np.abs(tf_filtered_amplitudes/tf_1_filtered_amplitudes)
ratios_filtered_2 = np.abs(tf_filtered_amplitudes/tf_2_filtered_amplitudes)
average_filtered_1 = np.sum(np.abs(tf_filtered_amplitudes/tf_1_filtered_amplitudes))/tf_filtered_amplitudes.shape[0]
average_filtered_2 = np.sum(np.abs(tf_filtered_amplitudes/tf_2_filtered_amplitudes))/tf_filtered_amplitudes.shape[0]
print(f"Average quan 1: {average_filtered_1}, Average quan 2: {average_filtered_2}")

# --------------- FILTER CALCULATION ---------------
b, a = butter(order_secondary, cutoff_secondary / (0.5 * fs), btype='low', analog=False)
freqs, response = freqz(b, a, worN=np.linspace(20, max_frequency, max_frequency - 19) * 2*np.pi / fs)
magnitude_secondary = np.abs(response) * 1.08

# --------------- DISPLAY PLOTS ---------------
frequencies = np.linspace(20, max_frequency, len(tf_quantized_amplitudes))

plt.figure(figsize=(12, 8))
## Displaying transfer functions NORMAL:
# plt.semilogx(frequencies, tf_amplitudes, color='cornflowerblue')
# plt.semilogx(frequencies, tf_1_amplitudes, color='lightcoral')
# plt.semilogx(frequencies, tf_2_amplitudes, color='lime')
## Displaying transfer functions QUANTIZED:
# plt.semilogx(frequencies, tf_quantized_amplitudes, color='blue')
# plt.semilogx(frequencies, tf_1_quantized_amplitudes, color='red')
# plt.semilogx(frequencies, tf_2_quantized_amplitudes, color= 'green')
## Displaying transfer functions QUANTIZED and FILTERED:
# plt.semilogx(frequencies, tf_filtered_amplitudes, color='darkblue')
# plt.semilogx(frequencies, tf_1_filtered_amplitudes, color='darkred')
# plt.semilogx(frequencies, tf_2_filtered_amplitudes, color='darkgreen')
## Displaying ratios NORMAL and QUANTIZED
# plt.semilogx(frequencies, ratios_1, color='lightcoral')
# plt.semilogx(frequencies, ratios_2, color='lime')
plt.semilogx(frequencies, ratios_quantized_1, color='red')
plt.semilogx(frequencies, ratios_quantized_2, color='green')
## Displaying radios filtered
plt.semilogx(frequencies, ratios_filtered_1, color='darkred')
plt.semilogx(frequencies, ratios_filtered_2, color='darkgreen')
## Butterworth filter
plt.semilogx(frequencies, magnitude_secondary, color='orange')
# plt.semilogx(frequencies, magnitude_prim, color='magenta')
## Displaying graphs
plt.title('Transfer function of linear microphone array')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude')
plt.grid(True, which="both", ls="--")
plt.xlim(20, max_frequency)

plt.show()
