"""
Tyto grafy ukazují, jak moc se liší hlasitost z "imaginárních" mikrofonů vypočtených 
pomocí průměrování okolních "fyzických". Na přenosových funkcích je vidět, že hlasitost
kardioidy vypočítané z "imaginárních" mikrofonnů je na klíčových frekvencích pod 1 kHz 
nižší, než na je kardioida z "fyzických mikrofonů". Z toho plyne, že pro hledání nejhlasitějšího
zdroje pomocí kardioid je potřeba "imaginární" mikrofony adekvátně zesílit, abychom tento
nežádoucí efekt eliminovali.

Přenosové funkce zároveň ovlivňuje i kvantizace na celé počty vzorků. To upravuje potřebné
""" 
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, freqz

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
max_frequency = 1000
c = 343
phi = 0
mic_distance = 0.1 # For angle 0
mic_distance_1 = cf.approximation_radius(mic_distance, 1, 2)
# -----------------------------------------------------------------------------------
mic_distance_2 = cf.approximation_radius(mic_distance, 1, 4) # 0.09326442173106045 # For angle 15 or 30

fs = 48000  # Vzorkovací frekvence v Hz
cutoff = 2250
order = 1

# --------------- TRANSFER FUNCTION CALCULATION ---------------

# Transfer function for 0 degrease
tf = cf.cardioid_tf(phi, mic_distance, max_frequency)
tf_amplitudes = np.array([np.abs(x) for x in tf])
tf_quantized = cf.cardioid_tf_quantized(phi, mic_distance, max_frequency)
tf_quantized_amplitudes = np.array([np.abs(x) for x in tf_quantized])

# Transfer function for angle 22.5 degrease
tf_1 = cf.cardioid_tf(phi, mic_distance_1, max_frequency) 
tf_1_amplitudes = np.array([np.abs(x) for x in tf_1])
tf_1_quantized = cf.cardioid_tf_quantized(phi, mic_distance_1, max_frequency)
tf_1_quantized_amplitudes = np.array([np.abs(x) for x in tf_1_quantized])

# Transfer function for angles 15 or 30 degrease
tf_2 = cf.cardioid_tf(phi, mic_distance_2, max_frequency)
tf_2_amplitudes = np.array([np.abs(x) for x in tf_2])
tf_2_quantized = cf.cardioid_tf_quantized(phi, mic_distance_2, max_frequency)
tf_2_quantized_amplitudes = np.array([np.abs(x) for x in tf_2_quantized])

# --------------- AVERAGE CALCULATION ---------------
# This part calculates difference in loudness between transfer function for 0 degrease and other degreases 
ratios_1 = np.abs(tf_amplitudes/tf_1_amplitudes)
ratios_2 = np.abs(tf_amplitudes/tf_2_amplitudes)
average_1 = np.sum(np.abs(tf_amplitudes/tf_1_amplitudes))/tf_amplitudes.shape[0]
average_2 = np.sum(np.abs(tf_amplitudes/tf_2_amplitudes))/tf_amplitudes.shape[0]
print(f"Average norm 1: {average_1}, Average norm 2: {average_2}")

ratios_quantized_1 = np.abs(tf_quantized_amplitudes/tf_1_quantized_amplitudes)
ratios_quantized_2 = np.abs(tf_quantized_amplitudes/tf_2_quantized_amplitudes)
average_quantized_1 = np.sum(np.abs(tf_quantized_amplitudes/tf_1_quantized_amplitudes))/tf_quantized_amplitudes.shape[0]
average_quantized_2 = np.sum(np.abs(tf_quantized_amplitudes/tf_2_quantized_amplitudes))/tf_quantized_amplitudes.shape[0]
print(f"Average quan 1: {average_quantized_1}, Average quan 2: {average_quantized_2}")

# --------------- FILTER CALCULATION ---------------
b, a = butter(order, cutoff / (0.5 * fs), btype='low', analog=False)
freqs, response = freqz(b, a, worN=np.linspace(20, max_frequency, max_frequency - 19) * 2*np.pi / fs)
magnitude = np.abs(response) * ratios_1[20]

# --------------- DISPLAY PLOTS ---------------
frequencies = np.linspace(20, max_frequency, len(tf_quantized_amplitudes))

plt.figure(figsize=(12, 8)) 
## Displaying transfer functions NORMAL:
plt.semilogx(frequencies, tf_amplitudes, color='cornflowerblue')
plt.semilogx(frequencies, tf_1_amplitudes, color='lightcoral')
plt.semilogx(frequencies, tf_2_amplitudes, color='lime')
## Displaying transfer functions QUANTIZED:
# plt.semilogx(frequencies, tf_quantized_amplitudes, color='blue')
# plt.semilogx(frequencies, tf_1_quantized_amplitudes, color='darkred')
# plt.semilogx(frequencies, tf_2_quantized_amplitudes, color='darkgreen')
## Displaying ratios NORMAL and QUANTIZED
# plt.semilogx(frequencies, ratios_1, color='lightcoral')
# plt.semilogx(frequencies, ratios_2, color='lime')
# plt.semilogx(frequencies, ratios_quantized_1, color='darkred')
# plt.semilogx(frequencies, ratios_quantized_2, color='darkgreen')
## Butterworth filter
# plt.semilogx(frequencies, magnitude, color='orange')
## Plot display commands
plt.title('Transfer function')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude')
plt.grid(True, which="both", ls="--")
plt.xlim(20, max_frequency)

plt.show()

