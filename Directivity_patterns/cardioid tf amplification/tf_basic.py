"""
Teď mám ještě jeden problém, a to sice v tom, že do těchto výpočtů potřebuji zahrnout zaokrouhlení.
Mě zajímá, jakou budu mít hlasitost, když ty mikrofony zpozdím o nějaký zaokrouhlený čas, který nebude 
odpovídat délce
"""
import sys
import os
import numpy as np
import matplotlib.pyplot as plt

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
frequency = 1000
c = 343
phi = 0
mic_distance_1 = 0.1
mic_distance_2 = 0.09238795325112868
# mic_distance_3 = 0.09326442173106045
mic_distance_3 = cf.approximation_radius(mic_distance_1, 1, 4)

# --------------- TRANSFER FUNCTION CALCULATION ---------------

tf_1 = cf.cardioid_tf(0, mic_distance_1) # Transfer function
tf_1_amplitudes = np.abs(tf_1) 

tf_2 = cf.cardioid_tf(0, mic_distance_2) # Transfer function
tf_2_amplitudes = np.abs(tf_2)

tf_3 = cf.cardioid_tf(0, mic_distance_3) # Transfer function
tf_3_amplitudes = np.abs(tf_3)

index_min_prvku = np.argmin(tf_1_amplitudes)
# print(index_min_prvku)

# --------------- AVERAGE CALCULATION ---------------
prumer_1 = np.sum(np.abs(tf_1_amplitudes/tf_2_amplitudes))/tf_1_amplitudes.shape[0]
prumer_2 = np.sum(np.abs(tf_1_amplitudes/tf_3_amplitudes))/tf_1_amplitudes.shape[0]
print(f"Prumer 1: {prumer_1}, Prumer 2: {prumer_2}")

# Generování frekvenční osy
frekvence = np.linspace(20, 20000, len(tf_1_amplitudes))
# Vytvoření grafu
plt.figure(figsize=(12, 8)  )
plt.semilogx(frekvence, tf_1_amplitudes)  # Použití logaritmické osy pro frekvence
plt.semilogx(frekvence, tf_2_amplitudes, color='red')
plt.semilogx(frekvence, tf_3_amplitudes, color='lime')
plt.title('Přenosová funkce mikrofonního pole')
plt.xlabel('Frekvence (Hz)')
plt.ylabel('Amplituda (dB)')
plt.grid(True, which="both", ls="--")
plt.xlim(20, 20000)  # Nastavení limitů x-ové osy pro zobrazení od 20 Hz do 20 kHz

# Zobrazení grafu
plt.show()

