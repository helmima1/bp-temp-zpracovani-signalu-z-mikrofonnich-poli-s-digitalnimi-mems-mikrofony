import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, freqz

# Parametry filtru
cutoff = 1000  # Mezní frekvence low-pass filtru v Hz
fs = 48000  # Vzorkovací frekvence v Hz
order = 4  # Řád Butterworthova filtru

# Vytvoření Butterworthova low-pass filtru
b, a = butter(order, cutoff / (0.5 * fs), btype='low', analog=False)

# Výpočet frekvenční charakteristiky filtru
freqs, response = freqz(b, a, worN=np.linspace(20, 1000, 500) * 2*np.pi / fs)

# Převod frekvencí na Hz
freqs = np.linspace(0, fs / 2, len(response), endpoint=True)
# print(response.shape)

# Výpočet magnitudy frekvenční odezvy v decibelech
magnitude = np.abs(response) # 20 * np.log10(np.abs(response))
print(magnitude.shape)

# Vykreslení grafu
plt.figure()
plt.semilogx(freqs, magnitude)
plt.title('Frekvenční charakteristika Butterworthova low-pass filtru')
plt.xlabel('Frekvence [Hz]')
plt.ylabel('Amplituda [dB]')
plt.grid(which='both', axis='both')
plt.axvline(cutoff, color='red') # Značka pro mezní frekvenci
plt.ylim(0, 1.1)
plt.show()
