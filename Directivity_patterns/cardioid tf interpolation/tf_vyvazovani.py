"""
Cílem tohoto scriptu je vytvořit 2 přenosové funkce se stejným průběhem. 
- 1. je s diametrem 0.1 m a zpožděním kvantovaným na 14 vzorků
- 2. je s diametrem 0.092... a zpožděním optimalizovaným na průběh 1. funkce

TOTO JE MŮJ TESTOVACÍ SCRIPT
""" 
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, freqz

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
max_frequency = 1100
c = 343
phi = 0
sf = 48000
mic_distance = 0.1 # For angle 0
mic_distance_1 = cf.approximation_radius(mic_distance, 2) # 0.09238795325112868 # For angle 22.5
mic_distance_2 = cf.approximation_radius(mic_distance, 3) # 0.09326442173106045 # For angle 15 or 30
tau_add_1 = 0.000043

fs = 48000  # Vzorkovací frekvence v Hz
cutoff = 2550
order = 1

# -------------- VÝPOČET TAU ADDITION --------------
q_step = 1/sf
tau_norm = mic_distance/c
tau_q = round((tau_norm / q_step)) * q_step

tau_2 = mic_distance_1/c # (mic_distance_1 * tau_q) / mic_distance
tau_2_normalne = mic_distance_1/c
print(f"Pred: {tau_2}")
print(f" ted: {tau_2_normalne}")
rozdil = tau_q - tau_2
horsi_tau = rozdil + tau_q

novy_tau = cf.tau_compensation(mic_distance, mic_distance_1)

# print(f"Pocet vzorků: {novy_tau/q_step}")
# print(f"Novy tau zpozdeni: {novy_tau%q_step}")

# --------------- TRANSFER FUNCTION CALCULATION ---------------

tf_quantized = cf.cardioid_tf_quantized(phi, mic_distance, max_frequency)
tf_quantized_amplitudes = np.abs(tf_quantized)

tf_added = cf.cardioid_tf_tau(phi, mic_distance_1, novy_tau, max_frequency)
tf_added_amplitudes = np.abs(tf_added)

tf_added_2 = cf.cardioid_tf_tau(phi, mic_distance_1, horsi_tau, max_frequency)
tf_added_amplitudes_2 = np.abs(tf_added_2)

# --------------- CONTROLLING BOUNDARIES ---------------
print(cf.are_arrays_within_bounds(tf_quantized_amplitudes, tf_added_amplitudes, 0.00003))

# --------------- DISPLAY PLOTS ---------------
frequencies = np.linspace(20, max_frequency, max_frequency - 19)

plt.figure(figsize=(12, 8))
## Transfer functions
plt.semilogx(frequencies, tf_quantized_amplitudes, color='cornflowerblue')
plt.semilogx(frequencies, tf_added_amplitudes, color='lime')
plt.semilogx(frequencies, tf_added_amplitudes_2, color='red')
## Plot display commands
plt.title('Transfer function of linear microphone array')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude')
plt.grid(True, which="both", ls="--")
plt.xlim(20, max_frequency)

plt.show()

