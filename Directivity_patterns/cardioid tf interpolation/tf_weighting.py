"""
TODO
""" 
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, freqz

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
max_frequency = 1100
c = 343
phi = 0
sf = 48000
delta = 0.1 # For angle 0
delta_approx_2 = cf.approximation_radius(delta, 1, 2) # 0.09238795325112868 # For angle n*22.5
delta_approx_3 = cf.approximation_radius(delta, 1, 3) # 0.09326442173106045 # For angle n*15 or n*30

# -------------- VÝPOČET TAU ADDITION --------------
tau_approx_2 = cf.tau_compensation(delta, delta_approx_2)
tau_approx_3 = cf.tau_compensation(delta, delta_approx_3)

# --------------- TRANSFER FUNCTIONS CALCULATION ---------------
tf_quantized = cf.cardioid_tf_quantized(phi, delta, max_frequency)
tf_quantized_amplitudes = np.abs(tf_quantized)

tf_approx_2 = cf.cardioid_tf_tau(phi, delta_approx_2, tau_approx_2, max_frequency)
tf_approx_2_amplitudes = np.abs(tf_approx_2)

tf_approx_3 = cf.cardioid_tf_tau(phi, delta_approx_3, tau_approx_3, max_frequency)
tf_approx_3_amplitudes = np.abs(tf_approx_3)

# --------------- CONTROLLING BOUNDARIES ---------------
print(cf.are_arrays_within_bounds(tf_quantized_amplitudes, tf_approx_2_amplitudes, 0.00003))
print(cf.are_arrays_within_bounds(tf_quantized_amplitudes, tf_approx_3_amplitudes, 0.00003))

# --------------- DISPLAY PLOTS ---------------
frequencies = np.linspace(20, max_frequency, max_frequency - 19)

plt.figure(figsize=(12, 8))
## Transfer functions
plt.semilogx(frequencies, tf_quantized_amplitudes, color='cornflowerblue')
plt.semilogx(frequencies, tf_approx_2_amplitudes, color='lime')
plt.semilogx(frequencies, tf_approx_3_amplitudes, color='lightcoral')
## Plot display commands
plt.title('Transfer function of linear microphone array')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude')
plt.grid(True, which="both", ls="--")
plt.xlim(20, max_frequency)

plt.show()

