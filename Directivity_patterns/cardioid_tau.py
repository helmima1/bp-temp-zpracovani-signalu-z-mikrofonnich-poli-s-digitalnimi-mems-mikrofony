# import ComputationFunctions.ComputationFunctions as cf
import sys
import os
import numpy as np
import matplotlib.pyplot as plt

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
frequency = 300 # 1715
c = 343
phi = 0
theta = 90
M = 8
mic_distance = 0.1
mic_distance_approx = cf.approximation_radius(mic_distance, 1, 2)
radius = 0.05
resolution = 1000

# --------------- TAU CALCULATION ---------------
approx_tau = cf.tau_compensation(mic_distance, mic_distance_approx)

# --------------- BEAMPATTERN CALCULATION ---------------
beampattern_cardioid = cf.cardioid_beampattern(frequency, mic_distance, resolution, c=c)
amplitudes_cardioid = np.array([np.abs(x) for x in beampattern_cardioid]) # np.array(cf.signal_to_decibels([np.abs(x) for x in beampattern_cardioid]))

beampattern_cardioid_tau = cf.cardioid_beampattern(frequency, mic_distance_approx, resolution, approx_tau)
amplitudes_cardioid_tau = np.array([np.abs(x) for x in beampattern_cardioid_tau]) # np.array(cf.signal_to_decibels([np.abs(x) for x in beampattern_cardioid_tau]))

# print(amplitudes_cardioid)
print(cf.are_arrays_within_bounds(amplitudes_cardioid_tau, amplitudes_cardioid, 0.1))
# --------------- DISPLAY PLOT ---------------

fig, axes = plt.subplots(subplot_kw={'projection': 'polar'}, figsize=(4, 4), dpi=200)
reference_radians_values = np.linspace(0, 2 * np.pi, resolution)
axes.plot(reference_radians_values, amplitudes_cardioid, color='green')
axes.plot(reference_radians_values, amplitudes_cardioid, color='blue')
axes.set_ylim([0, 1])
# axes.set_yticks([-40, -30, -20, -10, 0])

plt.show()