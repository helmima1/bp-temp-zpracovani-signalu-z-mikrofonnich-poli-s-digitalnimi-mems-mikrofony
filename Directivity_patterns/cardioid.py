# import ComputationFunctions.ComputationFunctions as cf
import sys
import os
import numpy as np
import matplotlib.pyplot as plt

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
frequency = 900 # 1715
c = 343
phi = 0
theta = 90
M = 8
mic_distance = 0.1
radius = 0.05
resolution = 2000

# --------------- BEAMPATTERN CALCULATION ---------------

beampattern_cardioid = np.array(cf.cardioid_beampattern(frequency, mic_distance, resolution, c=c))
beampattern_dsb = np.array(cf.cma_beampattern(theta, phi, frequency, M, radius, c, resolution))
amplitudes_cardioid = np.array(cf.signal_to_decibels_old([np.abs(x) for x in beampattern_cardioid]))
amplitudes_dsb = np.array(cf.signal_to_decibels_old([np.abs(x) for x in beampattern_dsb]))

# Combination of cardioid and dsb
beampattern_dsb_cardioid = beampattern_cardioid * beampattern_dsb
amplitudes_dsb_cardioid = np.array(cf.signal_to_decibels_old([np.abs(x) for x in beampattern_dsb_cardioid]))

# --------------- DISPLAY PLOT ---------------

fig, axes = plt.subplots(subplot_kw={'projection': 'polar'}, figsize=(4, 4), dpi=200)
reference_radians_values = np.linspace(0, 2 * np.pi, resolution)
axes.plot(reference_radians_values, amplitudes_cardioid, color='green')
# axes.plot(reference_radians_values, amplitudes_dsb, color='blue')
# axes.plot(reference_radians_values, amplitudes_dsb_cardioid, color='red')
axes.set_ylim([-40, 10])
axes.set_yticks([-40, -30, -20, -10, 0])

plt.show()
