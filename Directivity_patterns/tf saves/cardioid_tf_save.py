# import ComputationFunctions.ComputationFunctions as cf
import sys
import os
import numpy as np
import matplotlib.pyplot as plt

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
max_frequency = 1000
c = 343
phi = 0
mic_distance = 0.1 # For angle 0
mic_distance_1 = 0.09238795325112868 # For angle 22.5
mic_distance_2 = 0.09326442173106045 # For angle 15

# --------------- TRANSFER FUNCTION CALCULATION ---------------

tf_1 = cf.cardioid_tf(0, mic_distance, max_frequency) # Transfer function
tf_amplitudes = np.array([np.abs(x) for x in tf_1])

tf_1 = cf.cardioid_tf(0, mic_distance_1, max_frequency) # Transfer function
tf_1_amplitudes = np.array([np.abs(x) for x in tf_1])

tf_2 = cf.cardioid_tf(0, mic_distance_2, max_frequency) # Transfer function
tf_2_amplitudes = np.array([np.abs(x) for x in tf_2])

index_min_prvku = np.argmin(tf_amplitudes)
# print(index_min_prvku)

# --------------- AVERAGE CALCULATION ---------------
procenta_1 = np.abs(tf_amplitudes/tf_1_amplitudes)
procenta_2 = np.abs(tf_amplitudes/tf_2_amplitudes)
prumer_1 = np.sum(np.abs(tf_amplitudes/tf_1_amplitudes))/tf_amplitudes.shape[0]
prumer_2 = np.sum(np.abs(tf_amplitudes/tf_2_amplitudes))/tf_amplitudes.shape[0]
print(f"Prumer 1: {prumer_1}, Prumer 2: {prumer_2}")

# Generování frekvenční osy
frekvence = np.linspace(20, max_frequency, len(tf_amplitudes))
# Vytvoření grafu
plt.figure(figsize=(12, 8)  )
# plt.semilogx(frekvence, tf_1_amplitudes)  # Použití logaritmické osy pro frekvence
plt.semilogx(frekvence, procenta_1, color='red')
plt.semilogx(frekvence, procenta_2, color='green')
plt.title('Přenosová funkce mikrofonního pole')
plt.xlabel('Frekvence (Hz)')
plt.ylabel('Amplituda (dB)')
plt.grid(True, which="both", ls="--")
plt.xlim(20, max_frequency)  # Nastavení limitů x-ové osy pro zobrazení od 20 Hz do 20 kHz

# Zobrazení grafu
plt.show()

