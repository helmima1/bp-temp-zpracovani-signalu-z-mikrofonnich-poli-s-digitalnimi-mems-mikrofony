"""
Poznámky:
1. Projedu všechny směry s a na každý z nich aplikuji odpovídající zpoždění.
2. Pro každý směr vypočítám RMS hodnotu (RMS budu počítat na velikost bufferu, což odpovídá 100 ms)
3. Všechny RMS zapíšu do pole
4. Promínu tyto hodnoty do polar plotu
5. Podle toho, kde je hodnota nejsilnější, zaměřím hlavní lalok
"""
import sys
import os
import sounddevice as sd
import numpy as np
from scipy.io.wavfile import write 
import matplotlib.pyplot as plt

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

print(sd.query_devices())
duration = 90  # determines how long a program should run in seconds
sd.default.device = 4, 3  # Selection of input and output devices (usually different for every computer)

# ------------------------------------------------------------
# ------------------- VARIABLES DEFINITION -------------------
# ------------------------------------------------------------
radius = 0.05  # radius of a circular microphone array in meters
M = 8  # number of microphones in circular microphone array
# theta = 90 # horizontal steering angle of beamforming algorithm
c = 343  # speed of sound in meters/seconds
sampling_frequency = 48000  # sampling frequency of microphones in Hz
buffer_size = 4800
channels_in = 16
number_of_tried_phi = 4 # number of horizontal directions that it will try while searching for strongest signal
number_of_tried_theta = 1 # number of vertical directions that it will try while searching for strongest signal

# ----------------------------------------------------------------
# ------------------------- CALCULATIONS -------------------------
# ----------------------------------------------------------------

## Buffer definition
history = None
previous_buffer = np.zeros((buffer_size, channels_in))

## Recording files
recording_normal = np.empty((0, channels_in))
recording_dsb = np.empty((0, 1))

## Source finder - number of angles
phi_angles = np.linspace(0, 360, number_of_tried_phi, endpoint=False)
phi_radians = np.linspace(0, 2*np.pi, number_of_tried_phi, endpoint=False)
#TODO: Vytvořit nějakou hezčí logiku, ať to nepočítá 0 a 90 stupňů..
theta_angles = np.linspace(90, 20, number_of_tried_theta) 

## Source finder - calculations of delays
# all_latencies is a 2 dimensional array storing latencies for 
all_latencies = np.zeros((number_of_tried_theta, number_of_tried_phi, 8), dtype=int)
for theta_index in range(0, number_of_tried_theta):
    for phi_index in range(0, number_of_tried_phi):
        # Calculate delays for current direction
        delays = cf.cma_tmi(radius, M, phi_angles[phi_index], theta_angles[theta_index], c)
        all_latencies[theta_index][phi_index] = np.array(cf.quantize_tmi_to_samples(delays, sampling_frequency), dtype=int)

## Functions
def calculate_rms_from_buffer(audio_buffer: np.array):
    """
    Calculates RMS value from a buffer 
    """
    # Vypočítá RMS hodnotu z bufferu
    rms_hodnota = np.sqrt(np.mean(audio_buffer**2))
    
    # Převede RMS hodnotu na decibely; referenční hodnota 1 pro digitální audio signály
    # Používáme 20 * log10(RMS/1) = 20 * log10(RMS)
    # Přidáváme malou hodnotu k RMS hodnotě, aby se předešlo logaritmu z nuly
    rms_v_db = 20 * np.log10(rms_hodnota + 1e-20)
    
    return rms_hodnota

def delay_signal_by_frames(delays: int, input_buffer: np.array, delay_buffer: np.array) -> np.array:
    """
    This function takes in previous buffer and uses it to delay data in the current "input_buffer" buffer. It re
    Signal cannot be delayed by more samples than the size of the buffer.
    :param delays: list of delays, where index of an array matches channel number
    :param input_buffer: current buffer
    :param delay_buffer: previous buffer
    :return:
    """
    if delays == 0:
        return input_buffer
    history = delay_buffer[-delays:]
    delayed_signal = np.concatenate((history, input_buffer))
    delayed_signal = delayed_signal[:-delays]
    return delayed_signal

def apply_delays_on_buffer(indata, previous_buffer, theta_index, phi_index, gain = 1):
    delayed_ch1 = delay_signal_by_frames(all_latencies[theta_index][phi_index][4], indata[:, 8], previous_buffer[:, 8])
    delayed_ch2 = delay_signal_by_frames(all_latencies[theta_index][phi_index][5], indata[:, 0], previous_buffer[:, 0])
    delayed_ch3 = delay_signal_by_frames(all_latencies[theta_index][phi_index][6], indata[:, 9], previous_buffer[:, 9])
    delayed_ch4 = delay_signal_by_frames(all_latencies[theta_index][phi_index][7], indata[:, 1], previous_buffer[:, 1])
    delayed_ch5 = delay_signal_by_frames(all_latencies[theta_index][phi_index][0], indata[:, 10], previous_buffer[:, 10])
    delayed_ch6 = delay_signal_by_frames(all_latencies[theta_index][phi_index][1], indata[:, 2], previous_buffer[:, 2])
    delayed_ch7 = delay_signal_by_frames(all_latencies[theta_index][phi_index][2], indata[:, 11], previous_buffer[:, 11])
    delayed_ch8 = delay_signal_by_frames(all_latencies[theta_index][phi_index][3], indata[:, 3], previous_buffer[:, 3])
    added_signals = np.empty((4800, 1))
    # Add delayed signals together
    for i in range(0, len(delayed_ch1)):
        added_signals[i] = (delayed_ch1[i] + 
                            delayed_ch2[i] + 
                            delayed_ch3[i] + 
                            delayed_ch4[i] + 
                            delayed_ch5[i] + 
                            delayed_ch6[i] + 
                            delayed_ch7[i] + 
                            delayed_ch8[i]) / M
        added_signals[i] *= gain
    return added_signals

fig = plt.figure()
ax = fig.add_subplot(111, polar=True)

# Inicializujte graf s počátečními daty
line, = ax.plot(phi_radians, np.full(number_of_tried_phi, 0.054))  # 'bo' pro modrý bod
ax.set_ylim([0.053, 0.059])
plt.ion()  # Zapne interaktivní mód
plt.show()

def callback(indata, outdata, frames, time, status):
    """ 
    This function delays signal in real time by using global variable delay_buffer to remember signals from the previous
    buffer. Both delayed and not delayed signal is then stored in global variables "recording_normal" and "recording_delayed"
    and then stored in wav files to the output_data folder.

    Delayed signal is stored in outdata and then played by sounddevice library in the device's speakers (delayed
    signal is converted to stereo for 2 user's device speakers).
    :param indata: Input data
    :param outdata: Output data
    :param frames:
    :param time:
    :param status:
    """
    if status:
        print(status)
    # If the signal is too low it can be amplified by raising value of this variable
    gain = 1

    # Delaying signal - searching for a write angle
    global previous_buffer
    global M
    rms_values = np.empty((number_of_tried_theta, number_of_tried_phi))
    for theta_index in range(0, number_of_tried_theta):
        for phi_index in range(0, number_of_tried_phi):
            added_signals = apply_delays_on_buffer(indata, previous_buffer, theta_index, phi_index, gain)
            rms_values[theta_index][phi_index] = calculate_rms_from_buffer(added_signals)

    # Calculating highest RMS value 
    # max_rms_index stores indices of the heighest RMS value relative to rms_values array
    max_rms_index = np.unravel_index(np.argmax(rms_values), rms_values.shape)
    max_rms_phi = phi_angles[max_rms_index[1]]
    max_rms_theta = theta_angles[max_rms_index[0]]
    print(f"Phi: {max_rms_phi}, Theta: {max_rms_theta}")

    # Diagram update
    # line.set_data(np.append(phi_radians, phi_radians[0]), np.append(rms_values[max_rms_index[0]], rms_values[max_rms_index[0]][0]))
    # fig.canvas.draw_idle()  # Použijeme draw_idle pro aktualizaci grafu
    # plt.pause(0.1)

    # Saving data into delay buffer for next time
    previous_buffer = indata.copy()

    # output stream
    outdata[:, 0] = apply_delays_on_buffer(indata, previous_buffer, max_rms_index[0], max_rms_index[1])[:, 0]
    outdata[:, 1] = outdata[:, 0]
    # outdata[:, 0] = added_signals[:, 0]
    # outdata[:, 1] = added_signals[:, 0]

    # Recording both added and not modified signal together for later analysis
    global recording_normal
    global recording_dsb
    recording_normal = np.concatenate((recording_normal, indata))
    recording_dsb = np.concatenate((recording_dsb, added_signals))


with sd.Stream(samplerate=sampling_frequency, channels=[16, 2], callback=callback, blocksize=buffer_size):
    sd.sleep(int(duration * 1000))
    plt.ioff()
    # Saving recorded data in wav files
    write("Loudest_Source_Finder/output_data/output_dsb.wav", sampling_frequency, recording_dsb)
    write("Loudest_Source_Finder/output_data/output_8ch_not_processed.wav", sampling_frequency, recording_normal)
    print("Finished")
    