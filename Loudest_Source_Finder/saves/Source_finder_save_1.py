"""
Poznámky:
1. Projedu všechny směry s a na každý z nich aplikuji odpovídající zpoždění.
2. Pro každý směr vypočítám RMS hodnotu (RMS budu počítat na velikost bufferu, což odpovídá 100 ms)
3. Všechny RMS zapíšu do pole
4. Promínu tyto hodnoty do polar plotu
5. Podle toho, kde je hodnota nejsilnější, zaměřím hlavní lalok
"""
import sys
import os
import sounddevice as sd
import numpy as np
from scipy.io.wavfile import write 
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

print(sd.query_devices())
duration = 5  # determines how long a program should run in seconds
sd.default.device = 4, 3  # Selection of input and output devices (usually different for every computer)

# ------------------------------------------------------------
# ------------------- VARIABLES DEFINITION -------------------
# ------------------------------------------------------------
radius = 0.05  # radius of a circular microphone array in meters
M = 8  # number of microphones in circular microphone array
theta = 90
# phi = 0
number_of_tried_directions = 16 # number of directions it will try
phi_angles = angles = np.linspace(0, 360, number_of_tried_directions, endpoint=False)
phi_radians = np.linspace(0, 2*np.pi, number_of_tried_directions, endpoint=False)
print(phi_angles)
c = 343  # speed of sound in meters/seconds
sampling_frequency = 48000  # sampling frequency of microphones in Hz

buffer_size = 4800
channels_in = 16
history = None
previous_buffer = np.zeros((buffer_size, channels_in))
recording_normal = np.empty((0, channels_in))
recording_dsb = np.empty((0, 1))
# delays = cf.cma_tmi(radius, M, phi, theta, c)
# sample_delays = cf.quantize_tmi_to_samples(delays, sampling_frequency)

# ----------------------------------------------------------------
# ------------------------- CALCULATIONS -------------------------
# ----------------------------------------------------------------

def calculate_rms_from_buffer(audio_buffer: np.array):
    """
    Calculates RMS value from a buffer 
    """
    rms_value = np.sqrt(np.mean(np.square(audio_buffer)))
    return rms_value

def delay_signal_by_frames(delays: list, input_buffer: np.array, delay_buffer: np.array) -> np.array:
    """
    This function takes in previous buffer and uses it to delay data in the current "input_buffer" buffer. It re
    Signal cannot be delayed by more samples than the size of the buffer.
    :param delays: list of delays, where index of an array matches channel number
    :param input_buffer: current buffer
    :param delay_buffer: previous buffer
    :return:
    """
    if delays == 0:
        return input_buffer
    history = delay_buffer[-delays:]
    delayed_signal = np.concatenate((history, input_buffer))
    delayed_signal = delayed_signal[:-delays]
    return delayed_signal

# Přidání potřebných proměnných pro graf
fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
line, = ax.plot([], [], 'r-')  # Inicializace prázdného plotu
ax.set_ylim(0, 1)  # Přizpůsobte podle rozsahu vašich RMS hodnot

def init():
    line.set_data([], [])
    return line,

def update_line(phi_radians, rms_values):
    line.set_data(phi_radians, rms_values)
    return line,

def callback(indata, outdata, frames, time, status):
    """
    This function delays signal in real time by using global variable delay_buffer to remember signals from the previous
    buffer. Both delayed and not delayed signal is then stored in global variables "recording_normal" and "recording_delayed"
    and then stored in wav files to the output_data folder.

    Delayed signal is stored in outdata and then played by sounddevice library in the device's speakers (delayed
    signal is converted to stereo for 2 user's device speakers).
    :param indata: Input data
    :param outdata: Output data
    :param frames:
    :param time:
    :param status:
    """
    # If the signal is too low it can be amplified by raising value of this variable
    gain = 1

    # Delaying signal - searching for a write angle
    global previous_buffer
    global M
    rms_values = []
    for phi in phi_angles:
        # Calculate delays for current direction 
        delays = cf.cma_tmi(radius, M, phi, theta, c)
        sample_delays = cf.quantize_tmi_to_samples(delays, sampling_frequency)
        # Apply delays on channels
        # 0 degrees is on P1 microphone
        delayed_ch1 = delay_signal_by_frames(sample_delays[0], indata[:, 8], previous_buffer[:, 8])
        delayed_ch2 = delay_signal_by_frames(sample_delays[1], indata[:, 0], previous_buffer[:, 0])
        delayed_ch3 = delay_signal_by_frames(sample_delays[2], indata[:, 9], previous_buffer[:, 9])
        delayed_ch4 = delay_signal_by_frames(sample_delays[3], indata[:, 1], previous_buffer[:, 1])
        delayed_ch5 = delay_signal_by_frames(sample_delays[4], indata[:, 10], previous_buffer[:, 10])
        delayed_ch6 = delay_signal_by_frames(sample_delays[5], indata[:, 2], previous_buffer[:, 2])
        delayed_ch7 = delay_signal_by_frames(sample_delays[6], indata[:, 11], previous_buffer[:, 11])
        delayed_ch8 = delay_signal_by_frames(sample_delays[7], indata[:, 3], previous_buffer[:, 3])
        added_signals = np.empty((4800, 1))
        # Add delayed signals together
        for i in range(0, len(delayed_ch1)):
            added_signals[i] = (delayed_ch1[i] + 
                                delayed_ch2[i] + 
                                delayed_ch3[i] + 
                                delayed_ch4[i] + 
                                delayed_ch5[i] + 
                                delayed_ch6[i] + 
                                delayed_ch7[i] + 
                                delayed_ch8[i]) / M
            added_signals[i] *= gain
        # calculate RMS value
        rms_values.append(calculate_rms_from_buffer(added_signals))  

    #TODO: Zobrazování dat do polárního grafu v reálném čase
    print(phi_angles[np.argmax(rms_values)])
    update_line(phi_radians, rms_values)

    # Saving data into delay buffer for next time
    previous_buffer = indata.copy()

    # output stream
    # outdata[:, 0] = added_signals[:, 0]
    # outdata[:, 1] = added_signals[:, 0]

    #TODO: teď to nijak neaplikuje beamforming
    outdata[:, 0] = indata[:, 0]
    outdata[:, 1] = indata[:, 0]

    # Recording both added and not modified signal together for later analysis
    global recording_normal
    global recording_dsb
    recording_normal = np.concatenate((recording_normal, indata))
    recording_dsb = np.concatenate((recording_dsb, added_signals))

ani = FuncAnimation(fig, update_line, init_func=init, blit=True, repeat=False)

with sd.Stream(samplerate=sampling_frequency, channels=[16, 2], callback=callback, blocksize=buffer_size):
    plt.show(block=False)
    sd.sleep(int(duration * 1000))
    # Saving recorded data in wav files
    write("Loudest_Source_Finder/output_data/output_dsb.wav", sampling_frequency, recording_dsb)
    write("Loudest_Source_Finder/output_data/output_8ch_not_processed.wav", sampling_frequency, recording_normal)
    print("Finished")
    