"""
Poznámky:
Ve funkci find_loudest_source provedu horní a dolní propust na 1 kHz
Vypočítám z nich směr, ze kterého přicházejí největší RMS
RMS pak sečtu
"""
import sys
import os
import multiprocessing
from functools import partial
import sounddevice as sd
import numpy as np
from scipy.io.wavfile import write 
import time

from scipy.signal import sosfilt, butter, sosfilt_zi
import ctypes

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

print(sd.query_devices())
duration = 50  # determines how long a program should run in seconds
sd.default.device = 3, 0    # Selection of input and output devices (usually different for every computer)

# ------------------------------------------------------------
# ------------------- VARIABLES DEFINITION -------------------
# ------------------------------------------------------------
radius = 0.05  # radius of a circular microphone array in meters
M = 8  # number of microphones in circular microphone array
c = 343  # speed of sound in meters/seconds
sampling_frequency = 48000  # sampling frequency of microphones in Hz
buffer_size = 4800
channels_in = 16
active_channels_ids = np.array([0, 1, 2, 3, 8, 9, 10, 11]) # Ids of channels, that are active in this current FPGA configuration

## Source finder DSB variables
number_of_tried_phi = 8 # number of horizontal directions that it will try while searching for strongest signal
number_of_tried_theta = 1 # number of vertical directions that it will try while searching for strongest signal
phi_angles = np.linspace(0, 360, number_of_tried_phi, endpoint=False)
theta_angles = np.linspace(90, 20, number_of_tried_theta) 
all_dsb_delays = cf.calculate_dsb_delays(radius, M, sampling_frequency, number_of_tried_theta, number_of_tried_phi, c)

## Cardioid variables
MICROPHONE_PAIRS = [[8, 10], [0, 2], [9, 11], [1, 3], [10, 8], [2, 0], [11, 9], [3, 1]]
SOUND_DELAY = (radius*2)/c
DELAY_IN_SAMPLES = round(SOUND_DELAY*sampling_frequency)

## Previous buffers definition
previous_buffer = np.zeros((buffer_size, channels_in)) # normal previous buffer
previous_buffer_noDC = np.zeros((buffer_size, channels_in)) # No DC Offset
previous_buffer_LP = np.zeros((buffer_size, channels_in)) # applied lowpass filter
previous_buffer_HP = np.zeros((buffer_size, channels_in)) # applied highpass filter

## Recording files (for later analysis/debugging only)
recording_normal = np.empty((buffer_size))
recording_dsb = np.empty((buffer_size))

## Source finder - multithreading
# this variable stores information about state of source finder, wheather the calculations are already finished or not.
direction_found_event = multiprocessing.Event()
direction_found_event.set()
SCHED_FIFO = 1
SCHED_RR = 2
pid = os.getpid()
libc = ctypes.CDLL("libc.so.6", use_errno=True)

## Information about current max rms direction index.
max_rms_index = (0, 0)

## Filtration variables
cutoff_freq = 1000
order = 6
SOS_LOW = butter(order, cutoff_freq, fs=sampling_frequency, btype='low', output='sos')
SOS_HIGH = butter(order, cutoff_freq, fs=sampling_frequency, btype='high', output='sos')
# Both arrays storing zi values have size 16, whether as active channels are only 8. 
# Therefore 8 channels will have always not set values. This is to prevent confusion.
#TODO: is it really the case? :)

zi_low = {channel: sosfilt_zi(SOS_LOW) for channel in active_channels_ids}
zi_high = {channel: sosfilt_zi(SOS_HIGH) for channel in active_channels_ids}

# ----------------------------------------------------------------
# ------------------------- CALCULATIONS -------------------------
# ----------------------------------------------------------------

def set_real_time_priority(pid, policy, priority):
    class SchedParam(ctypes.Structure):
        _fields_ = [("sched_priority", ctypes.c_int)]

    param = SchedParam(priority)

    result = libc.sched_setscheduler(pid, policy, ctypes.byref(param))
    if result != 0:
        err = ctypes.get_errno()  # Získání errno
        raise OSError(err, os.strerror(err))


def init_process():
    # Nastaví nižší prioritu pro tento proces
    os.nice(30)

def butter_filter(cutoff: int, fs:int, type, order=6):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    sos = butter(order, normal_cutoff, btype=type, analog=False, output='sos')
    return sos

def find_loudest_source_dsb_card_basic(queue: multiprocessing.Queue, event: multiprocessing.Event, indata: np.array, previous_buffer: np.array, number_of_tried_theta: int, number_of_tried_phi: int) -> np.array:
    """TODO

    :param queue: _description_
    :type queue: multiprocessing.Queue
    :param event: _description_
    :type event: multiprocessing.Event
    :param indata: _description_
    :type indata: np.array
    :param previous_buffer: _description_
    :type previous_buffer: np.array
    :param number_of_tried_theta: _description_
    :type number_of_tried_theta: int
    :param number_of_tried_phi: _description_
    :type number_of_tried_phi: int
    :return: _description_
    :rtype: np.array
    """ 
    try:
        set_real_time_priority(os.getpid(), 1, 10)
        print("Real-time priorita úspěšně nastavena.")
    except OSError as e:
        print(e)
    start_time = time.time()
    global zi_low
    global zi_high
    rms_values = np.empty((number_of_tried_theta, number_of_tried_phi))

    ## Filtration
    # Remove DC offset
    dc_offsets_in = np.mean(indata, axis=0)
    indata_noDC = indata - dc_offsets_in

    lp_buffer = np.empty(indata.shape)
    hp_buffer = np.empty(indata.shape)

    # Lowpass filtration
    for channel in active_channels_ids:
        lp_buffer[:, channel], zi_low[channel] = sosfilt(SOS_LOW, indata[:, channel], zi=zi_low[channel], axis=0)

    # Highpass filtration
    for channel in active_channels_ids:
        hp_buffer[:, channel], zi_high[channel] = sosfilt(SOS_LOW, indata[:, channel], zi=zi_high[channel], axis=0)

    ## Cardioid and DSB lowpass source finder
    lp_rms = np.empty(8)
    for pair_index in range(0, len(MICROPHONE_PAIRS)):
        # Cardioid
        delayed_buffer = cf.delay_signal_by_frames(DELAY_IN_SAMPLES, lp_buffer[:, MICROPHONE_PAIRS[pair_index][1]], previous_buffer_LP[:, MICROPHONE_PAIRS[pair_index][1]])
        leading_buffer = lp_buffer[:, MICROPHONE_PAIRS[pair_index][0]]
        cardioid_combined = leading_buffer - delayed_buffer

        # DSB
        dsb_buffer = cf.apply_delays_on_buffer(indata_noDC, previous_buffer_noDC, 0, pair_index, all_dsb_delays, M)

        # DSB and Cardioid combined
        combined = cardioid_combined * dsb_buffer

        # RMS calculation

        lp_rms[pair_index] = cf.calculate_rms_from_buffer(combined)

    rms_values[0] = lp_rms
    ## DSB
    # for theta_index in range(0, number_of_tried_theta):
    #     for phi_index in range(0, number_of_tried_phi):
    #         added_signals = cf.apply_delays_on_buffer(indata, previous_buffer, theta_index, phi_index, all_dsb_delays, M)
    #         rms_values[theta_index][phi_index] = cf.calculate_rms_from_buffer(added_signals)

    # Calculating highest RMS value 
    # max_rms_index stores indices of the heighest RMS value relative to rms_values array
    max_rms_index = np.unravel_index(np.argmax(rms_values), rms_values.shape)
    queue.put(max_rms_index)
    end_time = time.time()
    # print((end_time - start_time)*1000)
    event.set()

def callback(queue, indata, outdata, frames, time, status):
    """ 
    This function delays signal in real time by using global variable delay_buffer to remember signals from the previous
    buffer. Both delayed and not delayed signal is then stored in global variables "recording_normal" and "recording_delayed"
    and then stored in wav files to the output_data folder.

    Delayed signal is stored in outdata and then played by sounddevice library in the device's speakers (delayed
    signal is converted to stereo for 2 user's device speakers).
    :param indata: Input data
    :param outdata: Output data
    :param frames:
    :param time:
    :param status:
    """
    if status:
        print(status)
    global direction_found_event
    global previous_buffer
    global max_rms_index

    # Checking if calculation to find the loudest source was finished
    if (not queue.empty()):
        max_rms_index = queue.get_nowait()
        max_rms_phi = phi_angles[max_rms_index[1]]
        max_rms_theta = theta_angles[max_rms_index[0]]
        print(f"Theta: {max_rms_theta}, Phi: {max_rms_phi}")  

    # Starting a new calculations if the previous ones were calculated
    if direction_found_event.is_set():
        direction_found_event.clear()
        process = multiprocessing.Process(target=find_loudest_source_dsb_card_basic, args=(queue, direction_found_event, indata, previous_buffer, number_of_tried_theta, number_of_tried_phi))
        process.start()
        
    # output stream
    outdata[:, 0] = cf.apply_delays_on_buffer(indata, previous_buffer, max_rms_index[0], max_rms_index[1], all_dsb_delays, M)[:, 0]
    outdata[:, 1] = outdata[:, 0]

    # Saving data into delay buffer for next time
    previous_buffer = indata.copy()

    # Recording both added and not modified signal together for later analysis
    global recording_normal
    global recording_dsb
    recording_normal = np.concatenate((recording_normal, indata[:, 0]))
    recording_dsb = np.concatenate((recording_dsb, outdata[:, 0]))

# Callback wrapping for multiprocessing purposes
queue = multiprocessing.Queue()
wrapped_callback = partial(callback, queue)

with sd.Stream(samplerate=sampling_frequency, channels=[16, 2], callback=wrapped_callback, blocksize=buffer_size):
    sd.sleep(int(duration * 1000))
    # Saving recorded data in wav files
    # write("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/Ready_source_code/out_data/recording_normal.wav", sampling_frequency, recording_normal)
    # write("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/Ready_source_code/out_data/recording_dsb.wav", sampling_frequency, recording_dsb)
    # print("Finished")
    