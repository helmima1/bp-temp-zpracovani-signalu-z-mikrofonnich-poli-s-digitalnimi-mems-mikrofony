"""
Poznámky:
https://medium.com/analytics-vidhya/how-to-filter-noise-with-a-low-pass-filter-python-885223e5e9b7
"""
import sys
import os
import sounddevice as sd
import numpy as np
from scipy.io.wavfile import write 
import matplotlib.pyplot as plt
import multiprocessing
from multiprocessing.queues import Empty
from functools import partial

from scipy.signal import butter, lfilter, firwin, filtfilt, sosfilt, sosfilt_zi

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

print(sd.query_devices())
duration = 15  # determines how long a program should run in seconds
sd.default.device = 3, 4  # Selection of input and output devices (usually different for every computer)

# ------------------------------------------------------------
# ------------------- VARIABLES DEFINITION -------------------
# ------------------------------------------------------------
sampling_frequency = 48000  # sampling frequency of microphones in Hz
buffer_size = 4800
channels_in = 16
cutoff_freq = 1000  # Mezní frekvence dolní propusti
order = 6  # Řád filtru

microphone_pairs = [[8, 10], [0, 2], [9, 11], [1, 3], [10, 8], [2, 0], [11, 9], [3, 1]] 
# Budu určovat směr podle velikosti tohoto pole, tj. 180/velikost pole * index, potom ještě + 180

# ----------------------------------------------------------------
# ------------------------- CALCULATIONS -------------------------
# ----------------------------------------------------------------

## Recording files
recording_normal = np.empty((buffer_size))
recording_filtered = np.empty((buffer_size))

# First chatgpt try
def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

# FIR
def create_lowpass_filter(cutoff, fs, numtaps=101):
    # Vytvoření koeficientů FIR filtru s použitím firwin
    coeffs = firwin(numtaps, cutoff, nyq=fs/2, window='hamming')
    return coeffs

def apply_filter(data, filter_coeffs):
    # Aplikace filtru pomocí konvoluce
    filtered_data = lfilter(filter_coeffs, 1.0, data)
    return filtered_data

## Medium
def butter_lowpass_filter(data, cutoff_frequency, fs, order):
    normal_cutoff = cutoff_frequency / (0.5 * fs)
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y

def butter_sos_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    sos = butter(order, normal_cutoff, btype='low', analog=False, output='sos')
    return sos

# Globální proměnná pro uchování stavu filtru mezi voláními
zi_global = None

def callback(queue, indata, outdata, frames, time, status):
    """ 
    This function delays signal in real time by using global variable delay_buffer to remember signals from the previous
    buffer. Both delayed and not delayed signal is then stored in global variables "recording_normal" and "recording_delayed"
    and then stored in wav files to the output_data folder.

    Delayed signal is stored in outdata and then played by sounddevice library in the device's speakers (delayed
    signal is converted to stereo for 2 user's device speakers).
    :param indata: Input data
    :param outdata: Output data
    :param frames:
    :param time:
    :param status:
    """
    global zi_global
    if status:
        print(status)
    filter_coeffs = create_lowpass_filter(cutoff_freq, sampling_frequency)
    
    # Aplikujte dolní propust na každý kanál v indata
    # outdata[:, 0] = butter_lowpass_filter(indata[:, 0], cutoff_freq, sampling_frequency, order)
    # outdata[:, 1] = butter_lowpass_filter(indata[:, 0], cutoff_freq, sampling_frequency, order)
    # outdata[:, 0] = apply_filter(indata[:, 0], filter_coeffs)
    # outdata[:, 1] = apply_filter(indata[:, 0], filter_coeffs)

    # Kontrola, zda již byl inicializován stav filtru
    if zi_global is None:
        sos = butter_sos_lowpass(cutoff_freq, sampling_frequency, order=order)
        zi_global = sosfilt_zi(sos) * indata[0, 0]

    print(zi_global)
    print("-------------")

    sos = butter_sos_lowpass(cutoff_freq, sampling_frequency, order=order)
    outdata[:, 0], zi_global = sosfilt(sos, indata[:, 0], zi=zi_global, axis=0)
    outdata[:, 1] = outdata[:, 0]

    global recording_normal
    global recording_filtered
    recording_normal = np.concatenate((recording_normal, indata[:, 0]))
    recording_filtered = np.concatenate((recording_filtered, outdata[:, 0]))

queue = multiprocessing.Queue()
wrapped_callback = partial(callback, queue)

with sd.Stream(samplerate=sampling_frequency, channels=[16, 2], callback=wrapped_callback, blocksize=buffer_size):
    sd.sleep(int(duration * 1000))
    plt.ioff()
    # Saving recorded data in wav files
    write("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/Cardioid_DSB_combinated/out_data/recording_normal.wav", sampling_frequency, recording_normal)
    write("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/Cardioid_DSB_combinated/out_data/recording_filtered.wav", sampling_frequency, recording_filtered)
    # print("Finished")
    