"""
Poznámky:
1. Projedu všechny směry s a na každý z nich aplikuji odpovídající zpoždění.
2. Pro každý směr vypočítám RMS hodnotu (RMS budu počítat na velikost bufferu, což odpovídá 100 ms)
3. Všechny RMS zapíšu do pole
4. Promínu tyto hodnoty do polar plotu
5. Podle toho, kde je hodnota nejsilnější, zaměřím hlavní lalok
"""
import sys
import os
import sounddevice as sd
import numpy as np
from scipy.io.wavfile import write 
import matplotlib.pyplot as plt
import multiprocessing
from multiprocessing.queues import Empty
from functools import partial
import time

from scipy.signal import butter, lfilter

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

print(sd.query_devices())
duration = 100  # determines how long a program should run in seconds
sd.default.device = 3, 4  # Selection of input and output devices (usually different for every computer)

# ------------------------------------------------------------
# ------------------- VARIABLES DEFINITION -------------------
# ------------------------------------------------------------
radius = 0.05  # radius of a circular microphone array in meters
M = 8  # number of microphones in circular microphone array
mic_dist = 0.1 
c = 343  # speed of sound in meters/seconds
sampling_frequency = 48000  # sampling frequency of microphones in Hz
buffer_size = 4800
channels_in = 16
number_of_tried_phi = 8 # number of horizontal directions that it will try while searching for strongest signal
number_of_aprox_phi = 1 #TODO: Dodělat i pro thetu
number_of_tried_theta = 1 # number of vertical directions that it will try while searching for strongest signal

microphone_pairs = [[8, 10], [0, 2], [9, 11], [1, 3], [10, 8], [2, 0], [11, 9], [3, 1]] 
# Budu určovat směr podle velikosti tohoto pole, tj. 180/velikost pole * index, potom ještě + 180

## Buffer definition
history = None
previous_buffer = np.zeros((buffer_size, channels_in))
previous_buffer_noDC = np.zeros((buffer_size, channels_in)) # No DC Offset
previous_buffer_LP = np.zeros((buffer_size, channels_in)) # applied lowpass filter
previous_buffer_HP = np.zeros((buffer_size, channels_in)) # applied highpass filter

## Recording files
recording_normal = np.empty((0, channels_in))
recording_dsb = np.empty((0, 1))

## Source finder - number of angles
phi_angles = np.linspace(0, 360, number_of_tried_phi*number_of_aprox_phi, endpoint=False)
phi_radians = np.linspace(0, 2*np.pi, number_of_tried_phi*number_of_aprox_phi, endpoint=False)
#TODO: Vytvořit nějakou hezčí logiku, ať to nepočítá 0 a 90 stupňů..
theta_angles = np.linspace(90, 20, number_of_tried_theta) 

## Source finder - calculations of delays
# all_latencies is a 2 dimensional array storing latencies for 
all_latencies = np.zeros((number_of_tried_theta, number_of_tried_phi*number_of_aprox_phi, 8), dtype=int)
for theta_index in range(0, number_of_tried_theta):
    for phi_index in range(0, number_of_tried_phi*number_of_aprox_phi):
        # Calculate delays for current direction
        delays = cf.cma_tmi(radius, M, phi_angles[phi_index], theta_angles[theta_index], c)
        all_latencies[theta_index][phi_index] = np.array(cf.quantize_tmi_to_samples(delays, sampling_frequency), dtype=int)

## Source finder - multithreading
zpracovani_event = multiprocessing.Event()
zpracovani_event.set()
max_rms_index = (0, 0)

## Filters constants
filter_frequency = 1500
b_lp, a_lp = butter(N=6, Wn=filter_frequency / (sampling_frequency / 2), btype='low') 
b_hp, a_hp = butter(N=6, Wn=filter_frequency / (sampling_frequency / 2), btype='high') 
active_channels = [0, 1, 2, 3, 7, 8, 9, 10]

# ----------------------------------------------------------------
# ------------------------- CALCULATIONS -------------------------
# ----------------------------------------------------------------

## Functions

def calculate_direction(args):
    """
    Calculates rms for direction specified in theta_index and phi_index. Those indices correspond to all_latencies
    variable, where they correspond to a certain angles. This is to speed up the system, so new latencies are not
    calculated every time they are being used.

    Purpose of this function is to speed up the algorithm, each of those functions are ment to be calculated in
    different thread.
    """
    indata, previous_buffer, theta_index, phi_index = args
    return cf.calculate_rms_from_buffer(cf.apply_delays_on_buffer(indata, previous_buffer, theta_index, phi_index)), theta_index, int(phi_index/number_of_aprox_phi)

def calculate_direction_aprox(args):
    """
    Calculates rms for direction specified in theta_index and phi_index. Those indices correspond to all_latencies
    variable, where they correspond to a certain angles. This is to speed up the system, so new latencies are not
    calculated every time they are being used.

    Purpose of this function is to speed up the algorithm, each of those functions are ment to be calculated in
    different thread.
    """
    indata, previous_buffer, theta_index, phi_index = args
    return calculate_rms_from_buffer(apply_delays_on_buffer(indata, previous_buffer, theta_index, phi_index)), theta_index, phi_index % number_of_aprox_phi

def init_process():
    # Nastaví nižší prioritu pro tento proces
    os.nice(20)

def find_aprox_indices(rms_max_index, rms_second_max_index):
    """
    This function calculates inicies of microphones that are to be approximated. 
    It returns list of microphone indices that are to be approximated + id identyfying scenario case that happened:
    
    0 - max is 0 and second_max is last index (první je second max)
    1 - second_max is 0 and max is last index (první je max)
    2 - max is smaller than second_max (první je max)
    3 - second_max is smaller than max (první je second max)
    """
    if rms_max_index[1] == 0 and rms_second_max_index[1] == number_of_tried_phi - 1:
        return ([i for i in range((number_of_tried_phi - 1)*number_of_aprox_phi + 1, (number_of_tried_phi)*number_of_aprox_phi)], 0)
    elif rms_max_index[1] == number_of_tried_phi - 1 and rms_second_max_index[1] == 0:
        return ([i for i in range((number_of_tried_phi - 1)*number_of_aprox_phi + 1, (number_of_tried_phi)*number_of_aprox_phi)], 1)
    elif rms_max_index[1] < rms_second_max_index[1]:
        return ([i for i in range(rms_max_index[1]*number_of_aprox_phi+1, rms_second_max_index[1]*number_of_aprox_phi)], 2)
    elif rms_max_index[1] > rms_second_max_index[1]:
        return ([i for i in range(rms_second_max_index[1]*number_of_aprox_phi+1, rms_max_index[1]*number_of_aprox_phi)], 3)

def find_loudest_source_dsb(queue, event, indata, previous_buffer, number_of_tried_theta: int, number_of_tried_phi: int):
    """
    Iterates through given amount of directions and finds the loudest one. Number of directions is defined for
    number of thetas and phis specified in variables number_of_tried_theta and number_of_tried_phi.
    """
    start_time = time.time()
    rms_values = np.empty((number_of_tried_theta, number_of_tried_phi))

    # Multithreading for every tried direction
    args = [(indata, previous_buffer, theta_index, phi_index) 
        for theta_index in range(number_of_tried_theta) 
        for phi_index in range(0, number_of_tried_phi*number_of_aprox_phi, number_of_aprox_phi)]

    with multiprocessing.Pool(initializer=init_process, processes=multiprocessing.cpu_count()) as pool:
        results = pool.map(calculate_direction, args)

    for result in results:
        rms_value, theta_index, phi_index = result
        rms_values[theta_index][phi_index] = rms_value

    # Calculating highest RMS value 
    # max_rms_index stores indices of the heighest RMS value relative to rms_values array
    max_rms_index = np.unravel_index(np.argmax(rms_values), rms_values.shape)
    # Find out which side mic is stronger
    if rms_values[max_rms_index[0]][(max_rms_index[1]-1)%number_of_tried_phi] > rms_values[max_rms_index[0]][(max_rms_index[1]+1)%number_of_tried_phi]:
        second_max_rms_index = (max_rms_index[0], (max_rms_index[1]-1)%number_of_tried_phi)
    else:
        second_max_rms_index = (max_rms_index[0], (max_rms_index[1]+1)%number_of_tried_phi)

    # Approximate phi
    # Počítám vlastně jenom 3 hodnoty + max a second max. 
    # Do rms_values_aprox pak započítávám i ty hraniční, tudíž tam mám 5 hodnot.
    aprox_indices = find_aprox_indices(max_rms_index, second_max_rms_index)
    rms_values_aprox = np.empty((number_of_tried_theta, number_of_aprox_phi + 1))

    if aprox_indices[1] == 0 or aprox_indices[1] == 3:
        rms_values_aprox[0][0] = rms_values[0][second_max_rms_index[1]]
        rms_values_aprox[0][number_of_aprox_phi] = rms_values[0][max_rms_index[1]]
    elif aprox_indices[1] == 1 or aprox_indices[1] == 2:
        rms_values_aprox[0][0] = rms_values[0][max_rms_index[1]]
        rms_values_aprox[0][number_of_aprox_phi] = rms_values[0][second_max_rms_index[1]]

    args = [(indata, previous_buffer, theta_index, phi_index) 
        for theta_index in range(number_of_tried_theta) 
        for phi_index in aprox_indices[0]]

    with multiprocessing.Pool(initializer=init_process, processes=multiprocessing.cpu_count()) as pool:
        # co když to tady nejsem schopný správně namapovat?
        results = pool.map(calculate_direction_aprox, args)

    for result in results:
        rms_value, theta_index, phi_index = result
        rms_values_aprox[theta_index][phi_index] = rms_value

    # Teď mám v rms_values_aprox mít 5 hodnot, které odpovídají těm aproximovaným rms hodnotám
        
    # Teď potřebuji přijít na nejvyšší hodnotu z rms_values_aprox a spárovat jí s aprox_indices
    if aprox_indices[1] == 0 or aprox_indices[1] == 3:
        # prvni je second max
        aprox_indices = [[second_max_rms_index[1]*number_of_aprox_phi] + aprox_indices[0] + [max_rms_index[1]*number_of_aprox_phi]]
    elif aprox_indices[1] == 1 or aprox_indices[1] == 2:
        aprox_indices = [[max_rms_index[1]*number_of_aprox_phi] + aprox_indices[0] + [second_max_rms_index[1]*number_of_aprox_phi]]
    
    max_aprox_rms_value = np.unravel_index(np.argmax(rms_values_aprox), rms_values_aprox.shape)
    max_aprox_rms_index = (0, aprox_indices[max_aprox_rms_value[0]][max_aprox_rms_value[1]]) #TODO: theta

    queue.put(max_aprox_rms_index)
    end_time = time.time()
    # print((end_time - start_time)*1000)
    event.set()

def find_source_cardioid(queue, event, indata, previous_buffer):
    sound_delay = mic_dist/c 
    delay_in_samples = round(sound_delay*sampling_frequency)
    measured_rms = [None] * 8
    for pair_index in range(0, len(microphone_pairs)):
        # Vypočet úhlu
        current_angle = (180/len(microphone_pairs))*pair_index
        current_angle_inverse = current_angle + 180
        print(f"current_angle: {current_angle}, inverse_angle: {current_angle_inverse}, index {microphone_pairs[pair_index]}")

        # cardioida výpočet
        delayed_data_f = delay_signal_by_frames(delay_in_samples, indata[:, microphone_pairs[pair_index][1]], previous_buffer[:, microphone_pairs[pair_index][1]])
        leading_data_f = indata[:, microphone_pairs[pair_index][0]]
        cardioid_data_forward = leading_data_f - delayed_data_f

        delayed_data_b = delay_signal_by_frames(delay_in_samples, indata[:, microphone_pairs[pair_index][0]], previous_buffer[:, microphone_pairs[pair_index][0]])
        leading_data_b = indata[:, microphone_pairs[pair_index][1]]
        cardioid_data_backwards = leading_data_b - delayed_data_b

        measured_rms[pair_index] = calculate_rms_from_buffer(cardioid_data_forward)
        measured_rms[pair_index + 4] = calculate_rms_from_buffer(cardioid_data_backwards)
    
    max_rms_index = measured_rms.index(max(measured_rms))
    print(measured_rms)
    queue.put((0, max_rms_index))
    event.set()

def find_source_dsb_and_cardioid_filter(queue, event, indata, previous_buffer):
    """
    This function combines cardioid and dsb algorithm to seach for the loudest source. 
    This version searches only 8 directions, since cardioid can be created only for pairs of microphone.
    For better performance on lower frequencies, it is combinated with cardioid for lower frequencies since it has 
    better directivity

    I'll make the cardioid + dsb from lowpass filtered buffers and then dsb from the highpass filtered buffers.
    """
    global previous_buffer_noDC
    global previous_buffer_HP
    global previous_buffer_LP

    sound_delay = mic_dist/c 
    delay_in_samples = round(sound_delay*sampling_frequency)
    measured_rms = [None] * 8

    # Remove dc_offset
    dc_offsets_in = np.mean(indata, axis=0)
    indata_noDC = indata - dc_offsets_in

    #TODO: Apply lowpass and highpass filter on current buffer
    indata_lowpass = apply_lowpass(indata)
    indata_highpass = apply_highpass(indata)

    # For this to work I also need the filters to be applied to the previous buffer
    # For maximum efectivity it would be good to store the filtered signal in the previous buffers

    # Search for different directions
    for pair_index in range(0, len(microphone_pairs)):
        # Cardioid calculation 
        delayed = delay_signal_by_frames(delay_in_samples, indata_noDC[:, microphone_pairs[pair_index][1]], previous_buffer_noDC[:, microphone_pairs[pair_index][1]])
        leading = indata_noDC[:, microphone_pairs[pair_index][0]]
        cardioid_forward = leading - delayed

        # DSB calculation forward
        dsb = apply_delays_on_buffer(indata_noDC, previous_buffer_noDC, 0, pair_index)
        combined_data_forward = cardioid_forward * dsb

        measured_rms[pair_index] = calculate_rms_from_buffer(combined_data_forward)
    
    max_rms_index = measured_rms.index(max(measured_rms))
    queue.put((0, max_rms_index))

    previous_buffer_noDC = indata_noDC.copy()
    event.set()

def callback(queue, indata, outdata, frames, time, status):
    """ 
    This function delays signal in real time by using global variable delay_buffer to remember signals from the previous
    buffer. Both delayed and not delayed signal is then stored in global variables "recording_normal" and "recording_delayed"
    and then stored in wav files to the output_data folder.

    Delayed signal is stored in outdata and then played by sounddevice library in the device's speakers (delayed
    signal is converted to stereo for 2 user's device speakers).
    :param indata: Input data
    :param outdata: Output data
    :param frames:
    :param time:
    :param status:
    """
    if status:
        print(status)
    global zpracovani_event
    global previous_buffer
    global max_rms_index
    try:
        # Zkoušíme přečíst z fronty, ale neblokujeme, pokud fronta je prázdná
        max_rms_index = queue.get_nowait()
        # print(max_rms_index[1]*number_of_aprox_phi)
        max_rms_phi = phi_angles[max_rms_index[1]]
        max_rms_theta = theta_angles[max_rms_index[0]]
        print(f"Theta: {max_rms_theta}, Phi: {max_rms_phi}")
    except Empty:
        # Žádná data k dispozici
        pass    

    if status:
        print(status)
    if zpracovani_event.is_set():
        zpracovani_event.clear()  # Resetování události pro další zpracování
        process = multiprocessing.Process(target=find_source_dsb_and_cardioid_filter, args=(queue, zpracovani_event, indata, previous_buffer))
        process.start()
        
    # Saving data into delay buffer for next time
    previous_buffer = indata.copy()

    # output stream
    # outdata[:, 0] = apply_delays_on_buffer(indata, previous_buffer, max_rms_index[0], max_rms_index[1])[:, 0]
    outdata[:, 0] = lfilter(b_lp, a_lp, indata[:, 0])
    outdata[:, 1] = outdata[:, 0]

    #TODO: ukládání do souboru

queue = multiprocessing.Queue()
wrapped_callback = partial(callback, queue)

with sd.Stream(samplerate=sampling_frequency, channels=[16, 2], callback=wrapped_callback, blocksize=buffer_size):
    sd.sleep(int(duration * 1000))
    plt.ioff()
    # Saving recorded data in wav files
    # write("Loudest_Source_Finder/output_data/output_dsb.wav", sampling_frequency, recording_dsb)
    # write("Loudest_Source_Finder/output_data/output_8ch_not_processed.wav", sampling_frequency, recording_normal)
    print("Finished")
    