"""
Poznámky:
https://medium.com/analytics-vidhya/how-to-filter-noise-with-a-low-pass-filter-python-885223e5e9b7
"""
import sys
import os
import sounddevice as sd
import numpy as np
from scipy.io.wavfile import write 
import matplotlib.pyplot as plt
import multiprocessing
from multiprocessing.queues import Empty
from functools import partial

from scipy.signal import butter, lfilter, firwin, filtfilt, sosfilt, sosfilt_zi

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

print(sd.query_devices())
duration = 115  # determines how long a program should run in seconds
sd.default.device = 3, 5  # Selection of input and output devices (usually different for every computer)

# ------------------------------------------------------------
# ------------------- VARIABLES DEFINITION -------------------
# ------------------------------------------------------------
sampling_frequency = 48000  # sampling frequency of microphones in Hz
buffer_size = 4800
channels_in = 16
cutoff_freq = 1000  # Mezní frekvence dolní propusti
order = 6  # Řád filtru

microphone_pairs = [[8, 10], [0, 2], [9, 11], [1, 3], [10, 8], [2, 0], [11, 9], [3, 1]] 
active_channels_ids = np.array([0, 1, 2, 3, 8, 9, 10, 11])
# Budu určovat směr podle velikosti tohoto pole, tj. 180/velikost pole * index, potom ještě + 180

# ----------------------------------------------------------------
# ------------------------- CALCULATIONS -------------------------
# ----------------------------------------------------------------

## Recording files
recording_normal = np.empty((buffer_size))
recording_filtered = np.empty((buffer_size))
zi_low_global = None

def butter_sos_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    sos = butter(order, normal_cutoff, btype='low', analog=False, output='sos')
    return sos

def butter_filter(cutoff: int, fs:int, type, order=6):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    sos = butter(order, normal_cutoff, btype=type, analog=False, output='sos')
    return sos

# Navržení SOS filtru
sos = butter(order, cutoff_freq, fs=sampling_frequency, btype='low', output='sos')

# Inicializace počátečního stavu 'zi' pro každý filtrující kanál
zi = {channel: sosfilt_zi(sos) for channel in active_channels_ids}

# Globální proměnná pro uchování stavu filtru mezi voláními
zi_global = None

def callback(queue, indata, outdata, frames, time, status):
    """ 
    This function delays signal in real time by using global variable delay_buffer to remember signals from the previous
    buffer. Both delayed and not delayed signal is then stored in global variables "recording_normal" and "recording_delayed"
    and then stored in wav files to the output_data folder.

    Delayed signal is stored in outdata and then played by sounddevice library in the device's speakers (delayed
    signal is converted to stereo for 2 user's device speakers).
    :param indata: Input data
    :param outdata: Output data
    :param frames:
    :param time:
    :param status:
    """
    global zi_global
    global zi_low_global
    if status:
        print(status)

    # Kontrola, zda již byl inicializován stav filtru
    dc_offsets_in = np.mean(indata, axis=0)
    indata_noDC = indata - dc_offsets_in

    # Setting initial state for filter if not set before
    # Need to be done for all the channels, otherweise there would be clicks
    # TODO: test if the state can be stored only once for all channels, since the signal they are all getting
    #  is very similar.
    # sos = butter_filter(cutoff_freq, sampling_frequency, 'low', order=order)
    # if zi_low_global is None:
    #     zi_low_global = np.empty((channels_in, 3, 2)) #TODO: Funguje pouze pro order=6
    #     for channel in active_channels_ids:
    #         zi_low_global[channel] = sosfilt_zi(sos) * indata_noDC[0, channel]
    

    # Filtration
    # for channel in active_channels_ids:
    #     indata_noDC[:, channel], zi_low_global[channel] = sosfilt(sos, indata_noDC[:, channel], zi=zi_low_global[channel], axis=0)

    # outdata[:, 0] = indata_noDC[:, 0]
    # outdata[:, 1] = indata_noDC[:, 1]

    # indata_filtered = indata.copy()

    # Aplikujeme filtr na vybrané kanály
    for channel in active_channels_ids:
        indata_noDC[:, channel], zi[channel] = sosfilt(sos, indata[:, channel], zi=zi[channel], axis=0)

    outdata[:, 0] = indata_noDC[:, 0]
    outdata[:, 1] = indata_noDC[:, 1]

    global recording_normal
    global recording_filtered
    recording_normal = np.concatenate((recording_normal, indata[:, 0]))
    recording_filtered = np.concatenate((recording_filtered, outdata[:, 0]))

queue = multiprocessing.Queue()
wrapped_callback = partial(callback, queue)

with sd.Stream(samplerate=sampling_frequency, channels=[16, 2], callback=wrapped_callback, blocksize=buffer_size):
    sd.sleep(int(duration * 1000))
    plt.ioff()
    # Saving recorded data in wav files
    write("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/Cardioid_DSB_combinated/out_data/recording_normal.wav", sampling_frequency, recording_normal)
    write("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/Cardioid_DSB_combinated/out_data/recording_filtered.wav", sampling_frequency, recording_filtered)
    # print("Finished")
    