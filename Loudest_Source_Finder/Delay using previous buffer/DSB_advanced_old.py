"""
Poznámky:
1. Projedu všechny směry s a na každý z nich aplikuji odpovídající zpoždění.
2. Pro každý směr vypočítám RMS hodnotu (RMS budu počítat na velikost bufferu, což odpovídá 100 ms)
3. Všechny RMS zapíšu do pole
4. Promínu tyto hodnoty do polar plotu
5. Podle toho, kde je hodnota nejsilnější, zaměřím hlavní lalok
"""
import sys
import os
import multiprocessing
from functools import partial
import sounddevice as sd
import numpy as np
from scipy.io.wavfile import write 
import time

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

print(sd.query_devices())
duration = 20  # determines how long a program should run in seconds
sd.default.device = 4, 0     # Selection of input and output devices (usually different for every computer)

# ------------------------------------------------------------
# -------------- VARIABLES/CONSTANTS DEFINITION --------------
# ------------------------------------------------------------
## General constants
RADIUS = 0.05  # radius of a circular microphone array in meters
M = 8  # number of microphones in circular microphone array
C = 343  # speed of sound in meters/seconds
SAMPLING_FREQUENCY = 48000  # sampling frequency of microphones in Hz
BUFFER_SIZE = 4800
CHANNELS_IN = 16
ACTIVE_CHANNELS_IDS = np.array([0, 1, 2, 3, 8, 9, 10, 11]) # Ids of active channels in this current FPGA configuration

## Number of directions that the source finder is looking in
NUMBER_OF_TRIED_PHI = 64 # number of horizontal directions that it will try while searching for strongest signal
NUMBER_OF_TRIED_THETA = 1 # number of vertical directions that it will try while searching for strongest signal
PHI_ANGLES = np.linspace(0, 360, NUMBER_OF_TRIED_PHI, endpoint=False)
THETA_ANGLES = np.linspace(90, 20, NUMBER_OF_TRIED_THETA) 
ALL_DSB_DELAYS = cf.calculate_dsb_delays(RADIUS, M, SAMPLING_FREQUENCY, NUMBER_OF_TRIED_THETA, NUMBER_OF_TRIED_PHI, C)
# This stores index of delays relative to ALL_DSB_DELAYS array. They correspond to (theta, phi) angles.
# Initial state is set to 0, 0, propper source will be calculated with first run of source_finder function
max_rms_index = (0, 0) 

## Previous buffer definition
previous_buffer = np.zeros((BUFFER_SIZE, CHANNELS_IN))

## Recording files (for later analysis/debugging only)
recording_normal = np.empty((BUFFER_SIZE))
recording_dsb = np.empty((BUFFER_SIZE))

## Source finder - variables
direction_found_event = multiprocessing.Event()
direction_found_event.set()
REFRESH_TIME = 1000 # Minimal time intervals in which the calculation should be made. Given in miliseconds
last_refresh = time.time() 
first_run = True

# ----------------------------------------------------------------
# ------------------------- CALCULATIONS -------------------------
# ----------------------------------------------------------------

def calculate_direction(args):
    """
    Calculates rms for direction specified in theta_index and phi_index. Those indices correspond to all_latencies
    variable, where they correspond to a certain angles. This is to speed up the system, so new latencies are not
    calculated every time they are being used.

    Purpose of this function is to speed up the algorithm, each of those functions are ment to be calculated in
    different thread.
    """
    indata, previous_buffer, theta_index, phi_index = args
    return cf.calculate_rms_from_buffer(cf.apply_delays_on_buffer(indata, previous_buffer, theta_index, phi_index, ALL_DSB_DELAYS, M)), theta_index, phi_index

def init_process():
    os.nice(30)

def find_loudest_source_advanced(queue, event, indata, previous_buffer, number_of_tried_theta: int, number_of_tried_phi: int):
    """
    Iterates through given amount of directions and finds the loudest one. Number of directions is defined for
    number of thetas and phis specified in variables number_of_tried_theta and number_of_tried_phi.
    """
    start_time = time.time()
    rms_values = np.empty((number_of_tried_theta, number_of_tried_phi))

    args = [(indata, previous_buffer, theta_index, phi_index) 
            for theta_index in range(number_of_tried_theta) 
            for phi_index in range(number_of_tried_phi)]

    with multiprocessing.Pool(initializer=init_process, processes=multiprocessing.cpu_count()) as pool:
        results = pool.map(calculate_direction, args)

    for result in results:
        rms_value, theta_index, phi_index = result
        rms_values[theta_index][phi_index] = rms_value

    # Calculating highest RMS value 
    # max_rms_index stores indices of the heighest RMS value relative to rms_values array
    max_rms_index = np.unravel_index(np.argmax(rms_values), rms_values.shape)
    queue.put(max_rms_index)
    end_time = time.time()
    print((end_time - start_time)*1000)
    event.set()

def callback(queue, indata, outdata, frames, t, status):
    """ 
    This function delays signal in real time by using global variable delay_buffer to remember signals from the previous
    buffer. Both delayed and not delayed signal is then stored in global variables "recording_normal" and "recording_delayed"
    and then stored in wav files to the output_data folder.

    Delayed signal is stored in outdata and then played by sounddevice library in the device's speakers (delayed
    signal is converted to stereo for 2 user's device speakers).
    :param indata: Input data
    :param outdata: Output data
    :param frames:
    :param time:
    :param status:
    """
    # if status:
    #     print(status)
    global direction_found_event
    global previous_buffer
    global max_rms_index
    global last_refresh
    global first_run

    # Checking if calculation to find the loudest source was finished
    if (not queue.empty()):
        max_rms_index = queue.get_nowait()
        max_rms_phi = PHI_ANGLES[max_rms_index[1]]
        max_rms_theta = THETA_ANGLES[max_rms_index[0]]
        # print(f"Theta: {max_rms_theta}, Phi: {max_rms_phi}")  

    # Starting a new calculations if the previous ones were calculated
    if direction_found_event.is_set() and ((time.time() - last_refresh)*1000 > REFRESH_TIME or first_run):
        if first_run:
            first_run = False
        last_refresh = time.time()
        direction_found_event.clear()
        process = multiprocessing.Process(target=find_loudest_source_advanced, args=(queue, direction_found_event, indata, previous_buffer, NUMBER_OF_TRIED_THETA, NUMBER_OF_TRIED_PHI))
        process.start()
        
    # output stream
    outdata[:, 0] = cf.apply_delays_on_buffer(indata, previous_buffer, max_rms_index[0], max_rms_index[1], ALL_DSB_DELAYS, M)[:, 0]
    outdata[:, 1] = outdata[:, 0]

    # Saving data into delay buffer for next time
    previous_buffer = indata.copy()

    # Recording both added and not modified signal together for later analysis
    global recording_normal
    global recording_dsb
    recording_normal = np.concatenate((recording_normal, indata[:, 0]))
    recording_dsb = np.concatenate((recording_dsb, outdata[:, 0]))

# Callback wrapping for multiprocessing purposes
queue = multiprocessing.Queue()
wrapped_callback = partial(callback, queue)

with sd.Stream(samplerate=SAMPLING_FREQUENCY, channels=[16, 2], callback=wrapped_callback, blocksize=BUFFER_SIZE):
    sd.sleep(int(duration * 1000))
    # Saving recorded data in wav files
    # write("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/Ready_source_code/out_data/recording_normal.wav", sampling_frequency, recording_normal)
    # write("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/Ready_source_code/out_data/recording_dsb.wav", sampling_frequency, recording_dsb)
    # print("Finished")