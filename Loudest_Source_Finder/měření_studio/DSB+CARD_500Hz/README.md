All tests were done using 2 speakers positioned in 0 and 90 degrees relative to the microphone array. Speakers were both in the same distance from the microphone array.

Tests were done by changing loudness of the second speaker (positioned in 90 degrees), whether as second speaker was set to 0 dB all the time. By how much was the loudness of second speaker lowered is visible in the file name. 

Tests were done using sine wave of frequency 500 Hz generated in Audacity.