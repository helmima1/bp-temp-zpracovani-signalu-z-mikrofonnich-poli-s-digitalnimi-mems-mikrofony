"""
This script uses linear combination of neighbouring microphones to calculate cardioid 
in between them. This gives a user more power to differenciate between more possible angles of 
source directions.
ALLOWED NUMBER OF PHI DIRECTIONS: 8, 16, 24, 32, 40, 48, 56, 64
"""
import sys
import os
import multiprocessing
from functools import partial
import sounddevice as sd
import numpy as np
from scipy.io.wavfile import write 
import time

from scipy.signal import sosfilt, butter

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

print(sd.query_devices())
duration = 44  # determines how long a program should run in seconds
sd.default.device = 3, 0    # Selection of input and output devices (usually different for every computer)

# ------------------------------------------------------------
# -------------- VARIABLES/CONSTANTS DEFINITION --------------
# ------------------------------------------------------------
## General constants
RADIUS = 0.0436652320047  # radius of a circular microphone array in meters
M = 8  # number of microphones in circular microphone array
C = 343  # speed of sound in meters/seconds
SAMPLING_FREQUENCY = 48000  # sampling frequency of microphones in Hz
BUFFER_SIZE = 4800
CHANNELS_IN = 16
ACTIVE_CHANNELS_IDS = np.array([0, 1, 2, 3, 8, 9, 10, 11]) # Ids of active channels in this current FPGA configuration

## DSB constants
NUMBER_OF_TRIED_PHI = 32 # number of horizontal directions that it will try while searching for strongest signal
NUMBER_OF_TRIED_THETA = 1 # number of vertical directions that it will try while searching for strongest signal
PHI_ANGLES = np.linspace(0, 360, NUMBER_OF_TRIED_PHI, endpoint=False)
THETA_ANGLES = np.linspace(90, 20, NUMBER_OF_TRIED_THETA) 
ALL_DSB_DELAYS = cf.calculate_dsb_delays(RADIUS, M, SAMPLING_FREQUENCY, NUMBER_OF_TRIED_THETA, NUMBER_OF_TRIED_PHI, C)
# This stores index of delays relative to ALL_DSB_DELAYS array. They correspond to (theta, phi) angles.
# Initial state is set to 0, 0, propper source will be calculated with first run of source_finder function
max_rms_index = (0, 0) 

## Cardioid constants
MICROPHONE_PAIRS = [[8, 10], [0, 2], [9, 11], [1, 3], [10, 8], [2, 0], [11, 9], [3, 1]] # IDs of microphones facing in front of each other

ODVESNA = np.sqrt(5**2 + 5**2 -2*(5**2)*np.cos(np.pi/4))
MIDDLE_RADIUS = np.sqrt(5**2 - (ODVESNA/2)**2)/100
# print(MIDDLE_RADIUS)

MIDDLE_MIDDLE_RADIUS = np.sqrt((ODVESNA/4)**2 + 5**2 - 2*((ODVESNA/4)*5)*np.cos(np.radians(67.5)))/100
# print(MIDDLE_MIDDLE_RADIUS)

SOUND_DELAY = (MIDDLE_MIDDLE_RADIUS*2)/C
DELAY_IN_SAMPLES = round(SOUND_DELAY*SAMPLING_FREQUENCY)
# print(DELAY_IN_SAMPLES)

DELAYS_SAMPLES = np.array([14, 13, 13, 13]) #TODO: Nějaký hezčí výpočet
print(DELAYS_SAMPLES)

## Previous buffers definition
previous_buffer = np.zeros((BUFFER_SIZE, CHANNELS_IN)) # normal previous buffer
previous_buffer_noDC = np.zeros((BUFFER_SIZE, CHANNELS_IN)) # No DC Offset
previous_buffer_LP = np.zeros((BUFFER_SIZE, CHANNELS_IN)) # applied lowpass filter
previous_buffer_HP = np.zeros((BUFFER_SIZE, CHANNELS_IN)) # applied highpass filter

## Source finder - variables
direction_found_event = multiprocessing.Event()
direction_found_event.set() # event is set when new source direction calculations are needed
REFRESH_TIME = 7000 # Minimal time intervals in which the calculation should be made. Given in miliseconds
last_refresh = time.time() 
first_run = True

## Filtration constants
CUTOFF_FREQ = 1000
BUTTER_ORDER = 6
SOS_LOW = butter(BUTTER_ORDER, CUTOFF_FREQ, fs=SAMPLING_FREQUENCY, btype='low', output='sos')
SOS_HIGH = butter(BUTTER_ORDER, CUTOFF_FREQ, fs=SAMPLING_FREQUENCY, btype='high', output='sos')

## Recording files (for later analysis/debugging only)
recording_normal = np.empty((BUFFER_SIZE))
recording_dsb = np.empty((BUFFER_SIZE))
output_directions = []

# ----------------------------------------------------------------
# ------------------------- CALCULATIONS -------------------------
# ----------------------------------------------------------------

def butter_filter(cutoff: int, fs:int, type, order=6):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    sos = butter(order, normal_cutoff, btype=type, analog=False, output='sos')
    return sos

def find_loudest_source_dsb_card_advanced(queue: multiprocessing.Queue, event: multiprocessing.Event, indata: np.array, number_of_tried_theta: int, number_of_tried_phi: int) -> np.array:
    """TODO

    :param queue: _description_
    :type queue: multiprocessing.Queue
    :param event: _description_
    :type event: multiprocessing.Event
    :param indata: _description_
    :type indata: np.array
    :param previous_buffer: _description_
    :type previous_buffer: np.array
    :param number_of_tried_theta: _description_
    :type number_of_tried_theta: int
    :param number_of_tried_phi: _description_
    :type number_of_tried_phi: int
    :return: _description_
    :rtype: np.array
    """
    os.nice(50)
    start_time = time.time()
    global previous_buffer_LP
    global previous_buffer_HP
    rms_values = np.empty((number_of_tried_theta, number_of_tried_phi))

    ## Filtration
    # Remove DC offset
    dc_offsets_in = np.mean(indata, axis=0)
    indata_noDC = indata - dc_offsets_in

    # Lowpass butterworth filtration
    lp_buffer = np.empty(indata_noDC.shape)
    for channel in ACTIVE_CHANNELS_IDS:
        lp_buffer[:, channel] = sosfilt(SOS_LOW, indata_noDC[:, channel], axis=0)

    # Highpass butterworth filtration
    hp_buffer = np.empty(indata_noDC.shape)
    for channel in ACTIVE_CHANNELS_IDS:
        hp_buffer[:, channel] = sosfilt(SOS_HIGH, indata_noDC[:, channel], axis=0)

    # Here the changes come. Nechám si mikrofonní páry a pro každý z nich budu dělat větší aproximaci.
    COUNT_OF_CARD_APROX = int(number_of_tried_phi/8)

    ## Cardioid and DSB lowpass source finder
    lp_rms = np.empty(number_of_tried_phi)
    for pair_index in range(0, len(MICROPHONE_PAIRS)):
        # Cardioid calculation
        # Counting delays for 2 neighbouring microphones
        leading_curr_buffer = lp_buffer[:, MICROPHONE_PAIRS[pair_index][0]]
        leading_next_buffer = lp_buffer[:, MICROPHONE_PAIRS[(pair_index+1)%8][0]] 
        for aprox_coeff in range(0, COUNT_OF_CARD_APROX):
            delayed_curr_buffer = cf.delay_signal_by_frames(DELAYS_SAMPLES[aprox_coeff], lp_buffer[:, MICROPHONE_PAIRS[pair_index][1]], previous_buffer_LP[:, MICROPHONE_PAIRS[pair_index][1]])
            delayed_next_buffer = cf.delay_signal_by_frames(DELAYS_SAMPLES[aprox_coeff], lp_buffer[:, MICROPHONE_PAIRS[(pair_index+1)%8][1]], previous_buffer_LP[:, MICROPHONE_PAIRS[(pair_index+1)%8][1]])  
            # Adding weight to the microphone to create linear aproximation in the middle of them.
            leading_buffer = (aprox_coeff/COUNT_OF_CARD_APROX)*leading_curr_buffer + ((COUNT_OF_CARD_APROX - aprox_coeff)/COUNT_OF_CARD_APROX)*leading_next_buffer
            delayed_buffer = (aprox_coeff/COUNT_OF_CARD_APROX)*delayed_curr_buffer + ((COUNT_OF_CARD_APROX - aprox_coeff)/COUNT_OF_CARD_APROX)*delayed_next_buffer
            cardioid_combined = leading_buffer - delayed_buffer

            # DSB calculation
            dsb_buffer = cf.apply_delays_on_buffer(lp_buffer, previous_buffer_LP, 0, (pair_index*COUNT_OF_CARD_APROX)+aprox_coeff, ALL_DSB_DELAYS, M)

            # DSB and Cardioid combined
            combined = cardioid_combined * dsb_buffer

            # RMS calculation
            lp_rms[(pair_index*COUNT_OF_CARD_APROX)+aprox_coeff] = cf.calculate_rms_from_buffer(combined) # TODO

    ## DSB highpass source finder
    hp_rms = np.empty((number_of_tried_theta, number_of_tried_phi))
    for theta_index in range(0, number_of_tried_theta):
        for phi_index in range(0, number_of_tried_phi):
            added_signals = cf.apply_delays_on_buffer(hp_buffer, previous_buffer_HP, theta_index, phi_index, ALL_DSB_DELAYS, M)
            hp_rms[theta_index][phi_index] = cf.calculate_rms_from_buffer(added_signals)

    ## Calculating highest RMS value 
    rms_values[0] = np.sqrt(lp_rms**2 + hp_rms**2) # Combining calculated lowpass and highpass rms. Filters overlay around cutoff frequency has been neglected.
    max_rms_index = np.unravel_index(np.argmax(rms_values), rms_values.shape)
    queue.put(max_rms_index)

    ## Remembering old buffers
    previous_buffer_LP = lp_buffer.copy()
    previous_buffer_HP = hp_buffer.copy()
    
    end_time = time.time()
    # print((end_time - start_time)*1000)
    event.set()

def callback(queue, indata, outdata, frames, t, status):
    """ 
    This function delays signal in real time by using global variable delay_buffer to remember signals from the previous
    buffer. Both delayed and not delayed signal is then stored in global variables "recording_normal" and "recording_delayed"
    and then stored in wav files to the output_data folder.

    Delayed signal is stored in outdata and then played by sounddevice library in the device's speakers (delayed
    signal is converted to stereo for 2 user's device speakers).
    :param indata: Input data
    :param outdata: Output data
    :param frames:
    :param time:
    :param status:
    """
    # if status:
    #     print(status)
    global direction_found_event
    global previous_buffer
    global max_rms_index
    global last_refresh
    global first_run

    # Checking if calculation to find the loudest source was finished
    if (not queue.empty()):
        max_rms_index = queue.get_nowait()
        max_rms_phi = PHI_ANGLES[max_rms_index[1]]
        max_rms_theta = THETA_ANGLES[max_rms_index[0]]
        print(f"Theta: {max_rms_theta}, Phi: {max_rms_phi}") 
        output_directions.append(f"Theta: {max_rms_theta}, Phi: {max_rms_phi}") 

    # Starting a new calculations if the previous ones were calculated
    if direction_found_event.is_set() and ((time.time() - last_refresh)*1000 > REFRESH_TIME or first_run):
        if first_run:
            first_run = False
        last_refresh = time.time()
        direction_found_event.clear()
        process = multiprocessing.Process(target=find_loudest_source_dsb_card_advanced, args=(queue, direction_found_event, indata, NUMBER_OF_TRIED_THETA, NUMBER_OF_TRIED_PHI))
        process.start()
        
    # output stream
    outdata[:, 0] = cf.apply_delays_on_buffer(indata, previous_buffer, max_rms_index[0], max_rms_index[1], ALL_DSB_DELAYS, M)[:, 0] * 3  
    outdata[:, 1] = outdata[:, 0]

    # Saving data into delay buffer for next time
    previous_buffer = indata.copy()

    # Recording both added and not modified signal together for later analysis
    global recording_normal
    global recording_dsb
    recording_normal = np.concatenate((recording_normal, indata[:, 0]))
    recording_dsb = np.concatenate((recording_dsb, outdata[:, 0]))

# Callback wrapping for multiprocessing purposes
queue = multiprocessing.Queue()
wrapped_callback = partial(callback, queue)

with sd.Stream(samplerate=SAMPLING_FREQUENCY, channels=[16, 2], callback=wrapped_callback, blocksize=BUFFER_SIZE):
    sd.sleep(int(duration * 1000))
    # Saving recorded data in wav files
    # write("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/Ready_source_code/out_data/recording_normal.wav", sampling_frequency, recording_normal)
    # write("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/Ready_source_code/out_data/recording_dsb.wav", sampling_frequency, recording_dsb)
    # print("Finished")

soubor = "test_2000Hz_-3dB.txt"
with open("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/Loudest_Source_Finder/měření_studio/DSB+CARD_2000Hz/"+soubor, "w") as soubor:
    for string in output_directions:
        # Zápis každého řetězce na nový řádek
        soubor.write(string + "\n")
print("finished!")