"""
This script is meant to be the most advanced out of all.
User can select a mode in which the appoximation cardioids should be calculated:
- Approximation weighting by scalar (-scalar)
- Approximation weighting by butterworth filter (-butter)
- Approximation weighting by linear interpolation (-interpol)
"""
import multiprocessing.connection
import sys
import os
import multiprocessing
from functools import partial
import sounddevice as sd
import numpy as np
from fractions import Fraction
from scipy.io.wavfile import write 
import time
from scipy.signal import sosfilt, butter
import matplotlib.pyplot as plt

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

print(sd.query_devices())

# ------------------------------------------------------------
# ------------------- IMPORTANT VAIRABLES --------------------
# ------------------------------------------------------------
DURATION = 200  # how long a program should run in seconds
sd.default.device = 3, 1    # Selection of input and output devices (usually different for every computer)

# Cardioid+DSB presets
APPROXIMATION_MODE = "-scalar"
ONLY_CARDIOID_FOR_LOWPASS = True
FILTER_CONFIG = cf.RMSfilter.HIGHPASS_ONLY

# DSB presets
NUMBER_OF_TRIED_PHI = 64 # number of horizontal directions that it will try while searching for strongest signal
NUMBER_OF_TRIED_THETA = 1 # number of vertical directions that it will try while searching for strongest signal

# Performance slowdown - minimal time intervals to perform computations
REFRESH_TIME_SC = 0 # Source finder
REFRESH_TIME_GUI = 0 # GUI 

# GUI variables
SHOW_GUI = True

# Analysis tools
SHOW_TIME = False
SHOW_ANGLES = True
CALCULATE_AVERAGE_TIME = True
PRINT_STATUS = True
# ------------------------------------------------------------

if ONLY_CARDIOID_FOR_LOWPASS:
    BUTTER_INTERPOL_AMPLIFIER = 0.06
else:
    BUTTER_INTERPOL_AMPLIFIER = 0.04

# ------------------------------------------------------------
# -------------- VARIABLES/CONSTANTS DEFINITION --------------
# ------------------------------------------------------------
## General constants
RADIUS = 0.05  # radius of a circular microphone array in meters
M = 8  # number of microphones in circular microphone array
C = 343  # speed of sound in meters/seconds
SAMPLING_FREQUENCY = 48000  # sampling frequency of microphones in Hz
BUFFER_SIZE = 4800
CHANNELS_IN = 16
ACTIVE_CHANNELS_IDS = np.array([0, 1, 2, 3, 8, 9, 10, 11]) # Ids of active channels in this current FPGA configuration
previous_buffer = np.zeros((BUFFER_SIZE, CHANNELS_IN)) # normal previous buffer

## DSB constants
PHI_ANGLES = np.linspace(0, 360, NUMBER_OF_TRIED_PHI, endpoint=False)
THETA_ANGLES = np.linspace(90, 20, NUMBER_OF_TRIED_THETA) 
ALL_DSB_DELAYS = cf.calculate_dsb_delays(RADIUS, M, SAMPLING_FREQUENCY, NUMBER_OF_TRIED_THETA, NUMBER_OF_TRIED_PHI, C)
# This stores index of delays relative to ALL_DSB_DELAYS array. They correspond to (theta, phi) angles.
# Initial state is set to 0, 0, propper source will be calculated with first run of source_finder function
max_rms_index = (0, 0) 

## Cardioid constants
MICROPHONE_PAIRS = [[8, 10], [0, 2], [9, 11], [1, 3], [10, 8], [2, 0], [11, 9], [3, 1]] # IDs of microphones facing in front of each other
SOUND_DELAY = (RADIUS*2)/C
DIRECTIONS_BETWEEN_MICS = int(NUMBER_OF_TRIED_PHI/8)
DELAYS_IN_SAMPLES = np.zeros(DIRECTIONS_BETWEEN_MICS, dtype=int)

## Source finder - variables
direction_found_event = multiprocessing.Event()
direction_found_event.set() # event is set when new source direction calculations are needed
last_refresh = time.time() 
first_run = True

## Filtration constants
CUTOFF_FREQ = 1000
BUTTER_ORDER = 6
SOS_LOW = butter(BUTTER_ORDER, CUTOFF_FREQ, fs=SAMPLING_FREQUENCY, btype='low', output='sos')
SOS_HIGH = butter(BUTTER_ORDER, CUTOFF_FREQ, fs=SAMPLING_FREQUENCY, btype='high', output='sos')

## Recording files (for later analysis/debugging only)
recording_normal = np.empty((BUFFER_SIZE))
recording_dsb = np.empty((BUFFER_SIZE))
if CALCULATE_AVERAGE_TIME:
    cycles_computation_times = []

# ----------------------------------------------------------------
# ------------------------- CALCULATIONS -------------------------
# ----------------------------------------------------------------
# Calculation of delays for approximated cardioids
if APPROXIMATION_MODE == "-scalar":
    tf_cutoff = 950
    SCALAR_DELAYS = {}
    SCALAR_DELAYS[Fraction(0, DIRECTIONS_BETWEEN_MICS)] = (round((RADIUS*2/C)*SAMPLING_FREQUENCY), 1.0) # physical microphone
    tf_main = cf.cardioid_tf(0, RADIUS*2, tf_cutoff) # Transfer function
    tf_main_amplitudes = np.array([np.abs(x) for x in tf_main])
    for i in range(1, int(np.floor(DIRECTIONS_BETWEEN_MICS/2)) + 1):
        distance = cf.approximation_radius(RADIUS, i, DIRECTIONS_BETWEEN_MICS) * 2
        tf_approx = cf.cardioid_tf(0, distance, tf_cutoff) # Transfer function
        tf_approx_amplitudes = np.abs(tf_approx)
        average_loudness_difference = np.sum(np.abs(tf_main_amplitudes/tf_approx_amplitudes))/tf_main_amplitudes.shape[0]
        delay_in_samples = round((distance/C)*SAMPLING_FREQUENCY)
        SCALAR_DELAYS[Fraction(i, DIRECTIONS_BETWEEN_MICS)] = (delay_in_samples, average_loudness_difference)
    # print(SCALAR_DELAYS)
elif APPROXIMATION_MODE == "-butter":
    """
    Goal of following lines of code is to create a dictionary FILTER_COEFFS_AND_AMPLIFIER, that contains 
    butterworth filter a and b coefficients together with delay in samples and amplification numerator of the 
    whole signal. As keys to this dicrionary are set the fractions representing distance of approximated 
    microphone between 2 physical microphones.

    Frequencies to the butterworth filter are set manualy for each of the fractions. In further development,
    more accurate calculations can be added.

    Amplification numerators are determined by averaging the difference between transfer function of physical
    microphones and approximated microphones among the whole spectrum of the cardioid (for lowpass filtered singal
    at 1 kHz in our context). Amplification numerator tells us, by how much do we need to amplify the signal of 
    approximated microphones in order to reach the levels of the physical microphones (e.g. 1.079 times.).
    """
    # Manualy set frequencies based of the look of the curve
    CUTOFF_FREQUENCIES = {
        Fraction(1, 2) : 2550,
        Fraction(1, 3) : 2550,
        Fraction(1, 4) : 2650,
        Fraction(1, 5) : 2750,
        Fraction(2, 5) : 2650,
        # Fraction(1, 6) : 0,
        # Fraction(1, 7) : 0,
        # Fraction(2, 7) : 0,
        # Fraction(3, 7) : 0,
        Fraction(1, 8) : 5050,
        Fraction(3, 8) : 2650
    }
    # Transfer function of the physical microphones
    tf_main_amplitudes = np.abs(cf.cardioid_tf_quantized(0, RADIUS*2, CUTOFF_FREQ))
    FILTER_COEFFS_AND_AMPLIFIER = {}
    FILTER_COEFFS_AND_AMPLIFIER[Fraction(0, DIRECTIONS_BETWEEN_MICS)] = (None, round((RADIUS*2/C)*SAMPLING_FREQUENCY), 1.0)
    for fraction, cutoff_freq in CUTOFF_FREQUENCIES.items():
        # Distance between approximated microhphones
        distance = cf.approximation_radius(RADIUS, fraction.numerator, fraction.denominator) * 2
        # Transfer function of approximated microphones
        tf_approx_amplitudes = np.abs(cf.cardioid_tf_quantized(0, distance, CUTOFF_FREQ))
        # Caclulation of ratio between physical and approximation microphones curve
        ratios_curve = np.abs(tf_main_amplitudes/tf_approx_amplitudes)
        # Determining the amplification numerator 
        amplification_numerator = ratios_curve[50] # + 0.02
        # Calculation of delay in samples
        delay_in_samples = round((distance/C)*SAMPLING_FREQUENCY)
        # Adding results to the dictionary
        FILTER_COEFFS_AND_AMPLIFIER[fraction] = (butter(1, cutoff_freq, fs=SAMPLING_FREQUENCY, btype='low', output='sos'), delay_in_samples, amplification_numerator)
    # print(FILTER_COEFFS_AND_AMPLIFIER)
elif APPROXIMATION_MODE == "-interpol":
    INTERPOLATION_DELAYS = {}
    INTERPOLATION_DELAYS[Fraction(0, DIRECTIONS_BETWEEN_MICS)] = (round((RADIUS*2/C)*SAMPLING_FREQUENCY), 0) # physical microphone
    QUANTIZATION_STEP = 1/SAMPLING_FREQUENCY
    for i in range(1, int(np.floor(DIRECTIONS_BETWEEN_MICS/2)) + 1):
        distance = cf.approximation_radius(RADIUS, i, DIRECTIONS_BETWEEN_MICS) * 2
        tau_compensated = cf.tau_compensation(RADIUS*2, distance)
        delay_in_samples = int(np.floor(tau_compensated/QUANTIZATION_STEP))
        remaining_delay_in_ms = (tau_compensated % QUANTIZATION_STEP) * 1000000
        INTERPOLATION_DELAYS[Fraction(i, DIRECTIONS_BETWEEN_MICS)] = (delay_in_samples, remaining_delay_in_ms)
    # print(INTERPOLATION_DELAYS)

def init_process():
    # Sets lower priority for the processor
    os.nice(30)    

def butter_filter(cutoff: int, fs:int, type, order=6):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    sos = butter(order, normal_cutoff, btype=type, analog=False, output='sos')
    return sos

def calculate_direction_card_dsb(args):
    buffer_LP, buffer_HP, pair_index, approx_index = args
    ## Cardioid + DSB calculation
    # Selection of mics for cardioid. It takes also the next one for approximation purposes.
    leading_buffer = buffer_LP[:, MICROPHONE_PAIRS[pair_index][1]]
    leading_next_buffer = buffer_LP[:, MICROPHONE_PAIRS[(pair_index+1)%M][1]]

    delaying_buffer = buffer_LP[:, MICROPHONE_PAIRS[pair_index][0]]
    delaying_next_buffer = buffer_LP[:, MICROPHONE_PAIRS[(pair_index+1)%M][0]] 

    # Approximating microphones based on approx_index.
    lead_buffer = ((DIRECTIONS_BETWEEN_MICS - approx_index)/DIRECTIONS_BETWEEN_MICS)*leading_buffer + (approx_index/DIRECTIONS_BETWEEN_MICS)*leading_next_buffer
    delaying_buffer = ((DIRECTIONS_BETWEEN_MICS - approx_index)/DIRECTIONS_BETWEEN_MICS)*delaying_buffer + (approx_index/DIRECTIONS_BETWEEN_MICS)*delaying_next_buffer
    # Approximation calculation
    if APPROXIMATION_MODE == "-scalar":
        if approx_index > np.floor(DIRECTIONS_BETWEEN_MICS/2):
            cardioid_sample_delay, amplification_numerator = SCALAR_DELAYS[Fraction(DIRECTIONS_BETWEEN_MICS - approx_index, DIRECTIONS_BETWEEN_MICS)]
        else:
            cardioid_sample_delay, amplification_numerator = SCALAR_DELAYS[Fraction(approx_index, DIRECTIONS_BETWEEN_MICS)]
        cardioid = (lead_buffer[:BUFFER_SIZE - cardioid_sample_delay] - delaying_buffer[cardioid_sample_delay:])
    elif APPROXIMATION_MODE == "-butter":
        if approx_index > np.floor(DIRECTIONS_BETWEEN_MICS/2):
            butter_coeffs, cardioid_sample_delay, amplification_numerator = FILTER_COEFFS_AND_AMPLIFIER[Fraction(DIRECTIONS_BETWEEN_MICS - approx_index, DIRECTIONS_BETWEEN_MICS)]
            amplification_numerator += BUTTER_INTERPOL_AMPLIFIER * ((DIRECTIONS_BETWEEN_MICS - approx_index)/DIRECTIONS_BETWEEN_MICS)
        else:
            butter_coeffs, cardioid_sample_delay, amplification_numerator = FILTER_COEFFS_AND_AMPLIFIER[Fraction(approx_index, DIRECTIONS_BETWEEN_MICS)]
            amplification_numerator += BUTTER_INTERPOL_AMPLIFIER * (approx_index/DIRECTIONS_BETWEEN_MICS)
        cardioid = (lead_buffer[:BUFFER_SIZE - cardioid_sample_delay] - delaying_buffer[cardioid_sample_delay:])
        if approx_index != 0:
            cardioid = sosfilt(butter_coeffs, cardioid, axis=0)
    elif APPROXIMATION_MODE == "-interpol":
        if approx_index > np.floor(DIRECTIONS_BETWEEN_MICS/2):
            cardioid_sample_delay, ms_delay = INTERPOLATION_DELAYS[Fraction(DIRECTIONS_BETWEEN_MICS - approx_index, DIRECTIONS_BETWEEN_MICS)]
            amplification_numerator = BUTTER_INTERPOL_AMPLIFIER * ((DIRECTIONS_BETWEEN_MICS - approx_index)/DIRECTIONS_BETWEEN_MICS) + 1.0
        else:
            cardioid_sample_delay, ms_delay = INTERPOLATION_DELAYS[Fraction(approx_index, DIRECTIONS_BETWEEN_MICS)]
            amplification_numerator = BUTTER_INTERPOL_AMPLIFIER * (approx_index/DIRECTIONS_BETWEEN_MICS) + 1.0
        delayed_buffer = cf.delay_signal_interpolation(delaying_buffer, ms_delay, SAMPLING_FREQUENCY)
        cardioid = (lead_buffer[:BUFFER_SIZE - cardioid_sample_delay] - delayed_buffer[cardioid_sample_delay:])
        
    # DSB and Cardioid combined. User can specify if DSB should be calculated on low frequencies for optimalization purposes.
    if not ONLY_CARDIOID_FOR_LOWPASS:
        # DSB low calculation
        dsb_low, dsb_max_delay = cf.cut_dsb_delays_on_buffer(buffer_LP, 0, (DIRECTIONS_BETWEEN_MICS*pair_index)+approx_index, ALL_DSB_DELAYS, M)
        # Shapes of cardioid and dsb buffers must be equal.
        if cardioid.shape > dsb_low.shape:
            # Cardioid shape is bigger, therefore it has to be cut to the shape of dsb
            cardioid = cardioid[dsb_max_delay - cardioid_sample_delay:]
        elif cardioid.shape < dsb_low.shape:
            # Dsb shape is bigger, therefore it has to be cut to the shape of dsb
            dsb_low = dsb_low[cardioid_sample_delay - dsb_max_delay:]
        card_dsb_combined = cardioid * dsb_low
        output_low_rms = cf.calculate_rms_from_buffer(card_dsb_combined) * amplification_numerator
    else:
        # Amplifying output rms for optimalization purpouses.
        output_low_rms = cf.calculate_rms_from_buffer(cardioid) * amplification_numerator

    ## DSB high Calculation
    output_high, max_delay = cf.cut_dsb_delays_on_buffer(buffer_HP, 0, (DIRECTIONS_BETWEEN_MICS*pair_index)+approx_index, ALL_DSB_DELAYS, M)

    ## RMS calculation
    return pair_index, approx_index, output_low_rms, cf.calculate_rms_from_buffer(output_high)

def find_loudest_source_dsb_card_basic_8dir(max_rms_indices_queue: multiprocessing.Queue, rms_values_queue: multiprocessing.Queue, cvar: multiprocessing.Condition, 
                                            indata: np.ndarray, number_of_tried_theta: int, number_of_tried_phi: int) -> np.ndarray:
    """TODO

    :param queue: _description_
    :type queue: multiprocessing.Queue
    :param event: _description_
    :type event: multiprocessing.Event
    :param indata: _description_
    :type indata: np.array
    :param previous_buffer: _description_
    :type previous_buffer: np.array
    :param number_of_tried_theta: _description_
    :type number_of_tried_theta: int
    :param number_of_tried_phi: _description_
    :type number_of_tried_phi: int
    :return: _description_
    :rtype: np.array
    """
    os.nice(30) # Give lower priority on the processor to this function
    start_time = time.time()
    rms_values = np.empty((number_of_tried_theta, number_of_tried_phi))

    ## Filtration
    # Remove DC offset
    dc_offsets_in = np.mean(indata, axis=0)
    indata_noDC = indata - dc_offsets_in

    # Lowpass butterworth filtration
    buffer_LP = np.empty(indata_noDC.shape)
    for channel in ACTIVE_CHANNELS_IDS:
        buffer_LP[:, channel] = sosfilt(SOS_LOW, indata_noDC[:, channel], axis=0)

    # Highpass butterworth filtration
    buffer_HP = np.empty(indata_noDC.shape)
    for channel in ACTIVE_CHANNELS_IDS:
        buffer_HP[:, channel] = sosfilt(SOS_HIGH, indata_noDC[:, channel], axis=0)

    ## Calculation of direction
    # Initialization of threads
    args = [(buffer_LP, buffer_HP, pair_index, approx_index) 
            for pair_index in range(0, len(MICROPHONE_PAIRS))
            for approx_index in range(0, DIRECTIONS_BETWEEN_MICS)]
    with multiprocessing.Pool(initializer=init_process, processes=multiprocessing.cpu_count()) as pool:
        results = pool.map(calculate_direction_card_dsb, args)
    # Reading the results
    for result in results:
        pair_index, approx_index, rms_LP, rms_HP = result
        if FILTER_CONFIG == cf.RMSfilter.BOTH:
            rms_values[0][DIRECTIONS_BETWEEN_MICS*pair_index+approx_index] = np.sqrt(np.square(rms_LP) + np.square(rms_HP))
        elif FILTER_CONFIG == cf.RMSfilter.LOWPASS_ONLY:
            rms_values[0][DIRECTIONS_BETWEEN_MICS*pair_index+approx_index] = rms_LP
        elif FILTER_CONFIG == cf.RMSfilter.HIGHPASS_ONLY:
            rms_values[0][DIRECTIONS_BETWEEN_MICS*pair_index+approx_index] = rms_HP

    ## Calculating highest RMS value 
    max_rms_index = np.unravel_index(np.argmax(rms_values), rms_values.shape)
    end_time = time.time()
    max_rms_indices_queue.put((max_rms_index, (end_time - start_time)*1000))
    if rms_values_queue.empty():
        max_rms_phi = PHI_ANGLES[max_rms_index[1]]
        rms_values_queue.put(rms_values)
    if SHOW_TIME:
        print((end_time - start_time)*1000)

def gui_thread(rms_values_queue: np.ndarray, cvar: multiprocessing.Condition, duration: int):
    start_time = time.time()
    # Initializing phi_radians with 2*pi at the end to close the polar diagram
    phi_radians = np.linspace(0, 2*np.pi, NUMBER_OF_TRIED_PHI, endpoint=False)
    phi_radians = np.append(phi_radians, np.array([2*np.pi]))

    # Set interactive mode
    plt.ion() 
    # Set plot
    fig = plt.figure()
    ax = fig.add_subplot(111, polar=True)
    ax.set_ylim([-30, 0])
    ax.set_theta_direction(-1)
    arrow = ax.annotate('', xy=(phi_radians[0], -10), 
                        xytext=(phi_radians[0], -30), 
                        arrowprops=dict(color='red', linewidth=2, arrowstyle='->'))
    
    line, = ax.plot(phi_radians, np.full(NUMBER_OF_TRIED_PHI + 1, -10), linestyle='-', color='b')
    plt.draw()
    plt.pause(0.1)

    while time.time() - start_time < duration:
        with cvar:
            if not rms_values_queue.empty():
                rms_values = rms_values_queue.get_nowait()[0]
                rms_decibels = cf.signal_to_decibels(rms_values)
                rms_decibels_polar = np.append(rms_decibels, rms_decibels[0]) # Adding first element at the end to close the polar plot
                upper_limit = np.max(rms_decibels)
                upper_limit_index = np.argmax(rms_decibels)
                bottom_limit = np.min(rms_decibels)
                line.set_ydata(rms_decibels_polar)
                ax.set_ylim([bottom_limit, upper_limit])
                arrow.remove()
                arrow = ax.annotate('', xy=(phi_radians[upper_limit_index], upper_limit), 
                        xytext=(phi_radians[upper_limit_index], bottom_limit), 
                        arrowprops=dict(color='red', linewidth=2, arrowstyle='->'))
                plt.draw()
                plt.pause(0.1)
            else:
                # Waiting for the calculation of next portion of data...
                # cvar.wait()
                pass

def callback(max_rms_indices_queue, rms_values_queue, cvar, indata, outdata, frames, t, status):
    """ 
    This function delays signal in real time by using global variable delay_buffer to remember signals from the previous
    buffer. Both delayed and not delayed signal is then stored in global variables "recording_normal" and "recording_delayed"
    and then stored in wav files to the output_data folder.

    Delayed signal is stored in outdata and then played by sounddevice library in the device's speakers (delayed
    signal is converted to stereo for 2 user's device speakers).
    :param indata: Input data
    :param outdata: Output data
    :param frames:
    :param time:
    :param status:
    """
    if status and PRINT_STATUS:
        print(status)
    global previous_buffer
    global max_rms_index
    global last_refresh
    global first_run

    # Checking if calculation to find the loudest source was finished
    if first_run or (not max_rms_indices_queue.empty()):
        if not first_run:
            loudest_source_finder_output = max_rms_indices_queue.get_nowait()
            max_rms_index = loudest_source_finder_output[0]
            max_rms_phi = PHI_ANGLES[max_rms_index[1]]
            max_rms_theta = THETA_ANGLES[max_rms_index[0]]
            if SHOW_ANGLES:
                print(f"Theta: {max_rms_theta}, Phi: {max_rms_phi}")
            if CALCULATE_AVERAGE_TIME:
                cycles_computation_times.append(loudest_source_finder_output[1])   
        process = multiprocessing.Process(target=find_loudest_source_dsb_card_basic_8dir, args=(max_rms_indices_queue, rms_values_queue, cvar, indata, NUMBER_OF_TRIED_THETA, NUMBER_OF_TRIED_PHI))
        process.start()
        first_run = False
        
    # output stream
    outdata[:, 0] = cf.apply_dsb_delays_on_buffer(indata, previous_buffer, max_rms_index[0], max_rms_index[1], ALL_DSB_DELAYS, M) * 3
    outdata[:, 1] = outdata[:, 0]

    # Saving data into delay buffer for next time
    previous_buffer = indata.copy()

    # Recording both added and not modified signal together for later analysis
    global recording_normal
    global recording_dsb
    recording_normal = np.concatenate((recording_normal, indata[:, 0]))
    recording_dsb = np.concatenate((recording_dsb, outdata[:, 0]))

if __name__ == "__main__":
    # Queues for sharing calculated RMS values among the main, computation and GUI thread
    max_rms_indices_queue = multiprocessing.Queue()
    rms_values_queue = multiprocessing.Queue()

    # Condition variable to notify GUI thread about finished calculation
    lock = multiprocessing.Lock()
    cvar = multiprocessing.Condition(lock)

    # GUI thread initialization
    if SHOW_GUI:
        gui_process = multiprocessing.Process(target=gui_thread, args=(rms_values_queue, cvar, DURATION))
        gui_process.start()

    # Wrapping callback with
    wrapped_callback = partial(callback, max_rms_indices_queue, rms_values_queue, cvar)

    with sd.Stream(samplerate=SAMPLING_FREQUENCY, channels=[16, 2], callback=wrapped_callback, blocksize=BUFFER_SIZE):
        sd.sleep(int(DURATION * 1000))
        if CALCULATE_AVERAGE_TIME:
            print(f"Average: {sum(cycles_computation_times) / len(cycles_computation_times)}")
    
    if SHOW_GUI:
        gui_process.join()
    print("Finished")
    