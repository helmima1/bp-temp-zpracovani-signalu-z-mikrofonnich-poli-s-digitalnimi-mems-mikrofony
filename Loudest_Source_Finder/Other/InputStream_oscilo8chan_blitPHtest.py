import sounddevice as sd
import numpy as np
import matplotlib.pyplot as plt

print(sd.query_devices())

duration = 3600  # seconds
sd.default.device = 3

fs = 48000
frms = 4800
time = np.linspace(0,frms/fs, frms)
ini = np.zeros(frms)
fig = plt.figure()
ax = fig.add_subplot(111)
line1, = ax.plot(time, ini, animated=True)
line2, = ax.plot(time, ini, animated=True)
line3, = ax.plot(time, ini, animated=True)
line4, = ax.plot(time, ini, animated=True)
line5, = ax.plot(time, ini, animated=True)
line6, = ax.plot(time, ini, animated=True)
line7, = ax.plot(time, ini, animated=True)
line8, = ax.plot(time, ini, animated=True)
ax.set_ylim(-1,1)
plt.show(block=False)
plt.pause(0.1)
bg = fig.canvas.copy_from_bbox(fig.bbox)
ax.draw_artist(line1)
ax.draw_artist(line2)
ax.draw_artist(line3)
ax.draw_artist(line4)
ax.draw_artist(line5)
ax.draw_artist(line6)
ax.draw_artist(line7)
ax.draw_artist(line8)
fig.canvas.blit(fig.bbox)

def callback(indata, frames, time, status):
    if status:
        print(status)
    DataToPlot = indata - np.mean(indata, axis=0)
    fig.canvas.restore_region(bg)
    line1.set_ydata(DataToPlot[:,0])
    line2.set_ydata(DataToPlot[:,1])
    line3.set_ydata(DataToPlot[:,2])
    line4.set_ydata(DataToPlot[:,3])
    line5.set_ydata(DataToPlot[:,4])
    line6.set_ydata(DataToPlot[:,5])
    line7.set_ydata(DataToPlot[:,6])
    line8.set_ydata(DataToPlot[:,7])
    ax.draw_artist(line1)
    ax.draw_artist(line2)
    ax.draw_artist(line3)
    ax.draw_artist(line4)
    ax.draw_artist(line5)
    ax.draw_artist(line6)
    ax.draw_artist(line7)
    ax.draw_artist(line8)
    fig.canvas.blit(fig.bbox)
    fig.canvas.flush_events()

with sd.InputStream(samplerate=fs, channels=2, callback=callback, blocksize=frms):
    sd.sleep(int(duration * 1000))