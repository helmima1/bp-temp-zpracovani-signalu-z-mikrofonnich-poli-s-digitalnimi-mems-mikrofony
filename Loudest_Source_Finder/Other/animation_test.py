import matplotlib.pyplot as plt
import numpy as np

def get_new_data():
    # Simulujeme příjem nového snímku dat
    theta = np.random.rand() * 2 * np.pi
    r = np.random.rand()
    return theta, r

fig = plt.figure()
ax = fig.add_subplot(111, polar=True)

# Inicializujeme graf s nějakými daty
theta, r = get_new_data()
line, = ax.plot(theta, r, 'bo')  # 'bo' pro modrý bod

plt.ion()  # Zapne interaktivní mód
plt.show()

for _ in range(10):  # Pro demonstraci provedeme 10 aktualizací
    theta, r = get_new_data()
    line.set_data(theta, r)
    fig.canvas.draw_idle()  # Použijeme draw_idle pro aktualizaci grafu
    plt.pause(0.5)  # Pauza mezi aktualizacemi

plt.ioff()  # Vypneme interaktivní mód po dokončení
