"""
Poznámky:
1. Projedu všechny směry s a na každý z nich aplikuji odpovídající zpoždění.
2. Pro každý směr vypočítám RMS hodnotu (RMS budu počítat na velikost bufferu, což odpovídá 100 ms)
3. Všechny RMS zapíšu do pole
4. Promínu tyto hodnoty do polar plotu
5. Podle toho, kde je hodnota nejsilnější, zaměřím hlavní lalok
"""
import sys
import os
import sounddevice as sd
import numpy as np
from scipy.io.wavfile import write 
import matplotlib.pyplot as plt
import multiprocessing
from multiprocessing.queues import Empty
from functools import partial
import time

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

print(sd.query_devices())
duration = 100  # determines how long a program should run in seconds
sd.default.device = 3, 4  # Selection of input and output devices (usually different for every computer)

# ------------------------------------------------------------
# ------------------- VARIABLES DEFINITION -------------------
# ------------------------------------------------------------
radius = 0.05  # radius of a circular microphone array in meters
M = 8  # number of microphones in circular microphone array
# theta = 90 # horizontal steering angle of beamforming algorithm
c = 343  # speed of sound in meters/seconds
sampling_frequency = 48000  # sampling frequency of microphones in Hz
buffer_size = 4800
channels_in = 16
number_of_tried_phi = 8 # number of horizontal directions that it will try while searching for strongest signal
number_of_aprox_phi = 2 #TODO: Dodělat i pro thetu
number_of_tried_theta = 1 # number of vertical directions that it will try while searching for strongest signal

# ----------------------------------------------------------------
# ------------------------- CALCULATIONS -------------------------
# ----------------------------------------------------------------

## Buffer definition
history = None
previous_buffer = np.zeros((buffer_size, channels_in))

## Recording files
recording_normal = np.empty((0, channels_in))
recording_dsb = np.empty((0, 1))

## Source finder - number of angles
phi_angles = np.linspace(0, 360, number_of_tried_phi*number_of_aprox_phi, endpoint=False)
phi_radians = np.linspace(0, 2*np.pi, number_of_tried_phi*number_of_aprox_phi, endpoint=False)
#TODO: Vytvořit nějakou hezčí logiku, ať to nepočítá 0 a 90 stupňů..
theta_angles = np.linspace(90, 20, number_of_tried_theta) 

## Source finder - calculations of delays
# all_latencies is a 2 dimensional array storing latencies for 
all_latencies = np.zeros((number_of_tried_theta, number_of_tried_phi*number_of_aprox_phi, 8), dtype=int)
for theta_index in range(0, number_of_tried_theta):
    for phi_index in range(0, number_of_tried_phi*number_of_aprox_phi):
        # Calculate delays for current direction
        delays = cf.cma_tmi(radius, M, phi_angles[phi_index], theta_angles[theta_index], c)
        all_latencies[theta_index][phi_index] = np.array(cf.quantize_tmi_to_samples(delays, sampling_frequency), dtype=int)

## Source finder - multithreading
zpracovani_event = multiprocessing.Event()
zpracovani_event.set()
max_rms_index = (0, 0)

## Functions
def calculate_rms_from_buffer(audio_buffer: np.array):
    """
    Calculates RMS value from a buffer 
    """
    rms_hodnota = np.sqrt(np.mean(audio_buffer**2))
    
    # Převede RMS hodnotu na decibely; referenční hodnota 1 pro digitální audio signály
    # Používáme 20 * log10(RMS/1) = 20 * log10(RMS)
    # Přidáváme malou hodnotu k RMS hodnotě, aby se předešlo logaritmu z nuly
    rms_v_db = 20 * np.log10(rms_hodnota + 1e-20)
    
    return rms_hodnota

def delay_signal_by_frames(delays: int, input_buffer: np.array, delay_buffer: np.array) -> np.array:
    """
    This function takes in previous buffer and uses it to delay data in the current "input_buffer" buffer. It re
    Signal cannot be delayed by more samples than the size of the buffer.
    :param delays: list of delays, where index of an array matches channel number
    :param input_buffer: current buffer
    :param delay_buffer: previous buffer
    :return:
    """
    if delays == 0:
        return input_buffer
    history = delay_buffer[-delays:]
    delayed_signal = np.concatenate((history, input_buffer))
    delayed_signal = delayed_signal[:-delays]
    return delayed_signal

def apply_delays_on_buffer(indata, previous_buffer, theta_index, phi_index, gain = 1):
    delayed_ch1 = delay_signal_by_frames(all_latencies[theta_index][phi_index][4], indata[:, 8], previous_buffer[:, 8])
    delayed_ch2 = delay_signal_by_frames(all_latencies[theta_index][phi_index][5], indata[:, 0], previous_buffer[:, 0])
    delayed_ch3 = delay_signal_by_frames(all_latencies[theta_index][phi_index][6], indata[:, 9], previous_buffer[:, 9])
    delayed_ch4 = delay_signal_by_frames(all_latencies[theta_index][phi_index][7], indata[:, 1], previous_buffer[:, 1])
    delayed_ch5 = delay_signal_by_frames(all_latencies[theta_index][phi_index][0], indata[:, 10], previous_buffer[:, 10])
    delayed_ch6 = delay_signal_by_frames(all_latencies[theta_index][phi_index][1], indata[:, 2], previous_buffer[:, 2])
    delayed_ch7 = delay_signal_by_frames(all_latencies[theta_index][phi_index][2], indata[:, 11], previous_buffer[:, 11])
    delayed_ch8 = delay_signal_by_frames(all_latencies[theta_index][phi_index][3], indata[:, 3], previous_buffer[:, 3])
    added_signals = np.empty((4800, 1))
    # Add delayed signals together
    for i in range(0, len(delayed_ch1)):
        added_signals[i] = (delayed_ch1[i] + 
                            delayed_ch2[i] + 
                            delayed_ch3[i] + 
                            delayed_ch4[i] + 
                            delayed_ch5[i] + 
                            delayed_ch6[i] + 
                            delayed_ch7[i] + 
                            delayed_ch8[i]) / M
        added_signals[i] *= gain
    return added_signals

# fig = plt.figure()
# ax = fig.add_subplot(111, polar=True)

# # Inicializujte graf s počátečními daty
# line, = ax.plot(phi_radians, np.full(number_of_tried_phi, 0.054))  # 'bo' pro modrý bod
# ax.set_ylim([0.053, 0.059])
# plt.ion()  # Zapne interaktivní mód
# plt.show()  

def calculate_direction(args):
    """
    Calculates rms for direction specified in theta_index and phi_index. Those indices correspond to all_latencies
    variable, where they correspond to a certain angles. This is to speed up the system, so new latencies are not
    calculated every time they are being used.

    Purpose of this function is to speed up the algorithm, each of those functions are ment to be calculated in
    different thread.
    """
    indata, previous_buffer, theta_index, phi_index = args
    return calculate_rms_from_buffer(apply_delays_on_buffer(indata, previous_buffer, theta_index, phi_index)), theta_index, int(phi_index/number_of_aprox_phi)

def calculate_direction_aprox(args):
    """
    Calculates rms for direction specified in theta_index and phi_index. Those indices correspond to all_latencies
    variable, where they correspond to a certain angles. This is to speed up the system, so new latencies are not
    calculated every time they are being used.

    Purpose of this function is to speed up the algorithm, each of those functions are ment to be calculated in
    different thread.
    """
    indata, previous_buffer, theta_index, phi_index = args
    return calculate_rms_from_buffer(apply_delays_on_buffer(indata, previous_buffer, theta_index, phi_index)), theta_index, phi_index % number_of_aprox_phi

def init_process():
    # Nastaví nižší prioritu pro tento proces
    os.nice(20)

def find_aprox_indices(rms_max_index, rms_second_max_index):
    """
    This function calculates inicies of microphones that are to be approximated. 
    It returns list of microphone indices that are to be approximated + id identyfying scenario case that happened:
    
    0 - max is 0 and second_max is last index (první je second max)
    1 - second_max is 0 and max is last index (první je max)
    2 - max is smaller than second_max (první je max)
    3 - second_max is smaller than max (první je second max)
    """
    if rms_max_index[1] == 0 and rms_second_max_index[1] == number_of_tried_phi - 1:
        return ([i for i in range((number_of_tried_phi - 1)*number_of_aprox_phi + 1, (number_of_tried_phi)*number_of_aprox_phi)], 0)
    elif rms_max_index[1] == number_of_tried_phi - 1 and rms_second_max_index[1] == 0:
        return ([i for i in range((number_of_tried_phi - 1)*number_of_aprox_phi + 1, (number_of_tried_phi)*number_of_aprox_phi)], 1)
    elif rms_max_index[1] < rms_second_max_index[1]:
        return ([i for i in range(rms_max_index[1]*number_of_aprox_phi+1, rms_second_max_index[1]*number_of_aprox_phi)], 2)
    elif rms_max_index[1] > rms_second_max_index[1]:
        return ([i for i in range(rms_second_max_index[1]*number_of_aprox_phi+1, rms_max_index[1]*number_of_aprox_phi)], 3)

def find_loudest_source(queue, event, indata, previous_buffer, number_of_tried_theta: int, number_of_tried_phi: int):
    """
    Iterates through given amount of directions and finds the loudest one. Number of directions is defined for
    number of thetas and phis specified in variables number_of_tried_theta and number_of_tried_phi.
    """
    start_time = time.time()
    rms_values = np.empty((number_of_tried_theta, number_of_tried_phi))

    # Multithreading for every tried direction
    args = [(indata, previous_buffer, theta_index, phi_index) 
        for theta_index in range(number_of_tried_theta) 
        for phi_index in range(0, number_of_tried_phi*number_of_aprox_phi, number_of_aprox_phi)]

    with multiprocessing.Pool(initializer=init_process, processes=multiprocessing.cpu_count()) as pool:
        results = pool.map(calculate_direction, args)

    for result in results:
        rms_value, theta_index, phi_index = result
        rms_values[theta_index][phi_index] = rms_value

    # Calculating highest RMS value 
    # max_rms_index stores indices of the heighest RMS value relative to rms_values array
    max_rms_index = np.unravel_index(np.argmax(rms_values), rms_values.shape)
    # Find out which side mic is stronger
    if rms_values[max_rms_index[0]][(max_rms_index[1]-1)%number_of_tried_phi] > rms_values[max_rms_index[0]][(max_rms_index[1]+1)%number_of_tried_phi]:
        second_max_rms_index = (max_rms_index[0], (max_rms_index[1]-1)%number_of_tried_phi)
    else:
        second_max_rms_index = (max_rms_index[0], (max_rms_index[1]+1)%number_of_tried_phi)
    
    # problém je v tom, max a second max indexy jsou divné, pak nedokážu dát dohromady directions mezi nimi
    # print(f"max: {max_rms_index[1]} second max:  {second_max_rms_index[1]}")

    # Approximate phi
    # Počítám vlastně jenom 3 hodnoty + max a second max. 
    # Do rms_values_aprox pak započítávám i ty hraniční, tudíž tam mám 5 hodnot.
    aprox_indices = find_aprox_indices(max_rms_index, second_max_rms_index)
    rms_values_aprox = np.empty((number_of_tried_theta, number_of_aprox_phi + 1))
    # print(aprox_indices)

    if aprox_indices[1] == 0 or aprox_indices[1] == 3:
        rms_values_aprox[0][0] = rms_values[0][second_max_rms_index[1]]
        rms_values_aprox[0][number_of_aprox_phi] = rms_values[0][max_rms_index[1]]
    elif aprox_indices[1] == 1 or aprox_indices[1] == 2:
        rms_values_aprox[0][0] = rms_values[0][max_rms_index[1]]
        rms_values_aprox[0][number_of_aprox_phi] = rms_values[0][second_max_rms_index[1]]

    args = [(indata, previous_buffer, theta_index, phi_index) 
        for theta_index in range(number_of_tried_theta) 
        for phi_index in aprox_indices[0]]

    with multiprocessing.Pool(initializer=init_process, processes=multiprocessing.cpu_count()) as pool:
        # co když to tady nejsem schopný správně namapovat?
        results = pool.map(calculate_direction_aprox, args)

    # print(results)
    for result in results:
        rms_value, theta_index, phi_index = result
        rms_values_aprox[theta_index][phi_index] = rms_value

    # Teď mám v rms_values_aprox mít 5 hodnot, které odpovídají těm aproximovaným rms hodnotám
    # print(rms_values_aprox)

    # Teď potřebuji přijít na nejvyšší hodnotu z rms_values_aprox a spárovat jí s aprox_indices
    if aprox_indices[1] == 0 or aprox_indices[1] == 3:
        # prvni je second max
        aprox_indices = [[second_max_rms_index[1]*number_of_aprox_phi] + aprox_indices[0] + [max_rms_index[1]*number_of_aprox_phi]]
    elif aprox_indices[1] == 1 or aprox_indices[1] == 2:
        aprox_indices = [[max_rms_index[1]*number_of_aprox_phi] + aprox_indices[0] + [second_max_rms_index[1]*number_of_aprox_phi]]
    
    # print(aprox_indices)

    max_aprox_rms_value = np.unravel_index(np.argmax(rms_values_aprox), rms_values_aprox.shape)
    max_aprox_rms_index = (0, aprox_indices[max_aprox_rms_value[0]][max_aprox_rms_value[1]]) #TODO: theta

    # print(max_aprox_rms_index)

    queue.put(max_aprox_rms_index)
    end_time = time.time()
    # print((end_time - start_time)*1000)
    event.set()

def callback(queue, indata, outdata, frames, time, status):
    """ 
    This function delays signal in real time by using global variable delay_buffer to remember signals from the previous
    buffer. Both delayed and not delayed signal is then stored in global variables "recording_normal" and "recording_delayed"
    and then stored in wav files to the output_data folder.

    Delayed signal is stored in outdata and then played by sounddevice library in the device's speakers (delayed
    signal is converted to stereo for 2 user's device speakers).
    :param indata: Input data
    :param outdata: Output data
    :param frames:
    :param time:
    :param status:
    """
    if status:
        print(status)
    global zpracovani_event
    global previous_buffer
    global max_rms_index
    try:
        # Zkoušíme přečíst z fronty, ale neblokujeme, pokud fronta je prázdná
        max_rms_index = queue.get_nowait()
        # print(max_rms_index[1]*number_of_aprox_phi)
        max_rms_phi = phi_angles[max_rms_index[1]]
        max_rms_theta = theta_angles[max_rms_index[0]]
        print(f"Theta: {max_rms_theta}, Phi: {max_rms_phi}")
    except Empty:
        # Žádná data k dispozici
        pass    

    if status:
        print(status)
    if zpracovani_event.is_set():
        zpracovani_event.clear()  # Resetování události pro další zpracování
        process = multiprocessing.Process(target=find_loudest_source, args=(queue, zpracovani_event, indata, previous_buffer, number_of_tried_theta, number_of_tried_phi))
        process.start()

    # output stream
    outdata[:, 0] = apply_delays_on_buffer(indata, previous_buffer, max_rms_index[0], max_rms_index[1])[:, 0]
    outdata[:, 1] = outdata[:, 0]

    # Saving data into delay buffer for next time
    previous_buffer = indata.copy()

    # Recording both added and not modified signal together for later analysis
    # global recording_normal
    # global recording_dsb
    # recording_normal = np.concatenate((recording_normal, indata))
    # recording_dsb = np.concatenate((recording_dsb, outdata[:, 0]))

queue = multiprocessing.Queue()
wrapped_callback = partial(callback, queue)

with sd.Stream(samplerate=sampling_frequency, channels=[16, 2], callback=wrapped_callback, blocksize=buffer_size):
    sd.sleep(int(duration * 1000))
    plt.ioff()
    # Saving recorded data in wav files
    write("Loudest_Source_Finder/output_data/output_dsb.wav", sampling_frequency, recording_dsb)
    write("Loudest_Source_Finder/output_data/output_8ch_not_processed.wav", sampling_frequency, recording_normal)
    # print("Finished")
    