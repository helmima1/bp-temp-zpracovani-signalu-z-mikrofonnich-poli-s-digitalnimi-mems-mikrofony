import os
import sys
import sounddevice as sd
import numpy as np

current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
from ClassArchitecture import *

# ------------------------------------------------------------
# ------------------- MODIFYING VARIABLES --------------------
# ------------------------------------------------------------
DURATION = 1000 # Duration of the program in seconds

# Steering angles
PHI = 0 # range from 0 to 360 degrees 
THETA = 90 # range from 0 to 90 degrees.

PRINT_STATUS = True # Prints info in the console if buffer overflow happens

# ------------------------------------------------------------
# ----------- CMA PROTOTYPE VARIABLES DEFINITIONS ------------
# ------------------------------------------------------------
SF = 48000 # Sampling frequency
BUFFER_SIZE = 4800
CHANNELS_IN = 16
ACTIVE_CHANNELS_IDS = np.array([0, 1, 2, 3, 8, 9, 10, 11]) # Ids of active channels in this current FPGA configuration

# ------------------------------------------------------------
# ------------------- CLASSES DEFINITIONS --------------------
# ------------------------------------------------------------
# Microphone array properties definition
microphoneArray = MicrophoneArray(radius=0.05,
                              m=8,
                              c=343,
                              sf=SF,
                              buffer_size=BUFFER_SIZE,
                              channels_in=CHANNELS_IN,
                              active_channels_ids=ACTIVE_CHANNELS_IDS
                            )

# Delay and sum beamforming properties definition
dsb = DSB(microphoneArray=microphoneArray, 
             phi= PHI,
             theta= THETA
            )

# ------------------------------------------------------------

def callback(indata, outdata, frames, t, status):
    if status and PRINT_STATUS:
        print(status)
    global previous_buffer

    outdata[:, 0] = outdata[:, 1] = dsb.apply_dsb_delays_realtime(input_buffer=indata, previous_buffer=previous_buffer)

    # Saving data into delay buffer for next time
    previous_buffer = indata.copy()

if __name__ == "__main__":
    # --------- Searching for microphone array device id ---------
    print(sd.query_devices())
    OUTPUT_DEVICE_ID = 0
    FPGA_DEVICE_ID = dsb.find_device_by_name("FPGA MicArrayBoard: USB Audio (hw:2,0)")
    if FPGA_DEVICE_ID == None:
        raise Exception(f"Error: Microphone array prototype from FEE CTU has not been detected.")
    else:
        print(f"device found: {FPGA_DEVICE_ID}")
    sd.default.device = FPGA_DEVICE_ID, OUTPUT_DEVICE_ID

    # Setting previous buffer
    previous_buffer = np.zeros((BUFFER_SIZE, CHANNELS_IN))
    
    with sd.Stream(samplerate=SF, channels=[16, 2], callback=callback, blocksize=BUFFER_SIZE):
        sd.sleep(int(DURATION * 1000))

    print("Finished")