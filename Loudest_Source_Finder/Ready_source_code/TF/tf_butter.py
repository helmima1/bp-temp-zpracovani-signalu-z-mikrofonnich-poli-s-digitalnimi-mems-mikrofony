"""
Tyto grafy ukazují, jak moc se liší hlasitost z "imaginárních" mikrofonů vypočtených 
pomocí průměrování okolních "fyzických". Na přenosových funkcích je vidět, že hlasitost
kardioidy vypočítané z "imaginárních" mikrofonnů je na klíčových frekvencích pod 1 kHz 
nižší, než na je kardioida z "fyzických mikrofonů". Z toho plyne, že pro hledání nejhlasitějšího
zdroje pomocí kardioid je potřeba "imaginární" mikrofony adekvátně zesílit, abychom tento
nežádoucí efekt eliminovali.

Přenosové funkce zároveň ovlivňuje i kvantizace na celé počty vzorků. Tomu je pak potřeba 

Nutno instalovat PyQt5 package, pomocí pip lze instalovat tímto způsobem:
pip install PyQt5
""" 
import sys
import os
import numpy as np
from scipy.signal import butter, freqz

import matplotlib
matplotlib.use('Qt5Agg') 
import matplotlib.pyplot as plt

# Import of ComputationFunctions from different directory
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '../../..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

# --------------- VARIABLES DECLARATIONS ---------------
max_frequency = 1000
c = 343 # Speed of sound
phi = 0 # Angle from which the sound waves reach the microphone array
base_mic_distance = 0.1 # 10 cm
approx_mic_distance = cf.approximation_radius(base_mic_distance, 1, 2)
# ----------------------------------------------------------------------------------
# --------------- TRANSFER FUNCTION CALCULATION ---------------

# Transfer function for physical microphone
tf_base = np.abs(cf.cardioid_tf(phi, base_mic_distance, max_frequency))
tf_base_q = np.abs(cf.cardioid_tf_quantized(phi, base_mic_distance, max_frequency))

# Transfer function for approximated microhone
tf_approx = np.abs(cf.cardioid_tf(phi, approx_mic_distance, max_frequency))
tf_approx_q = np.abs(cf.cardioid_tf_quantized(phi, approx_mic_distance, max_frequency))

# --------------- AVERAGE CALCULATION ---------------
# Calculates ratios between 2 transfer functions. 
# Values greater than 1 indicates by how much is base tf stronger than approximated tf.
# Values smaller than 1 indicates by how much is base tf weaker than approximated tf.
ratios = np.abs(tf_base/tf_approx)
ratios_q = np.abs(tf_base_q/tf_approx_q)

# --------------- FILTER CALCULATION ---------------
fs = 48000 
most_similar_frequency = cf.find_most_similar_frequency(ratios_q, fs, 2000, 8000, max_frequency)
print(f"Most similar frequency is {most_similar_frequency}")
 
cutoff = most_similar_frequency 
order = 1
b, a = butter(order, cutoff / (0.5 * fs), btype='low', analog=False)
freqs, response = freqz(b, a, worN=np.linspace(20, max_frequency, max_frequency - 19) * 2*np.pi / fs)
butter_magnitudes = np.abs(response) * ratios_q[0]

# --------------- DISPLAY PLOTS ---------------
frequencies = np.linspace(20, max_frequency, len(tf_base_q))

# Graf 1
plt.figure(1, figsize=(9, 6))
plt.semilogx(frequencies, tf_base, color='cornflowerblue', label='TF Base')
plt.semilogx(frequencies, tf_approx, color='lightcoral', label='TF Approx')
plt.title('Normal Transfer Functions')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude')
plt.grid(True, which="both", ls="--")
plt.xlim(20, max_frequency)
plt.legend()

# Graf 2
# plt.figure(2, figsize=(9, 6))
# plt.semilogx(frequencies, tf_base_q, color='blue', label='TF Base Quantized')
# plt.semilogx(frequencies, tf_approx_q, color='darkred', label='TF Approx Quantized')
# plt.title('Quantized Transfer Functions')
# plt.xlabel('Frequency (Hz)')
# plt.ylabel('Amplitude')
# plt.grid(True, which="both", ls="--")
# plt.xlim(20, max_frequency)
# plt.legend()

# Graf 3
plt.figure(3, figsize=(9, 6))
# plt.semilogx(frequencies, ratios, color='lightcoral', label='Ratio Normal')
plt.semilogx(frequencies, ratios_q, color='darkred', label='Ratio Quantized')
plt.semilogx(frequencies, butter_magnitudes, color='orange', label='Butterworth Filter Response')
# plt.title('Ratios and Filter Response')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Ratio')
plt.grid(True, which="both", ls="--")
plt.xlim(20, max_frequency)
# plt.legend()

plt.show()