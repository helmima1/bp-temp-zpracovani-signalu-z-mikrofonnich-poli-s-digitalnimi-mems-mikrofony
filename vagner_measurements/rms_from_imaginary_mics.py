import os
import sys
import soundfile as sf
import numpy as np
current_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(current_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf


wav_file_path = f"Processed_recordings/krok_0{16}_processed.wav"
data, samplerate = sf.read("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/vagner_measurements/" +wav_file_path)

rms = cf.calculate_rms_from_buffer(data)
print(rms)

print(0.01712900371875529/rms)