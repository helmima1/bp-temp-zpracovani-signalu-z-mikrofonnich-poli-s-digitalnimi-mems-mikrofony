import numpy as np
import matplotlib.pyplot as plt

from os.path import isfile, splitext, abspath, dirname, join as pjoin
from os import listdir
import scipy.io

import PyOctaveBand

################################################
################## FUNCTIONS ###################
################################################

# generator to list all files in folder with sound files
def get_files(path):
    for file in listdir(path):
        if isfile(pjoin(path, file)):
            if splitext(file)[1] == '.wav':
                if 'krok_' in splitext(file)[0]:
                    yield file

# make an mic order vector for delayed mics from the leading mic order vector
def order_leading_to_delayed(leading_mic_order):
    if len(leading_mic_order) % 2:
        raise ValueError("The array has to have an even number of microphones.")
    middle = int(len(leading_mic_order) / 2)
    first_half = leading_mic_order[:middle]
    second_half = leading_mic_order[middle:]
    delayed_mic_ord = second_half + first_half
    return delayed_mic_ord

################################################
################## PARAMETERS ##################
################################################

# file parameters
WAV_FOLDER_NAME = 'measurement_2023-07-27_14-44'
DATA_DIR = pjoin(abspath(dirname( __file__ )), WAV_FOLDER_NAME)
FILE_NAME_PREFIX = 'krok_'  # prefix in filenames of the wav files before the step number

# aparatus parameters
SOUND_SPEED = 343                       # in m/s
MIC_DIST = 0.1                          # in m
SOUND_DELAY = MIC_DIST/SOUND_SPEED      # in s
MICROPHONE_PREPARE_TIME = 0           # in s
STEPS_PER_REVOLUTION = 200

# check all the files in the working directory
first_iteration = True
step_list = []
for file in get_files(DATA_DIR):
    if first_iteration:
        first_file_path = pjoin(DATA_DIR,file)
        rate, data_first = scipy.io.wavfile.read(first_file_path)
        data_first = data_first[round(rate*MICROPHONE_PREPARE_TIME):,:]       # remove microphone prepare time
        means = data_first.mean(0)
        data_first = data_first - np.tile(means,(data_first.shape[0],1))  # remove DC offset
        first_iteration = False
    filename_without_extension = splitext(file)[0]
    filename_only_step_number = filename_without_extension.lstrip(FILE_NAME_PREFIX)
    step_list.append(int(filename_only_step_number))

# microphone arrangement parameters
NUMBER_OF_MICROPHONES = data_first.shape[1]
MIC_ORDER_DEFAULT = list(range(0,NUMBER_OF_MICROPHONES))
DELAYED_MIC_ORDER_DEFAULT = order_leading_to_delayed(MIC_ORDER_DEFAULT)
#MIC_ORDER = MIC_ORDER_DEFAULT
#MIC_ORDER = [1,6,4,2,0,7,5,3] # pro measurement_2023-07-27_15-19
MIC_ORDER = [3,4,6,0,2,5,7,1]  # pro measurement_2023-07-27_14-44
DELAYED_MIC_ORDER = order_leading_to_delayed(MIC_ORDER)
ANGLE_LIST = np.array([2*np.pi*x/STEPS_PER_REVOLUTION for x in step_list])

# DSP parameters
POLAR_PATTERN_FREQUENCIES = [400,600,800,1000]          # in Hz
PLOT_R_LIM = [-30,0]                                      # in dB - limits to r axis in polar plots

################################################
##################### DSP ######################
################################################

def calculate_rms_from_buffer(audio_buffer: np.array):
    """
    Calculates RMS value from a buffer 
    """
    rms_hodnota = np.sqrt(np.mean(audio_buffer**2))
    
    # Převede RMS hodnotu na decibely; referenční hodnota 1 pro digitální audio signály
    # Používáme 20 * log10(RMS/1) = 20 * log10(RMS)
    # Přidáváme malou hodnotu k RMS hodnotě, aby se předešlo logaritmu z nuly
    rms_v_db = 20 * np.log10(rms_hodnota + 1e-20)

polar_patterns = np.zeros([len(step_list),NUMBER_OF_MICROPHONES,len(POLAR_PATTERN_FREQUENCIES)])
for index,file in enumerate(get_files(DATA_DIR)):
    print('Given file:',file)
    # import signal
    wav_fname = pjoin(DATA_DIR, file)
    rate, data = scipy.io.wavfile.read(wav_fname)

    # prepare signal
    data = data[round(rate*MICROPHONE_PREPARE_TIME):,:]       # remove microphone prepare time
    means = data.mean(0)
    data = data - np.tile(means,(data.shape[0],1))  # remove DC offset

    # crop the signal to compensate for sound delay
    delay_in_samples = round(SOUND_DELAY*rate)
    delayed_data = data[delay_in_samples:,:]
    leading_data = data[:-delay_in_samples,:]

    # subtract the signals
    delayed_data_rearranged = delayed_data[:,DELAYED_MIC_ORDER]
    leading_data_rearranged = leading_data[:,MIC_ORDER]
    cardioid_data = leading_data_rearranged - delayed_data_rearranged

    print(cardioid_data)

    # prepare DSP values
    samples = cardioid_data.shape[0]
    time = np.linspace(0., samples/rate, samples, endpoint=False)

    # calculate spectrum
    spectrum = abs(np.fft.fft(cardioid_data,axis=0))
    #spl, freq = PyOctaveBand.octavefilter(cardioid_data[:,1], fs=rate, fraction=12, order=6, limits=[12, 20000], show=0, sigbands=0)

    for freq_index,chosen_freq in enumerate(POLAR_PATTERN_FREQUENCIES):
        chosen_frequency_sample_number = int(np.floor(data_first.shape[0]*chosen_freq/rate))
        # print('freq_samp',chosen_frequency_sample_number)
        # extract the value on chosen frequency
        polar_patterns[index,:,freq_index] = spectrum[chosen_frequency_sample_number,:]

max_matrix = polar_patterns.max(axis=0)
polar_patterns_norm = polar_patterns / max_matrix[np.newaxis,:,:]       # normalizing the matrix; np.newaxis means to reshape from 2 to 3 dims. 
polar_patterns_norm_db = 20*np.log10(polar_patterns_norm)
polar_patterns_norm_db = np.clip(polar_patterns_norm_db,PLOT_R_LIM[0],PLOT_R_LIM[1])

sorting_vector = np.argsort(ANGLE_LIST)
sorted_polar_patterns = polar_patterns_norm_db[sorting_vector,:,:]
# to join the end of each polar plot lines together (add starting value to the end)
sorted_polar_patterns = np.append(sorted_polar_patterns,np.reshape(sorted_polar_patterns[0,:,:],[1,sorted_polar_patterns.shape[1],sorted_polar_patterns.shape[2]]),0)
SORTED_ANGLE_LIST = ANGLE_LIST[sorting_vector]
SORTED_ANGLE_LIST = np.append(SORTED_ANGLE_LIST,SORTED_ANGLE_LIST[0])

plt.figure(1)
of_rows = 2
#orient = ['SW','E','W','SE','SW','E','W','SE']
#orient = ['SW','NE','S','N','NE','SW','N','S']
for selected_pair in MIC_ORDER_DEFAULT:
    ax = plt.subplot(of_rows,int(len(MIC_ORDER_DEFAULT)/of_rows),selected_pair+1, projection='polar')
    plt.yticks(np.append(np.array(np.arange(PLOT_R_LIM[0],PLOT_R_LIM[1],6)),PLOT_R_LIM[1]))
    plt.ylim(PLOT_R_LIM)
    plt.title('Microphone pair: ' + str(MIC_ORDER_DEFAULT[selected_pair]+1) + '-' + str(DELAYED_MIC_ORDER_DEFAULT[selected_pair]+1))
    #ax.set_theta_zero_location(orient[selected_pair])
    ax.set_theta_zero_location('N')
    ax.set_theta_direction(-1)
    plt.plot(SORTED_ANGLE_LIST,sorted_polar_patterns[:,selected_pair,:])
#plt.figlegend(POLAR_PATTERN_FREQUENCIES,title='Frequency [Hz]')

#print('step_list_len',step_list.shape)
#print('polpat_len',polar_patterns.shape)
#print('pol_pat',sorted_polar_patterns)
#print('angles', SORTED_ANGLE_LIST)

plt.tight_layout(pad=-1.6)
plt.show()
















###################################################
################## REFERENCE MICROPHONE

# # import signal
# DATA_DIR = pjoin(abspath(dirname( __file__ )), 'measurement_2023-07-10_15-18')
# wav_fname = pjoin(DATA_DIR, 'reference_recording.wav')
# ref_rate, ref_data = scipy.io.wavfile.read(wav_fname)

# #means = ref_data.mean(0)
# #ref_data = ref_data - np.tile(means,(ref_data.shape[0],1))
# samples = ref_data.shape[0]
# time = np.linspace(0., samples/rate, samples, endpoint=False)

# ref_spectrum = abs(np.fft.fft(ref_data))


# print('rate:',rate,'Hz')
# print('ref_data shape is:', ref_data.shape)
# print('spectrum shape is:', ref_spectrum.shape)


# fig, axs = plt.subplots(2,1)
# axs[0].plot(time, ref_data)
# axs[1].plot(time, ref_spectrum)
# plt.xlabel('t [s]')
# plt.ylabel('signed 16-bit integers')
# plt.show()