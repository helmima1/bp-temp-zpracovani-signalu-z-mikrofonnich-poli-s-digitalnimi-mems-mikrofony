import numpy as np
import matplotlib.pyplot as plt

from os.path import isfile, splitext, abspath, dirname, join as pjoin
from os import listdir
import scipy.io
import scipy.signal

REFERENCE_SIGNAL = 3       # reference signal channel number 0 to 7
ANGLE_IN_INDEX = 0         # index (number) of the file read if files are sorted alphabetically

def get_files(path):
    for file in listdir(path):
        if isfile(pjoin(path, file)):
            if splitext(file)[1] == '.wav':
                if 'krok_' in splitext(file)[0]:
                    yield file

WAV_FOLDER_NAME = 'measurement_2023-07-27_14-44'
DATA_DIR = pjoin(abspath(dirname( __file__ )), WAV_FOLDER_NAME)
FILE_NAME_PREFIX = 'krok_'  # prefix in filenames of the wav files before the step number
MICROPHONE_PREPARE_TIME = 0           # in s

first_iteration = True
for index,file in enumerate(get_files(DATA_DIR)):
    if index==ANGLE_IN_INDEX:
        first_file_path = pjoin(DATA_DIR,file)
        rate, data_first = scipy.io.wavfile.read(first_file_path)
        data_first = data_first[round(rate*MICROPHONE_PREPARE_TIME):,:]       # remove microphone prepare time
        means = data_first.mean(0)
        data_first = data_first - np.tile(means,(data_first.shape[0],1))  # remove DC offset
        first_iteration = False

plt.figure(1)
for t in range(0,8):
    stopa0 = data_first[:,REFERENCE_SIGNAL]
    stopa1 = data_first[:,t]

    vysl = scipy.signal.correlate(stopa0,stopa1,mode='full')
    lags = scipy.signal.correlation_lags(len(stopa1), len(stopa0))
    # print('vysl',vysl)
    # print('st0',stopa0)
    # print('St1',stopa1)

    plt.subplot(8,1,t+1)
    plt.stem(lags,vysl)
    plt.xlim([-20,20])
    if t == 7:
        plt.xlabel('Delay in samples [-]')
    else:
        plt.xticks([])
    if t == 3:
        plt.ylabel('Cross-correlation value [-]')
plt.show()
