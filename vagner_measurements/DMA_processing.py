"""
This script applies DSB algorithm to measured data by David Vágner, Jan Šedivý and David Ringsmuth. The output data
are then stored in "Processed_recordings" folder.
"""
import os
import sys
import soundfile as sf
import numpy as np
original_dir = os.path.dirname(__file__)
computation_functions_path = os.path.join(original_dir, '..', 'ComputationFunctions')
computation_functions_path = os.path.abspath(computation_functions_path)
sys.path.append(computation_functions_path)
import ComputationFunctions as cf

os.chdir(original_dir)

abs_cesta = os.path.abspath('/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/vagner_measurements/')

# Kontrola existence souboru
existuje = os.path.exists(abs_cesta)
print(existuje)

# ------------------------------------------------------------
# ------------------- VARIABLES DEFINITION -------------------
# ------------------------------------------------------------
MIC_ORDER = [3, 4, 6, 0, 2, 5, 7, 1]  # Sorting channels in the correct positions
radius = 0.05  # in meters
M = 8
phi = 0  # degrees
theta = 90  # degrees
c = 343  # meters/second
sampling_frequency = 48000  # Hz

# -------------------------------------------------------------
# ------------------- CALCULATION OF DELAYS -------------------
# -------------------------------------------------------------
delays_seconds = cf.cma_tmi(radius, M, phi, theta, c)
delays_samples = cf.quantize_tmi_to_samples(delays_seconds, sampling_frequency)
heighest_delay = max(delays_samples)

sound_delay = (radius*2)/c 
cardioid_delay = round(sound_delay*sampling_frequency)

# ----------------------------------------------------------------
# ------------------- PROCESSING OF RECORDINGS -------------------
# ----------------------------------------------------------------
for i in range(0, 200, 2):
    # Load data from files
    wav_file = ""
    if (i < 10):
        wav_file = f"krok_00{i}"
    elif (i < 100):
        wav_file = f"krok_0{i}"
    else:
        wav_file = f"krok_{i}"
    # with open("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/vagner_measurements/measurement_2023-07-27_14-44/" + wav_file + ".wav", 'rb') as soubor:
    #     data, samplerate = soubor.read()
    # data, samplerate = sf.read("/home/mhel/Documents/Bakalářská Práce/bp-zpracovani-signalu-z-mikrofonnich-poli-s-digitalnimi-mems-mikrofony/vagner_measurements/measurement_2023-07-27_14-44/" + wav_file + ".wav")
    data, samplerate = sf.read("measurement_2023-07-27_14-44/" + wav_file + ".wav")
    
    print(f"Processing of file {wav_file}.wav ...")

    # Rearrange microphones in correct order
    if data.shape[1] == 8:
        data[:, [0, 1, 2, 3, 4, 5, 6, 7]] = data[:, MIC_ORDER]

    # Cardioid calculation
    # delayed_signal = np.pad(data[:, 0], (cardioid_delay, 0), 'constant', constant_values=(0, 0))
    # leading_signal = np.pad(data[:, 4], (0, cardioid_delay), 'constant', constant_values=(0, 0))
    # sum_signal = leading_signal - delayed_signal

    # Cardioid approximated between 2 mics
    delayed_signal_approx = (2/3)*data[:, 0] + (1/3)*data[:, 1]
    leading_signal_approx = (2/3)*data[:, 4] + (1/3)*data[:, 5]
    # delayed_signal_approx = data[:, 0]
    # leading_signal_approx = data[:, 4]
    delayed_signal = np.pad(delayed_signal_approx, (13, 0), 'constant', constant_values=(0, 0))
    leading_signal = np.pad(leading_signal_approx, (0, 13), 'constant', constant_values=(0, 0))
    sum_signal = leading_signal - delayed_signal

    # Save files
    sf.write(f'Processed_recordings/{wav_file}_processed.wav', sum_signal, samplerate)
    print(f"File {wav_file}.wav processed!")